export interface ProductSchema{
    id: number;
    imagen: String;
    precio: number;
    unidad: String;
    nombre:string;
    variedad:String;
    volumen: String;
    disponibleDesde: String;
    disponibleHasta: String;
    proveedor?: String;
}