export interface Userschema {
    administrador?: number;
    estatus?: number;
    token?:string;
    user_id:number;
    wp_user_id:number;
    nickname:string;
    first_name:string;
    last_name:string;
    secont_last_name:string;
    billing_email:string;
    email_alt?:string;
    enterprise_phone:string;
    phone:string;
    celphone?:string;
    billing_name:string;
    billing_company?:string;
    rfc:string;
    cp:string;
    country_id?:number;
    billing_address:string;
    billing_phone:string;
    comertial_name:string;
    semblance:string;
    region_id?:number;
    estado_id?: number;
    ciudad?: string;
    caracteristica_id?: number;
    profile_avatar: string;
    enterprise_avatar: string;
    image_profile:string;
    image_enterprise:string;
    user_type:number
} 

