import { ImageProductSchema } from "./image-product-schema";
import { CertificacionSchema } from "./certificacionSchema";

export interface VenderSchema {

    producto_id:number;
    variedad:string;
    otras_caracteristicas:string;
    volumen_total:string;
    unidad_pedido_minimo_id: number;
    unidad_id:number;
    precio_unidad:number;
    presentacion_id:number;
    tamano:number;
    pedido_minimo:number;
    unidad_presentacion_id:number;
    precio:string;
    pais_id:number;
    estado_id:number;
    ciudad:string;
    codigo_postal:string;
    disponible_desde:string;
    disponible_hasta:string;
    resena:string;
    photo?:ImageProductSchema[];
    certificates?:CertificacionSchema[];
    usuario_id?:number;
    categoria:number;
    venta_id?:number;
    aprobado?:number;
}
