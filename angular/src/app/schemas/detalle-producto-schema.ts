export interface DetalleProductoSchema {

    producto:string;
    precio_unidad:string;
    unidad:string;
    caracteristicas:string;
    productor:string;
    produccion_total:string;
    presentacion:string;
    tamano:string;
    desde:string;
    hasta:string;
    certificados:any[];
    resena:string;
    comentarios:any[];
    imagenes:any[];
    tipo_precio:string;
    origen:string;
    ciudad:string;
    lat?:number;
    lon?:number;
}
