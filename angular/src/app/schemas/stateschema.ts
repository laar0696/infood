export interface StateSchema{
    stateId:number;
    stateName:String;
    stateCountryId: number;
}