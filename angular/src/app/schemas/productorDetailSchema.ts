export interface ProductorDetailSchema<T>{
    id:number;
    name:String;
    image:String;
    calif:number;
    categoryId:number;
    categoryName:String;
    stateId:number;
    stateName:String;
    birthday: String;
    semblance:String;
    products: T;
}