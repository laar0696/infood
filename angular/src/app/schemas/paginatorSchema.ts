export interface PaginatorSchema<T> {
    paginator: any;
    total?:any;
    per_page?:any;
    productors: T[];
    data?:T[];
}