export interface OfertaSchema{
    id:number,
    tipo?:string,
    variedad:string,
    imagenes:any,
    producto_id:number,
    unidad: any    
}