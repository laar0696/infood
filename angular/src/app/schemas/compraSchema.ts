import { OfertaSchema } from "./ofertaSchema";
import { SolicitudSchema } from "./solicitudSchema";
import { ComentarioSchema } from "./comentarioSchema";

export interface CompraSchema{
    id: number
    completado: number
    unidades: number
    oferta?: OfertaSchema
    solicitud?: SolicitudSchema
    comentarios: ComentarioSchema[]
    certificaciones? : any[];
}