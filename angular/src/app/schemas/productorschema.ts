export interface ProductorSchema{
    productorId:number;
    productorName:String;
    productorImage:String;
    productorCalif:number;
    productorCategoryId:number;
    productorCategoryName:String;
    productorStateId:number;
    productorStateName:String;
    productorSemblance:String;
}