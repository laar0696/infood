export interface Personaldataschema {
    user:string;
    name:string;
    lastname:string;
    secontLastname:string;
    email:string;
    emailAlt?:string;
    phone:string;
    celphone?:string;
    avatar:string;
}
