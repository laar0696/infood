import { ImageProductSchema } from "./image-product-schema";
import { CertificacionSchema } from "./certificacionSchema";

export interface SoliciteSchema {
    id?: number;
    variedad: string;
    otras_caracteristicas: string;
    volumen_total:number;
    unidad_id:number;
    precio_unidad:number;
    presentacion_id:number;
    tamano:number;
    requerido_desde:string;
    requerido_hasta:string;
    estado_id:number;
    ciudad:string;
    codigo_postal:string;
    resena:string;
    producto_id:number;
    usuario_id?:number;
    aprobado?:number;
    unidad_presentacion_id:number;
    pais_id:number;
    photo?:ImageProductSchema[];
    certificates?:CertificacionSchema[];
    categoria:number;
}
