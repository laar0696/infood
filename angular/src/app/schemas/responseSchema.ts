export interface ResponseSchema<T>{
    msg:string;
    paginator: any;
    items: T[];
    error:boolean
}