import { StateSchema } from "./stateschema";

export interface CountrySchema {
    id: number;
    name: String;
    states: StateSchema[];
}