import { Component } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { AuthService } from './servicios/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  constructor(auth: AuthService){

    /*
    auth.check().subscribe( data => {
        console.log( data );
        console.log('Usuario valido');
    });

    Observable.interval(600000).subscribe(data => {
        auth.refresh();
    });
    */
  }

}
