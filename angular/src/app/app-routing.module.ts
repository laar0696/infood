import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BeforeLoginService } from './servicios/before-login.service';
import { AfterLoginService } from './servicios/after-login.service';
import { LoginComponent } from './componentes/login/login.component';
import { SignupComponent } from './componentes/signup/signup.component';
import { RequestResetComponent } from './componentes/password/request-reset/request-reset.component';
import { ResponseResetComponent } from './componentes/password/response-reset/response-reset.component';
import { BusquedaComponent } from './componentes/busqueda/busqueda.component';
import { DashboardComponent } from './componentes/dashboard/dashboard.component';
import { DetalleProductoVentaComponent } from './componentes/vender-productos/detalle-producto-venta/detalle-producto.component';
import { NuevoProductoVentaComponent } from './componentes/vender-productos/nuevo-producto-venta/nuevo-producto.component';
import { EditarProductoVentaComponent } from './componentes/vender-productos/editar-producto-venta/editar-producto.component';
import { ListaProductosVentaComponent } from './componentes/vender-productos/lista-productos-venta/lista-productos.component';
import { DetalleProductoSolicitadoComponent } from './componentes/solicitar-productos/detalle-producto-solicitado/detalle-producto-solicitado.component';
import { NuevoProductoSolicitadoComponent } from './componentes/solicitar-productos/nuevo-producto-solicitado/nuevo-producto-solicitado.component';
import { VistaProductosComponent } from './componentes/marketplace-productos/vista-productos/vista-productos.component';
import { DetallesProductoComponent } from './componentes/marketplace-productos/detalles-producto/detalles-producto.component';
import { TodoProductoresComponent } from './componentes/productores/todo-productores/todo-productores.component';
import { VistaSolicitudesComponent } from './componentes/marketplace-solicitudes/vista-solicitudes/vista-solicitudes.component';
import { DetallesSolicitudComponent } from './componentes/marketplace-solicitudes/detalles-solicitud/detalles-solicitud.component';
import { TodoSolicitudesComponent } from './componentes/marketplace-solicitudes/todo-solicitudes/todo-solicitudes.component';
import { DetallesProductorComponent } from './componentes/productores/detalles-productor/detalles-productor.component';
import { DashboardAdministrativeComponent } from './componentes/dashboard-administrador/dashboard-administrative/dashboard-administrative.component';
import { TodoProductosComponent } from './componentes/marketplace-productos/todo-productos/todo-productos.component';
import { AdminService } from './servicios/admin.service';
import { DetallesCompraSolicitudComponent } from './componentes/marketplace-solicitudes/detalles-compra-solicitud/detalles-compra-solicitud.component';
import { DetallesCompraProductoComponent } from './componentes/marketplace-productos/detalles-compra-producto/detalles-compra-producto.component';
import { EditarProductoSolicitadoComponent } from './componentes/solicitar-productos/editar-producto-solicitado/editar-producto-solicitado.component';
import { EditarProductosAdministradorComponent } from './componentes/dashboard-administrador/editar-productos-administrador/editar-productos-administrador.component';
import { EditarSolicitudesAdministradorComponent } from './componentes/dashboard-administrador/editar-solicitudes-administrador/editar-solicitudes-administrador.component';
import { MapaComponent } from './material/mapa/mapa.component';


const routes: Routes = [
  

  {
    path: 'login',
    component: LoginComponent,
    canActivate: [BeforeLoginService]
  },

  {
    path: 'signup',
    component: SignupComponent
  },

  {
    path: 'request-password-reset',
    component: RequestResetComponent
  },

  {
    path: 'response-password-reset',
    component: ResponseResetComponent
  },

  { 
    path: 'busqueda' , 
    component: BusquedaComponent
  },

  { 
    path: 'dashboard', 
    component: DashboardComponent,
    canActivate: [AfterLoginService]
      
  },
  
  { 
    path: 'detalle-producto-venta/:id', 
    component: DetalleProductoVentaComponent,
    canActivate: [AfterLoginService]
  },
    
  { 
    path: 'nuevo-producto-venta', 
    component: NuevoProductoVentaComponent,
    canActivate: [AfterLoginService]  
  },
  
  { 
    path: 'editar-producto-venta/:id', 
    component: EditarProductoVentaComponent,
    canActivate: [AfterLoginService]  
  },
  
  { 
    path: 'listar-productos-venta', 
    component: ListaProductosVentaComponent,
    canActivate: [BeforeLoginService]
  },
    
  { 
    path: 'detalle-producto-solicitado/:id', 
    component: DetalleProductoSolicitadoComponent,
    canActivate: [AfterLoginService] 
    
  },

  { 
    path: 'nuevo-producto-solicitado', 
    component: NuevoProductoSolicitadoComponent, 
    canActivate: [AfterLoginService]
  },

  { 
    path: 'editar-producto-solicitado/:id', 
    component: EditarProductoSolicitadoComponent, 
    canActivate: [AfterLoginService]
  },

  {
    path: 'editar-solicitud-administrador/:id',
    component:EditarSolicitudesAdministradorComponent,
    canActivate: [AfterLoginService]
  },

  {
    path:'editar-producto-administrativo/:id',
    component:EditarProductosAdministradorComponent,
    canActivate: [AfterLoginService]
  },

  { 
    path: 'marketplace-vista-productos', 
    component: VistaProductosComponent
  },
  
  
  { 
    path: 'marketplace-detalles-producto/:id', 
    component: DetallesProductoComponent
  },


  { 
    path: 'marketplace-busqueda-productos/:buscar', 
    component: TodoProductosComponent
  },

  { 
    path: 'marketplace-busqueda-productos', 
    component: TodoProductosComponent
  },
  
  
  { path: 'marketplace-vista-solicitudes', component: VistaSolicitudesComponent },
  { path: 'marketplace-detalles-solicitud/:id', component: DetallesSolicitudComponent },

  { path: 'detalles-compra-solicitud/:id', component: DetallesCompraSolicitudComponent },
  { path: 'detalles-compra-producto/:id', component: DetallesCompraProductoComponent },
  


  
  { path: 'marketplace-busqueda-solicitudes', component: TodoSolicitudesComponent },
  { path: 'productores', component: TodoProductoresComponent  },
  { path: 'productor-detalle/:id', component: DetallesProductorComponent },
  
  { 
    path: 'dashboard-administrador', 
    component: DashboardAdministrativeComponent,
    canActivate: [AdminService]
  },

  {
    path: '**',
    redirectTo: 'dashboard'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}