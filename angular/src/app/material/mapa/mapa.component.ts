import {Component, ViewChild, Input} from '@angular/core';
import {SourceRasterComponent} from 'ngx-openlayers';
import { FormsModule } from '@angular/forms';

interface RasterData {
  brightness: number;
  contrast: number;
}

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css'],
})
export class MapaComponent {
  @Input() x:number;
  @Input() y:number;

  threads = 4;
  brightness = 0;
  contrast = 0;
  operationType = 'image';
  selectLayer = 'osm';
  lib: any = {
    brightness: this.brightness,
    contrast: this.contrast,
  };
 
  @ViewChild(SourceRasterComponent) currentRasterSource;


  beforeOperations(event) {
    const data: RasterData = event.data;
    data.brightness = this.brightness;
    data.contrast = this.contrast;
  }

  operation(imageDatas: [ImageData], data: RasterData) {
    let [imageData] = imageDatas;
    return imageData;
  }

  afterOperations() {}

  updateRaster() {
    this.currentRasterSource.instance.changed();
  }
}
