import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSliderModule} from '@angular/material/slider';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCardModule} from '@angular/material/card';
import {MatPaginatorModule, MatPaginatorIntl} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatPaginatorIntlCro } from './paginator/paginator';
import {SlideshowModule} from 'ng-simple-slideshow';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatExpansionModule} from '@angular/material/expansion';
import { MapaComponent } from './mapa/mapa.component';
import {AngularOpenlayersModule} from 'ngx-openlayers';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatTooltipModule,
    MatSliderModule,
    MatDatepickerModule,
    MatCardModule,
    MatPaginatorModule,
    MatFormFieldModule,
    SlideshowModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatMenuModule,
    MatExpansionModule,
    AngularOpenlayersModule,
    FormsModule
  ],
  exports: [
    MatTooltipModule,
    MatSliderModule,
    MatDatepickerModule,
    MatCardModule,
    MatPaginatorModule,
    MatFormFieldModule,
    SlideshowModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatMenuModule,
    MatExpansionModule,
    AngularOpenlayersModule,
    MapaComponent
  ],
  declarations: [MapaComponent],
  providers: [{ provide: MatPaginatorIntl, useClass: MatPaginatorIntlCro}],
})
export class MaterialModule { }
