import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatTooltipModule, MatDialogModule,MAT_DIALOG_DEFAULT_OPTIONS  } from '@angular/material';
import { NgForm, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JarwisService } from './servicios/jarwis.service';
import { TokenService } from './servicios/token.service';
import { AuthService } from './servicios/auth.service';
import { AfterLoginService } from './servicios/after-login.service';
import { BeforeLoginService } from './servicios/before-login.service';
import { RecaptchaModule } from 'ng-recaptcha';
import { SocialLoginModule } from "angularx-social-login";
import { AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";
import { LoginComponent } from './componentes/login/login.component';
import { RequestResetComponent } from './componentes/password/request-reset/request-reset.component';
import { ResponseResetComponent } from './componentes/password/response-reset/response-reset.component';
import { SignupComponent } from './componentes/signup/signup.component';
import { BusquedaModule } from './componentes/busqueda/busqueda.module';
import { DashboardModule } from './componentes/dashboard/dashboard.module';
import { DashboardAdministradorModule } from './componentes/dashboard-administrador/dashboard-administrador.module';
import { MarketplaceProductosModule } from './componentes/marketplace-productos/marketplace-productos.module';
import { MarketplaceSolicitudesModule } from './componentes/marketplace-solicitudes/marketplace-solicitudes.module';
import { NuevoUsuarioModule } from './componentes/nuevo-usuario/nuevo-usuario.module';
import { ProductosModule } from './componentes/vender-productos/productos.module';
import { ProductoresModule } from './componentes/productores/productores.module';
import { SolicitarProductosModule } from './componentes/solicitar-productos/solicitar-productos.module';
import { ProductorsService } from './servicios/productors.service';
import { MaterialModule } from './material/material.module';
import { LoadingModalComponent } from './componentes/loading-modal/loading-modal.component';
let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("461623854016-8a8euoak7n1rifm01pkjj68670a02dtj.apps.googleusercontent.com")
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("179326702957217")
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    LoadingModalComponent,
    LoginComponent,
    RequestResetComponent,
    ResponseResetComponent,
    SignupComponent,
    
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    MaterialModule,
    BusquedaModule,
    DashboardModule,
    DashboardAdministradorModule,
    FormsModule,
    HttpClientModule,
    MatDialogModule,
    MatTooltipModule,
    MarketplaceProductosModule,
    MarketplaceSolicitudesModule,
    NuevoUsuarioModule,
    ProductosModule,
    ProductoresModule,
    RecaptchaModule.forRoot(),
    SocialLoginModule,
    SolicitarProductosModule
  ],
  entryComponents:[
    LoadingModalComponent
  ],
  providers: [
    { 
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: { hasBackdrop:true, position: [{top: '5px'}] }
    },
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    JarwisService, TokenService, AuthService, AfterLoginService, BeforeLoginService,
    ProductorsService
      //{ provide: 'SnotifyToastConfig', useValue: ToastDefaults },
      //SnotifyService
  ],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
