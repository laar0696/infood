import { Component, OnInit, Input} from '@angular/core';
import { MatDialog } from '@angular/material';
import { Userschema } from '../../schemas/userschema';
import { DashboardPersonalService } from '../../servicios/dashboard-personal.service';
import { state } from '@angular/animations';
import { AuthService } from '../../servicios/auth.service';
import { TokenService } from '../../servicios/token.service';
import { Router } from '@angular/router';
import { AdminService } from '../../servicios/admin.service';
// import { TooltipPosition } from '@angular/material';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  // template: `
  //   Message: {{imgPerfil}}
  //   <app-datos-personales-form (change)="previewPerfilImage($event)"></app-datos-personales-form>`,
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

  pspinner = {  mode:'indeterminate' };
  spinerIsVisible= true;

  infoUser : Userschema;
  dashboardNavigatorRoute: string;

  perfil = true;
  solicitar = false;
  favoritos = false;
  comprar = false;
  vender = false;
  usuarios = false;

  countryId = 1;

  info = false;
  application = false;
  solicitute = false;

  visible='';

  public imgPerfil = 'assets/img/avatar.png';
  public name = '';

  setInvisible() {
    this.visible = 'invisible';

    setTimeout(() => { 
      this.visible = '';
    }, 1000);
  }

  abrePerfil(){
    this.perfil = true;
    this.vender = false;
    this.solicitar = false;
    this.favoritos = false;
    this.comprar = false;
  }

  abreVender(){
    this.perfil = false;
    this.vender = true;
    this.solicitar = false;
    this.favoritos = false;
    this.comprar = false;
  }

  abreSolicitar(){
    this.perfil = false;
    this.vender = false;
    this.solicitar = true;
    this.favoritos = false;
    this.comprar = false;
  }

  abreFavoritos(){
    this.perfil = false;
    this.vender = false;
    this.solicitar = false;
    this.favoritos = true;
    this.comprar = false;
  }

  abreComprar(){
    this.perfil = false;
    this.vender = false;
    this.solicitar = false;
    this.favoritos = false;
    this.comprar = true;
  }

  constructor( private router: Router, private token: TokenService, public dialog: MatDialog, private dashboardService:DashboardPersonalService, private auth: AuthService) { 
    
    //Evalua y actualiza estatus de administrador desde el servidor
    this.auth.admin().subscribe( 
      data => { 
        if( data ){
          this.auth.administrador( data );
          this.router.navigate(['dashboard-administrador']); 
        }
      }
    );
    
    if( this.auth.esAdmin() ) {
      this.router.navigate(['dashboard-administrador']);
    }

    sessionStorage.validForm = 'true';
    
  }
  
  openDialog(): void {
    let dialogRef = this.dialog.open(ModalCambiarContrasena, {
      width: '600px',
    });
  }

  ngOnInit() {
    /* 
      Abre el componente respectivo luego de crear un nuevo producto o solicitud, 
      no manipula ningun dato.
    */
    if(sessionStorage.displayInfood !== void 0){
      sessionStorage.displayInfood === 'vender' ? this.abreVender() :
      sessionStorage.displayInfood === 'solicitar' ? this.abreSolicitar() : 
      sessionStorage.displayInfood === 'comprar' ? this.abreComprar() :
      sessionStorage.displayInfood === 'favoritos' ? this.abreComprar() : '';
      sessionStorage.removeItem('displayInfood');
    }else{
      this.dashboardNavigatorRoute = 'Perfil';
    }

    this.dashboardService.getPersonalData(1).subscribe( res => {

      this.imgPerfil = this.isNull(res.usermarket.perfil_avatar);
      this.name = this.isNull(res.usermarket.name) +' '+ this.isNull(res.usermarket.ap_paterno);

      sessionStorage.setItem('countries',JSON.stringify(res.countries));
      sessionStorage.setItem('states',JSON.stringify(res.states));
      sessionStorage.setItem('user_type', JSON.stringify(res.user_type));
      sessionStorage.setItem('users_type', JSON.stringify(res.users_type));
      sessionStorage.setItem('user_type', JSON.stringify(res.user_type));
      sessionStorage.setItem('users_type', JSON.stringify(res.users_type));

      for (let i = 0; i < res.countries.length; i++) {
        for (let j = 0; j < res.countries[i].states.length; j++) {
          if (res.countries[i].states[j].id == this.isNull(res.usermarket.estado_id)) {
            this.countryId = res.countries[i].id;
            break;
          }
        };
      };

      this.infoUser = {
        token:localStorage.token,
        administrador: this.isNull(res.usermarket.administrador),
        estatus: this.isNull(res.usermarket.estatus),
        nickname: this.isNull(res.usermarket.user_wp.user_nicename) ,
        user_id: this.isNull(res.usermarket.id),
        wp_user_id: this.isNull(res.usermarket.wp_user_id),
        first_name: this.isNull(res.usermarket.name),
        last_name: this.isNull(res.usermarket.ap_paterno),
        secont_last_name: this.isNull(res.usermarket.ap_materno),
        // billing_email:this.isNull(res.usermarket.email),
        billing_email:this.isNull(res.usermeta.email),
        billing_company:this.isNull(res.usermeta.billing_company),
        email_alt: this.isNull(res.usermarket.email_alt),
        enterprise_phone: this.isNull(res.usermarket.telefono_empresa),
        billing_name: '',
        rfc: this.isNull(res.usermarket.rfc),
        cp: this.isNull(res.usermeta.cp),
        billing_phone: this.isNull(res.usermarket.telefono_empresa),
        celphone: this.isNull(res.usermarket.celular) ,
        phone: this.isNull(res.usermeta.billing_phone),
        billing_address: this.isNull(res.usermeta.billing_address),
        comertial_name: this.isNull(res.usermarket.nombre_comercial),
        semblance: this.isNull(res.usermarket.semblanza),
        profile_avatar : this.isNull(res.usermarket.perfil_avatar),
        enterprise_avatar: this.isNull(res.usermarket.empresa_avatar),
        image_profile: '',
        image_enterprise: '',
        estado_id: this.isNull(res.usermarket.estado_id),
        country_id: this.isNull(this.countryId),
        ciudad: this.isNull(res.usermarket.ciudad),
        user_type: this.isNull(res.usermarket.tipo_usuario_id)
      }
      
    })
    .add( () => {
      sessionStorage.setItem('userData',JSON.stringify(this.infoUser));
    })
    .add( () => this.info=true)
    .add( () => {
      this.spinerIsVisible = false;
    });

  }

  isNull(data){ 
    return data = data === void 0 || data === null ? '' : data; 
  }

}


/* Modal cambiar contraseña */

@Component({
  selector: 'modal-cambiar-contrasena',
  templateUrl: 'modal-cambiar-contrasena.html',
})
export class ModalCambiarContrasena {
  
  closeDialog(): void {
    let dialogRef = this.dialog.closeAll();
  }
  password = '';
  confirmPassword = '';
  updated = false;
  msg = '';

  constructor(private dashboardService:DashboardPersonalService, public dialog: MatDialog) {}

  savePassword(){
    this.updated = false;
    if (this.password != this.confirmPassword) {
      this.updated = true;
      this.msg = 'Las contraseñas no coinciden';
    } else if ( this.password.length < 5) {
      this.updated = true;
      this.msg = 'Debe contener al menos 5 caracteres';
    } else {
      this.dashboardService.updatePassword(this.password).subscribe(res => {
        this.updated = true;
        this.msg = res[0]
      });
    }
  }
}