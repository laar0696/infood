import { Component, OnInit } from '@angular/core'; 
import { AuthService } from '../../../servicios/auth.service';
import { TokenService } from '../../../servicios/token.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  menuOpened:boolean = false;
  
  public buscar=false;

  public login=false;

  constructor(private auth: AuthService, private token:TokenService, private router:Router) {
    auth.checkNav().subscribe( 
      data => {
        this.login = true;
      },
      error => {
        this.login = false;
      }      
    );    
  }

  toogleMovilMenu(opened) {
    
  }

  salir(){
    this.auth.logout();
  }

  entrar(){
    this.router.navigate(['login']);
  }

  ngOnInit() {
  }


  public clickBuscar(){
    this.buscar = !this.buscar;
  }

}
