import { Component, OnInit } from '@angular/core';
import { ProductoItem } from '../vender/producto-item/producto-item';
import { DashboardFavoritosService } from '../../../servicios/dashboard-favoritos.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-favoritos',
  templateUrl: './favoritos.component.html',
  styleUrls: ['./favoritos.component.css']
})
export class FavoritosComponent implements OnInit {
  private userId = JSON.parse(sessionStorage.getItem('userData')).user_id;

  pspinner = {  mode:'indeterminate' };
  spinerIsVisible= true;

  productos = [];
  noFavorites = false;

  legend:string;
  method:string;
  id_solicitate:number;
  modalDisplay:boolean;
  copyProduct:ProductoItem;

  constructor(
    private favoritesService: DashboardFavoritosService,
    private router: Router
  ) { 
    this.legend = '¿Desea quitar este producto de sus favoritos?';
    this.method = 'quitar-producto-favorito';
    this.id_solicitate = 0;
    this.modalDisplay = false;
  }

  
  public paginatorConf = {
    length: 0,
    pageSize: 8,
    pageIndex: 0,
    pageSizeOptions: [ 2, 4, 8, 16 ]
  };

  ngOnInit() {
    this.getFavorites();
    sessionStorage.originFavorites = 'true';
  }

  closeModal(event){
    if(event === true){
      this.spinerIsVisible = true;
      this.favoritesService.deleteFavoriteProduct(this.id_solicitate).subscribe(data => {
        this.getFavorites()
        if (this.productos.length == 0) {
          this.noFavorites = true;
        }
      })
      .add( () => this.modalDisplay = false)
    }else{
      this.modalDisplay = event;
    }
    
  }

  getFavorites() {
    this.favoritesService.getFavoriteProducts(this.userId, this.paginatorConf.pageIndex, this.paginatorConf.pageSize).subscribe( data => {
      this.productos = data.productos;
      this.paginatorConf.pageIndex = data.paginator.index;
      this.paginatorConf.length = data.paginator.Plength;
      if (this.productos.length == 0) {
        this.noFavorites = true;
      }
    } ).add( () => {
      this.spinerIsVisible = false;
    });
  }

  productDetail(id: number) {
    this.router.navigate(['marketplace-detalles-producto/'+id]);
  }

  eliminar(id: number) {
    this.modalDisplay = true;
    this.id_solicitate = id;
  }
  
  page(event){
    this.productos = null;
    this.paginatorConf.pageIndex = event.pageIndex + 1;
    this.paginatorConf.pageSize = event.pageSize;
    this.getFavorites();
  }

}
