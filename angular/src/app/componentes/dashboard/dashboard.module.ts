
import { NavBarComponent } from './perfil/nav-bar/nav-bar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardComponent, ModalCambiarContrasena } from './dashboard.component';
import { BtnNegroComponent } from './perfil/btn-negro/btn-negro.component';
import { BtnRegistrarComponent } from '../nuevo-usuario/btn-registrar/btn-registrar.component';
import { BtnVerdeComponent } from './perfil/btn-verde/btn-verde.component';
import { DatosPersonalesFormComponent } from './perfil/datos-personales-form/datos-personales-form.component';
import { DatosFacturacionFormComponent } from './perfil/datos-facturacion-form/datos-facturacion-form.component';
import { SemblanzaFormComponent } from './perfil/semblanza-form/semblanza-form.component';
import { PerfilComponent, ModalDatosGuardados } from './perfil/perfil/perfil.component';
import { SolicitarComponent } from './solicitar/solicitar.component';
import { ComprarComponent } from './comprar/comprar.component';
import { FavoritosComponent } from './favoritos/favoritos.component';
import { VenderComponent } from './vender/vender.component';
import { ProductoItemComponent } from './vender/producto-item/producto-item.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { ProductosComponent } from './productos/productos.component';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import { ServiciosModule } from '../../servicios/servicios.module';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../../material/material.module';
import { ModalConfirmarComponent } from './vender/modal-confirmar/modal-confirmar.component';



@NgModule({
  imports: [
    CommonModule, 
    ServiciosModule,
    FormsModule,
    MaterialModule
  ],
  declarations: [ 
    BtnNegroComponent,
    BtnVerdeComponent,
    ComprarComponent,
    DashboardComponent,
    DatosPersonalesFormComponent,
    DatosFacturacionFormComponent,
    FavoritosComponent,
    ModalCambiarContrasena,
    ModalDatosGuardados,
    NavBarComponent,
    NavbarComponent,
    PerfilComponent,
    ProductoItemComponent,
    SemblanzaFormComponent,
    SemblanzaFormComponent,
    SolicitarComponent,
    VenderComponent,
    UsuariosComponent,
    ProductosComponent,
    SolicitudesComponent,
    ModalConfirmarComponent
   ],
  entryComponents: [
    ModalCambiarContrasena,
    ModalDatosGuardados
  ],
  exports: [
    BtnNegroComponent,
    BtnVerdeComponent,
    ComprarComponent,
    DashboardComponent,
    DatosPersonalesFormComponent,
    DatosFacturacionFormComponent,
    FavoritosComponent,
    ModalCambiarContrasena,
    ModalDatosGuardados,
    NavBarComponent,
    NavbarComponent,
    PerfilComponent,
    ProductoItemComponent,
    SemblanzaFormComponent,
    SemblanzaFormComponent,
    SolicitarComponent,
    VenderComponent,
    UsuariosComponent,
    ProductosComponent,
    SolicitudesComponent,
    ModalConfirmarComponent
  ]
})
export class DashboardModule { }
