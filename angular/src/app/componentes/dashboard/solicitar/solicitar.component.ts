import { Component, OnInit, Input } from '@angular/core';
import { ProductSchema } from '../../../schemas/productSchema';
import { Router } from '@angular/router';
import { DashboardPersonalService } from '../../../servicios/dashboard-personal.service';

@Component({
  selector: 'app-solicitar',
  templateUrl: './solicitar.component.html',
  styleUrls: ['./solicitar.component.css']
})
export class SolicitarComponent implements OnInit {

  @Input() infoUser:boolean;

  pspinner = {  mode:'indeterminate' };
  spinerIsVisible= true;
  
  productos: ProductSchema[];
  productosAux: ProductSchema[];
  modalDisplay = false;
  legend:string = '';
  method:string = '';
  id_solicitate:number = 0;

  public paginatorConf = {
    length: 0,
    pageSize: 8,
    pageSizeOptions: [ 2, 4, 8, 16 ],
    pageIndex: 0
  };

  constructor(_router:Router, private dashboardService:DashboardPersonalService) { this.router = _router; }

  router: Router;
  ngOnInit() {
    this.dashboardService.retrieveUserSolicitutes()
    .subscribe(
      response => sessionStorage.setItem('solicitar',JSON.stringify(response))
    ).add( () => {

      this.productos = JSON.parse(sessionStorage.solicitar);
      this.productosAux = JSON.parse(sessionStorage.solicitar);
      this.paginatorConf.length = this.productos.length;
      this.productosAux.reverse();
      if(this.productosAux !== void 0 && this.paginatorConf.pageSize == 8) this.getData(0,7);
      if(this.productosAux !== void 0 && this.paginatorConf.pageSize != 8) this.getData(0,this.paginatorConf.pageSize);
      setTimeout(() => this.spinerIsVisible = false ,200);

    });
   }  

  copiar( index:number ){
    this.id_solicitate = this.productos[index].id;
    this.legend = `¿Desea copiar el contenido de ${this.productos[index].nombre}?`;
    this.method = 'copiar-solicitud';
    this.modalDisplay = true;
  }

  eliminar(index) {
    this.id_solicitate =  this.productos[index].id;
    this.legend = `¿Desea eliminar el contenido de ${this.productos[index].nombre}?`;
    this.method = 'eliminar-solicitud';
    this.modalDisplay = true;
  }

  editar(index:number) {
    let id = this.productos[index].id;
    this.router.navigate(['editar-producto-solicitado/'+id])
  }

  closeModal(event){

    if(event){
      this.spinerIsVisible = true;
    }else{
      if( sessionStorage.solicitar !== void 0 ) {
        this.productosAux = JSON.parse(sessionStorage.solicitar);
        this.getData(this.paginatorConf.pageIndex,this.paginatorConf.pageSize);
        this.paginatorConf.length = JSON.parse(sessionStorage.solicitar).length;
        this.productos.reverse()
      }

      this.modalDisplay = false;
      this.spinerIsVisible = false;
      
    }
    
  }
  
  detalle(index:number) {
    let id = this.productos[index].id;
    this.router.navigate(['detalle-producto-solicitado/'+id])
  }

  page(event){
    this.paginatorConf.pageIndex = event.pageIndex;
    this.paginatorConf.pageSize = event.pageSize;
    this.getData(event.pageIndex,event.pageSize);
  }

  getData(pageIndex:number, pageSize:number){
    this.paginatorConf.length = this.productosAux.length;
    if( this.productosAux.length < pageSize ){
      this.productos = [];
      this.productos = this.productosAux.map( (obj) =>({...obj}));
    }else{
      this.productos = this.productosAux.slice( (pageIndex * pageSize), (pageIndex * pageSize) + pageSize );
    }
     
  }

  addNew(){
    window.location.href = 'nuevo-producto-solicitado';
  }
  
}
