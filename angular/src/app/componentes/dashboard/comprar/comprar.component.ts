import { Component, OnInit } from '@angular/core';
import { ProductoItem } from '../vender/producto-item/producto-item';
import { MatDialog } from '@angular/material';
import { MarketService } from '../../../servicios/market.service';
import { Router } from '@angular/router';
import { OfertaSchema } from '../../../schemas/ofertaSchema';

@Component({
  selector: 'app-comprar',
  templateUrl: './comprar.component.html',
  styleUrls: ['./comprar.component.css']
})
export class ComprarComponent implements OnInit {

  public productos: OfertaSchema[];
  public pspinner = {  mode:'indeterminate' };
  spinerIsVisible = true;
  modalDisplay = false;
  legend:string = '';
  method:string = 'delete-sale-confirmation';
  id_solicitate:number = 0;
  type:string = '';

  vacio = true;

  public paginatorConf = {
    length: 16,
    pageSize: 8,
    pageIndex: 0,
    pageSizeOptions: [ 2, 4, 8, 16 ]
  };


  constructor( public dialog: MatDialog, private marketService: MarketService, private router:Router ) {
    this.getCompras();
  }

  closeModal($event){
    if($event){
      this.marketService.eliminaCompra(this.id_solicitate, this.type).subscribe(
        data => {
          let elemento = this.productos.find( (element) : any => {
            return element.id == this.id_solicitate && element.tipo == this.type;
          });
          let i = this.productos.indexOf( elemento );
          if( i > -1 ){
            this.productos.splice( i, 1);
          }
        }
      ).add( () => this.modalDisplay = false );
    }else this.modalDisplay = false;
  }

  getCompras(){
    this.marketService.getCompras(this.paginatorConf.pageIndex, this.paginatorConf.pageSize).subscribe( 
      data => {
        this.spinerIsVisible = data.items.length > 0 ? false:true;
        this.productos = data.items;
        this.paginatorConf.length = data.paginator.length;
        this.paginatorConf.pageIndex = data.paginator.pageIndex;
      }    
    ).add( () => {
      this.spinerIsVisible = false; 
      this.vacio = this.productos.length > 0 ? false : true; 
    });
  }

  getCompra(tipo,id){
    if( tipo == "oferta")
        this.router.navigate([`detalles-compra-producto/${id}`]);
    else if( tipo == "solicitud")
        this.router.navigate([`detalles-compra-solicitud/${id}`]);
    
  }

  eliminar(id, tipo){
    this.id_solicitate = id;
    this.type = tipo;
    let el:any = this.productos.find( (element) => element.id == id && element.tipo == tipo);
    this.legend = '¿Desea eliminar la compra de ' + el.producto.nombre + '?';
    this.modalDisplay = true;
  }


  page(event){
    this.productos = null;
    this.paginatorConf.pageIndex = event.pageIndex + 1;
    this.paginatorConf.pageSize = event.pageSize;
    this.getCompras();
  }

  ngOnInit() {
   sessionStorage.setItem('originBuy','true');
  }




}
