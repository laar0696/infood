export class CompraProductoItem{
    id: number
    precio: string
    nombre: string
    img: string
    variedad: string
    volumen: string
    disponibilidad: string   
    aprobado: boolean
    compra: boolean
}