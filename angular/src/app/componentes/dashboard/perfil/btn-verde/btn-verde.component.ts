import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-btn-verde',
  templateUrl: './btn-verde.component.html',
  styleUrls: ['./btn-verde.component.css']
})
export class BtnVerdeComponent implements OnInit {

  @Input() display:string;

  constructor() { }

  ngOnInit() {
  }

}
