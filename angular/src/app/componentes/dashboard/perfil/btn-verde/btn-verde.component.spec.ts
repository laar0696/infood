import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnVerdeComponent } from './btn-verde.component';

describe('BtnVerdeComponent', () => {
  let component: BtnVerdeComponent;
  let fixture: ComponentFixture<BtnVerdeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnVerdeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnVerdeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
