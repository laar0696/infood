import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnNegroComponent } from './btn-negro.component';

describe('BtnNegroComponent', () => {
  let component: BtnNegroComponent;
  let fixture: ComponentFixture<BtnNegroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnNegroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnNegroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
