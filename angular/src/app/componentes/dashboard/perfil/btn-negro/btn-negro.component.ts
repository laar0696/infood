import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-btn-negro',
  templateUrl: './btn-negro.component.html',
  styleUrls: ['./btn-negro.component.css']
})
export class BtnNegroComponent implements OnInit {

  @Input() display:string;
  
  constructor() { }

  ngOnInit() {
  }

}
