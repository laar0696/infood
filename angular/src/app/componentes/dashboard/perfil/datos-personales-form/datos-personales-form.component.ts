import { Component, OnInit, Input ,Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Personaldataschema } from '../../../../schemas/personaldataschema';
import { DashboardPersonalService } from '../../../../servicios/dashboard-personal.service';
import { Userschema } from '../../../../schemas/userschema';

@Component({
  selector: 'app-datos-personales-form',
  templateUrl: './datos-personales-form.component.html',
  styleUrls: ['./datos-personales-form.component.css']
})
export class DatosPersonalesFormComponent implements OnInit {

  public imgPerfil = 'assets/img/avatar.png';
  public dataSchema:Userschema;
  public userType:any;
  public deshabilitado;
  public errors = [];
  public ajeno: boolean;
  public user_types = [];

  @Input() infoUser:boolean;
  @ViewChild('inputImage') imagePut : ElementRef;

  constructor( private dashboardService:DashboardPersonalService ) {
    this.dataSchema = {
      user_id: 0,
      wp_user_id:0,
      nickname: '',
      first_name:'',
      last_name:'',
      secont_last_name:'',
      billing_email:'',
      email_alt:'',
      enterprise_phone:'',
      phone:'',
      celphone:'',
      billing_name:'',
      rfc:'',
      cp:'',
      country_id:0,
      billing_address:'',
      billing_phone:'',
      comertial_name:'',
      semblance:'',
      profile_avatar: '',
      enterprise_avatar: '',
      image_profile:'',
      image_enterprise:'',
      administrador: 0,
      user_type: 0
    }

    this.user_types = JSON.parse( sessionStorage.getItem('users_type'));
    this.dataSchema.user_type = JSON.parse( sessionStorage.getItem('user_type'));
  }

  ngOnChanges(){
    if(this.infoUser){
      this.dataSchema = sessionStorage.userData?JSON.parse(sessionStorage.userData):null;      
      this.dataSchema.administrador = JSON.parse( sessionStorage.getItem('admin_user_editando'));
      this.dataSchema.estatus = JSON.parse( sessionStorage.getItem('estatus_user_editando'));
      this.userType = sessionStorage.userType;
      this.imgPerfil = this.dataSchema.profile_avatar;
    }
  }
  
  ngOnInit() {
    this.ajeno = JSON.parse( sessionStorage.getItem('ajeno') );
  }

  simulateImageEnter(){
    this.imagePut.nativeElement.click();
  }

  previewPerfilImage(event,username,email,email2,phone,phone2) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.imgPerfil = event.target.result;
        this.dataSchema.profile_avatar = event.target.result;
        this.dataSchema.image_profile = event.target.result;
        this.saveSchema(username,email,email2,phone,phone2);
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  saveSchema(username,email,email2,phone,phone2){
    if (username.errors || email.errors || email2.errors || phone.errors || phone2.errors) {
      sessionStorage.setItem('validForm', 'false');      
    } else {
      sessionStorage.setItem('validForm', 'true');
    }
    
    sessionStorage.setItem('userData',JSON.stringify(this.dataSchema));
    console.log( sessionStorage.getItem('userData'));
  }

  userStatus(){
    this.dashboardService.changeStatus(JSON.parse(sessionStorage.userData).user_id).subscribe();
  }

  userStatusAdmin(){
    this.dashboardService.changeStatusAdmin(JSON.parse(sessionStorage.userData).user_id).subscribe();
  }

  cambiarTipoUsuario(){
    sessionStorage.setItem('userData',JSON.stringify(this.dataSchema));
  }


}
