import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef } from '@angular/core';
import { Userschema } from '../../../../schemas/userschema';

@Component({
  selector: 'app-semblanza-form',
  templateUrl: './semblanza-form.component.html',
  styleUrls: ['./semblanza-form.component.css']
})

export class SemblanzaFormComponent implements OnInit {

  @Input() infoUser: boolean;
  infoData:Userschema;

  constructor() {
    this.infoData = {
      user_id: 0,
      wp_user_id:0,
      nickname: '',
      first_name:'',
      last_name:'',
      secont_last_name:'',
      billing_email:'',
      email_alt:'',
      enterprise_phone:'',
      phone:'',
      celphone:'',
      billing_name:'',
      rfc:'',
      cp:'',
      country_id:0,
      billing_address:'',
      billing_phone:'',
      comertial_name:'',
      semblance:'',
      profile_avatar: '',
      enterprise_avatar: '',
      image_profile:'',
      image_enterprise:'',
      user_type: 0
    }
  }

  ngOnChanges() {
    this.infoUser ? this.infoData = JSON.parse(sessionStorage.userData) : '';
  }

  ngOnInit() { }

  saveSchema(){
    sessionStorage.setItem('userData',JSON.stringify(this.infoData));
  }

}
