import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemblanzaFormComponent } from './semblanza-form.component';

describe('SemblanzaFormComponent', () => {
  let component: SemblanzaFormComponent;
  let fixture: ComponentFixture<SemblanzaFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemblanzaFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemblanzaFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
