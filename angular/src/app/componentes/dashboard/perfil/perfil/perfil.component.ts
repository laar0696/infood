import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Userschema } from '../../../../schemas/userschema';
import { DashboardPersonalService } from '../../../../servicios/dashboard-personal.service';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})

export class PerfilComponent implements OnInit {
  pspinner = {  mode:'indeterminate' };
  spinerIsVisible= false;

  datosPersonales = true;
  datosFacturacion = false;
  datosSemblanza = false;

  @Input() infoUser:boolean;

  userSchema: Userschema;
  userId;
  userType;
  info = false;

  constructor(private dashboardService:DashboardPersonalService, public dialog: MatDialog) { }

  openFormulary(open:string){

    switch(open){
      case 'personal':
      this.datosFacturacion = false;
      this.datosSemblanza = false;
      this.datosPersonales = true;
      break;

      case 'factura':
      this.datosFacturacion = true;
      this.datosSemblanza = false;
      this.datosPersonales = false;
      break;

      case 'semblanza': 
      this.datosFacturacion = false;
      this.datosSemblanza = true;
      this.datosPersonales = false;  
      break;
    }
  }

  ngOnInit() {}

  ngOnChanges(){
    if(this.infoUser){
      sessionStorage.setItem('userDataCopy',sessionStorage.userData);
      this.info = true;
    }
  }

  updateDataInfo(){
    sessionStorage.validForm = sessionStorage.validForm == 'undefined' ? false : sessionStorage.validForm;
    if ( JSON.parse( sessionStorage.validForm ) ) {
      this.spinerIsVisible = true;
      this.userSchema = JSON.parse(sessionStorage.userData);
      this.userId = sessionStorage.getItem('userId');
      this.userType = sessionStorage.getItem('userType');

      if (this.userType && this.userType == 1 && this.userId != this.userSchema.wp_user_id) {
        this.dashboardService.updateUserData(this.userSchema, this.userId).subscribe(
          res =>  {
            let dialogRef = this.dialog.open(ModalDatosGuardados, {
              width: '600px',
            });
          }
        ).add( () => this.spinerIsVisible = false);
      } else {
        this.dashboardService.updatePersonalData(this.userSchema).subscribe(
          res =>{
            let dialogRef = this.dialog.open(ModalDatosGuardados, {
              width: '600px',
            });
            dialogRef.afterClosed().subscribe(result => {
              window.location.reload();
            });
          }
        ).add( () => this.spinerIsVisible = false);
      }
    }
  }

}


@Component({
  selector: 'modal-datos-guardados',
  templateUrl: 'modal-datos-guardados.html'
})

export class ModalDatosGuardados {
  
  closeDialog(): void {
    window.location.reload();
  }

  constructor(public dialog: MatDialog) {}

  savePassword(){
    window.location.reload();
  }
}