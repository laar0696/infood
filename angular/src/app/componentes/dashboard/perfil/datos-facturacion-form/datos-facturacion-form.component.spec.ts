import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosFacturacionFormComponent } from './datos-facturacion-form.component';

describe('DatosFacturacionFormComponent', () => {
  let component: DatosFacturacionFormComponent;
  let fixture: ComponentFixture<DatosFacturacionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosFacturacionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosFacturacionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
