import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Userschema } from '../../../../schemas/userschema';

@Component({
  selector: 'app-datos-facturacion-form',
  templateUrl: './datos-facturacion-form.component.html',
  styleUrls: ['./datos-facturacion-form.component.css']
})
export class DatosFacturacionFormComponent implements OnInit {

  @Input() infoUser:boolean;
  @ViewChild('inputImage') imagePut : ElementRef;
  @ViewChild('selectState') selectState : ElementRef;

  public imgEmpresa = 'assets/img/empresa.png';
  public infoData: Userschema;
  public countries: any;
  public states: any;
  public idCountry: any;
  public idState: any;
  constructor() {
    this.idCountry = 0;
    this.infoData = {
      user_id: 0,
      wp_user_id:0,
      nickname: '',
      first_name:'',
      last_name:'',
      secont_last_name:'',
      billing_email:'',
      email_alt:'',
      enterprise_phone:'',
      phone:'',
      celphone:'',
      billing_name:'',
      rfc:'',
      cp:'',
      country_id:0,
      billing_address:'',
      billing_phone:'',
      billing_company:'',
      comertial_name:'',
      semblance:'',
      profile_avatar: '',
      enterprise_avatar: '',
      image_profile:'',
      image_enterprise:'',
      user_type: 0
    }

  }
   
  ngOnChanges(){
    if(this.infoUser){
      this.infoData = JSON.parse(sessionStorage.userData);
      this.imgEmpresa = this.infoData.enterprise_avatar;
      this.countries = JSON.parse(sessionStorage.countries);
      // this.states = [];
    }
  }

  ngOnInit() {
    this.chargeStates();
  }

  simulateImageEnter(){
    this.imagePut.nativeElement.click();
  }

  previewEmpresaImage(event) {  
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.imgEmpresa = event.target.result;
        this.infoData.enterprise_avatar = event.target.result;
        this.infoData.image_enterprise = event.target.result;
        this.saveSchema();
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  saveSchema(){
    sessionStorage.setItem('userData',JSON.stringify(this.infoData));
  }

  chargeStates(){
    this.countries.forEach(country => {
      if (this.infoData.country_id != 0 && country.id == this.infoData.country_id) {
        this.states = country.states;
        return;
      } else if(this.infoData.country_id == 0) {
        this.states = [];
      }
    });
  }
 
}
