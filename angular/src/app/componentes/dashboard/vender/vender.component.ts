import { Component, OnInit, Input } from '@angular/core';
import { ProductSchema } from '../../../schemas/productSchema';
import { VenderService } from '../../../servicios/vender.service';
import { Router } from '@angular/router';
import { DashboardPersonalService } from '../../../servicios/dashboard-personal.service';

@Component({
  selector: 'app-vender',
  templateUrl: './vender.component.html',
  styleUrls: ['./vender.component.css']
})
export class VenderComponent implements OnInit {

  pspinner = {  mode:'indeterminate' };
  spinerIsVisible= true;

  productos: ProductSchema[];
  productosAux: ProductSchema[];
  
  modalDisplay = false;
  legend:string = '';
  method:string = '';
  id_solicitate:number = 0;

  public paginatorConf = {
    length: 0,
    pageSize: 8,
    pageSizeOptions: [ 2, 4, 8, 16 ],
    pageIndex: 0
  };

  @Input() infoUser:boolean;

  constructor(router:Router,private dashboardService:DashboardPersonalService) { this.routes = router; }
  routes:Router;

  ngOnInit() {

    this.dashboardService.retrieveUserApplications()
      .subscribe( res => sessionStorage.setItem('vender',JSON.stringify(res)) )   
      .add(() => {
        this.productos = JSON.parse(sessionStorage.vender);
        this.productosAux = JSON.parse(sessionStorage.vender).reverse();
        this.paginatorConf.length = this.productos.length;
        if(this.productosAux !== void 0 && this.paginatorConf.pageSize == 8) this.getData(0,7);
        if(this.productosAux !== void 0 && this.paginatorConf.pageSize != 8) this.getData(0,this.paginatorConf.pageSize);
        setTimeout(() => {
          this.spinerIsVisible = false
        }, 200);
      } );
    
  }

  copiar( index: number ){
    this.id_solicitate = this.productos[index].id;
    this.legend = `¿Desea copiar el contenido de ${this.productos[index].nombre}?`;
    this.method = 'copiar';
    this.modalDisplay = true;
  }

  eliminar( index:number ) {
    this.id_solicitate =  this.productos[index].id;
    this.legend = `¿Desea eliminar el contenido de ${this.productos[index].nombre}?`;
    this.method = 'eliminar';
    this.modalDisplay = true;
  }

  editar( index:number ) {
    let id = this.productos[index].id;
    this.routes.navigate(['editar-producto-venta/'+id])
  }

  detalle( index:number ) {
    let id = this.productos[index].id;
    this.routes.navigate(['detalle-producto-venta/'+id])
  }

  page(event){
    this.paginatorConf.pageIndex = event.pageIndex;
    this.paginatorConf.pageSize = event.pageSize;
    this.getData(event.pageIndex,event.pageSize);
  }

  getData(pageIndex:number, pageSize:number){
    if( this.productosAux.length < pageSize ){
      this.productos = [];
      this.productos = this.productosAux.map( (obj) =>({...obj}));
    }else{
      this.productos = this.productosAux.slice( (pageIndex * pageSize), (pageIndex * pageSize) + pageSize );
    }
  }

  closeModal(event){
    if(event){
      this.spinerIsVisible = true;
    }else{

      if( sessionStorage.vender !== void 0 ) {
        this.productosAux = JSON.parse(sessionStorage.vender);
        this.getData(this.paginatorConf.pageIndex,this.paginatorConf.pageSize);
        this.paginatorConf.length = JSON.parse(sessionStorage.vender).length;
        this.productos.reverse();
      }

      this.spinerIsVisible = false;
      this.modalDisplay = false;
    }
  }

  addNew(){
    window.location.href = 'nuevo-producto-venta';
  }

}
