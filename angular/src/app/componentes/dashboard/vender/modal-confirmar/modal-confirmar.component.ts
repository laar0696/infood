import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { VenderService } from '../../../../servicios/vender.service';

@Component({
  selector: 'app-modal-confirmar',
  templateUrl: './modal-confirmar.component.html',
  styleUrls: ['./modal-confirmar.component.css']
})
export class ModalConfirmarComponent implements OnInit {

  constructor(private venderHttp:VenderService) { }

  @Input() method:string;
  @Input() legend:string;
  @Input() id:number;

  @Output('display') display = new EventEmitter<boolean>();

  isConfirm: boolean = false;

  ngOnInit() {
    sessionStorage.autorizedTransactionInfood = 'true';
  }

  doActions(){
    this.method === 'copiar' ? this.copyCase() :  
    this.method === 'eliminar' ? this.deleteCase() : 
    this.method === 'copiar-solicitud' ? this.copyCaseSolicitude() :
    this.method === 'eliminar-solicitud' ? this.deleteCaseSolicitude() : 
    this.method === 'eliminar-producto-administrador' ? this.deleteCaseApplicationAdmin() : 
    this.method === 'eliminar-solicitud-administrador' ? this.deleteCaseSolicitudesAdmi() :
    this.method === 'quitar-producto-favorito' ? this.emmitTrue() : 
    this.method === 'delete-order-confirmation' ? this.emmitTrue() :
    this.method === 'delete-sale-confirmation' ? this.emmitTrue() :
    this.method === 'delete-offert-confirmation' ? this.emmitTrue()
    : '' ;  
  }

  copyCase(){
    this.emmitTrue();
    this.venderHttp.copyApplication(this.id)
      .subscribe( res => sessionStorage.vender = JSON.stringify(res)).add(()=>this.display.emit(false));
  }

  deleteCase(){
    this.emmitTrue();
    this.venderHttp.deleteApplication(this.id)
      .subscribe( res => sessionStorage.vender = JSON.stringify(res) ).add( () => this.display.emit(false));
  }

  copyCaseSolicitude(){
    this.emmitTrue();
    this.venderHttp.copySolicitude(this.id)
      .subscribe( res => sessionStorage.solicitar = JSON.stringify(res) ).add( () => this.display.emit(false));
  }

  deleteCaseSolicitude(){
    this.emmitTrue();
    this.venderHttp.deleteSolicitude(this.id)
      .subscribe( res => sessionStorage.solicitar = JSON.stringify(res) ).add( () => this.display.emit(false) );
  }

  deleteCaseApplicationAdmin(){
    this.emmitTrue();
    this.venderHttp.deleteApplicationAdministrative(this.id)
    .subscribe( res => sessionStorage.totalProduct = JSON.stringify(res) )
    .add( () => this.display.emit(false) );
  }

  deleteCaseSolicitudesAdmi(){
    this.emmitTrue();
    this.venderHttp.deleteSolicitudeAdministrative(this.id)
    .subscribe( res => sessionStorage.totalSolicitutes = JSON.stringify(res) )
    .add( () => this.display.emit(false) );
  }

  emmitTrue(){
    this.display.emit(true);
    this.isConfirm = true;
  }

  closeDialog(){
    sessionStorage.autorizedTransactionInfood = 'false';
    this.display.emit(false);
  }

}
