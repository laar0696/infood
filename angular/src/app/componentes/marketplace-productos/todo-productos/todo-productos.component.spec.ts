import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoProductosComponent } from './todo-productos.component';

describe('TodoProductosComponent', () => {
  let component: TodoProductosComponent;
  let fixture: ComponentFixture<TodoProductosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoProductosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoProductosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
