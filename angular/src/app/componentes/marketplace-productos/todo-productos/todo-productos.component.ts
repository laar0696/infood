import { Component, OnInit } from '@angular/core';
import { MarketService } from '../../../servicios/market.service';
import { Observable } from 'rxjs';
import { OfertaSchema } from '../../../schemas/ofertaSchema';
import { CategoriaSchema } from '../../../schemas/categoriaSchema';
import { ProductoSchema } from '../../../schemas/productoSchema';
import { EstadoSchema } from '../../../schemas/estadoSchema';
import { CertificacionSchema } from '../../../schemas/certificacionSchema';
import { IMyDpOptions } from 'mydatepicker';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { PageEvent, MatPaginatorIntl } from '@angular/material';

@Component({
  selector: 'app-todo-productos',
  templateUrl: './todo-productos.component.html',
  styleUrls: ['./todo-productos.component.css']
})
export class TodoProductosComponent implements OnInit {

  public pspinner = {  mode:'indeterminate' };

  public cualquier : { 'id' : 0};

  public filtroModal = false;

  public paginatorConf = {
    length: 0,
    pageSize: 8,
    pageIndex: 0,
    pageSizeOptions: [ 2, 4, 8, 16 ]
  };

  public filtros = {
    categoria: this.cualquier,
    producto: this.cualquier,
    estado: this.cualquier,
    certificacion: this.cualquier,
    fecha: null,
    precio: 0,
    busqueda: null,
    paginador: {     
      length: 0,
      pageIndex: 0,
      pageSize:4
    }
  };

  public siteUrl: string = environment.siteUrl;
  
  public maxPrice;

  public ofertas: OfertaSchema[]
  public categorias: CategoriaSchema[]
  public productos: ProductoSchema[]
  public estados: EstadoSchema[]
  public certificaciones: CertificacionSchema[]


  constructor(private marketService: MarketService, private route: ActivatedRoute, private router: Router) {

    this.filtros.busqueda = this.route.snapshot.params['buscar'];
  
    
    this.getOfertas();

    this.getCategorias();
    
    this.getEstados();

    this.getCertificaciones();

    this.getProductos();

    let date = new Date();
    

  }


  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
  };

  redirect(to:number) {
    switch (to) {
      case 0:
        this.router.navigate(['/']);
        break;
      case 1:
        window.location.reload();
        break;
    }
  }

  closeModal(){
    this.filtroModal = false;
  }

  alternaModal(){
    this.filtroModal = this.filtroModal?false:true;
  }

  getCategorias(){
    this.marketService.getCategorias().subscribe( data => {
      this.categorias = data;
    });
  }

  getProductos(){
    this.marketService.getProductos( this.filtros.categoria ).subscribe( data => {
      this.productos = data;
    });
  }

  getEstados(){
    this.marketService.getEstados().subscribe( data => {
      this.estados = data;
    });
  }

  getCertificaciones(){
    this.marketService.getCertificaciones().subscribe( data => {
      this.certificaciones = data;
    });
  }

  productor (id) {
    this.router.navigate(['productor-detalle/'+id]);
  }

  getOfertas(){
    this.ofertas = null;
    this.marketService.getOfertas(this.filtros).subscribe( data => {
      this.ofertas = data.items;
      this.maxPrice = data.paginator.maxPrice?data.paginator.maxPrice:0;
      this.paginatorConf.length = data.paginator.length;
    });
  }

  buscar() {
    this.filtros.paginador.pageIndex = 0;
    this.paginatorConf.pageIndex = 0;
    this.getOfertas();
  }

  solicitudes(){
    sessionStorage.setItem('displayInfood','solicitar')
    this.router.navigate(['/dashboard']);
  }

  detalleProducto(id:number){
    this.router.navigate([`marketplace-detalles-producto/${id}`]);
  }

  page(event){
    if (event.pageSize != this.paginatorConf.pageSize) {
      this.paginatorConf = event;
      this.paginatorConf.pageSizeOptions = [ 2, 4, 8, 16 ];
      this.paginatorConf.pageIndex = 0;
      this.filtros.paginador = this.paginatorConf;
    } else {
      this.paginatorConf = event;
      this.paginatorConf.pageSizeOptions = [ 2, 4, 8, 16 ];
      this.filtros.paginador = event;
    }
    this.getOfertas();
  }

  addProductoFavorito(producto_id, estado){
    let favorito = { 'id': producto_id, 'estado':!estado };
    this.marketService.addProductoFavorito( favorito ).subscribe();
  }

  ngOnInit() {
    sessionStorage.originBuy = "false";
  }

  ngOnDestroy(){
    sessionStorage.removeItem('originBuy');
  }

}
