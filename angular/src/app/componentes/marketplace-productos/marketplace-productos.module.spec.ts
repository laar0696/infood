import { MarketplaceProductosModule } from './marketplace-productos.module';

describe('MarketplaceProductosModule', () => {
  let marketplaceProductosModule: MarketplaceProductosModule;

  beforeEach(() => {
    marketplaceProductosModule = new MarketplaceProductosModule();
  });

  it('should create an instance', () => {
    expect(marketplaceProductosModule).toBeTruthy();
  });
});
