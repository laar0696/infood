import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VistaProductosComponent } from './vista-productos/vista-productos.component';
import { DetallesProductoComponent, ModalComprobacionOferta, ModalVerCalificaciones } from './detalles-producto/detalles-producto.component';
import { MyDatePickerModule } from 'mydatepicker';
import { TodoProductosComponent } from './todo-productos/todo-productos.component';
import { DashboardModule } from '../dashboard/dashboard.module';
import { MarketplaceSolicitudesModule } from '../marketplace-solicitudes/marketplace-solicitudes.module';
import { SchemasModule } from '../../schemas/schemas.module';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../../material/material.module';
import { DetallesCompraProductoComponent, ModalCalificacion } from './detalles-compra-producto/detalles-compra-producto.component';
import {AngularOpenlayersModule} from 'ngx-openlayers';

@NgModule({
  imports: [
    CommonModule,  MyDatePickerModule, DashboardModule, MarketplaceSolicitudesModule, SchemasModule, FormsModule, MaterialModule, AngularOpenlayersModule
  ],
  entryComponents: [
    ModalComprobacionOferta,
    ModalCalificacion,
    ModalVerCalificaciones
  ],
  declarations: [ ModalVerCalificaciones, ModalCalificacion, VistaProductosComponent, DetallesProductoComponent,TodoProductosComponent, ModalComprobacionOferta, DetallesCompraProductoComponent]
})
export class MarketplaceProductosModule { }
