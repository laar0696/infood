import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OfertaSchema } from '../../../schemas/ofertaSchema';
import { MarketService } from '../../../servicios/market.service';
import { Observable } from 'rxjs';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { inject } from '@angular/core/testing';



@Component({
  selector: 'app-detalles-producto',
  templateUrl: './detalles-producto.component.html',
  styleUrls: ['./detalles-producto.component.css']
})
export class DetallesProductoComponent implements OnInit {

  next:string;
  prev:string;
  position:number;

  public id:number;
  public oferta:OfertaSchema;
  public pedido = { cantidad: 1, total: 0 };
  public pspinner = {  mode:'indeterminate' };
  public ofertasSimilares: OfertaSchema[] = [];
  public imageUrls: string [] = [];
  public actualImage;
  public spinnerIsVisible = true;
  public producto = '';

  @ViewChild('imagenesOferta') public gallery: any;

  redirect(to: number) {
    switch (to) {
      case 0:
        this.router.navigate(['/']);
        break;
      case 1:
        if(sessionStorage.originFavorites === void 0){
          this.router.navigate(['marketplace-busqueda-productos']);
        }else{
          sessionStorage.displayInfood = 'favoritos';
          sessionStorage.removeItem('originFavorites');
          this.router.navigate(['dashboard']);
        }
        break;
    }
  }

  constructor( public dialog: MatDialog, private marketService: MarketService, private route:ActivatedRoute, private router:Router ) {
    this.id = this.route.snapshot.params['id'];
    this.getOferta();
   }

   public paginatorConf = {
    length: 0,
    pageSize: 8,
    pageIndex: 0,
    pageSizeOptions: [ 2, 4, 8, 16 ]
   };

   getOferta(){
    this.oferta = null;
    this.imageUrls = [];
    window.scrollTo(0, 0);
    this.marketService.getOferta( this.id ).subscribe( data => {
      this.oferta = data;
      if( this.ofertasSimilares.length == 0) 
        this.getOfertasSimilares( this.oferta.producto_id );
      this.setImagenes( data.imagenes );
    }).add( () => this.spinnerIsVisible = false ); 
   }

  setImagenes( imagenes:any[] ){
    this.gallery = imagenes;
    this.actualImage = this.gallery[0]?this.gallery[0].imagen:null;
  }

  changeImage(i:number){
    this.actualImage = this.gallery[i].imagen;
    this.position = i;
  }

  nextImage(){
    if( this.position < this.gallery.length-1 ){
      this.position++;
      this.actualImage = this.gallery[this.position].imagen;
    } 
  }

  prevImage(){
    if(this.position > 0){
      this.position--;
      this.actualImage = this.gallery[this.position].imagen;
    }
  }

   getOfertasSimilares(categoria_id){
     this.marketService.getOfertasSimilares( categoria_id, this.paginatorConf.pageIndex, this.paginatorConf.pageSize ).subscribe( data => {
       this.ofertasSimilares = data.data;
       this.paginatorConf.length = data.total;
     });
   }

  productor (id) {
    this.router.navigate(['productor-detalle/'+id]);
  }

   page(event){
    this.ofertasSimilares = [];
    this.paginatorConf.pageIndex = event.pageIndex + 1;
    this.paginatorConf.pageSize = event.pageSize;
    this.getOfertasSimilares(this.oferta.producto_id);    
   }

   detalleProducto(id:number){
    this.id = id;
    this.getOferta(); 
  }
   
  agregarCompra(){  
    this.spinnerIsVisible = true;
    this.marketService.compraOferta(this.oferta.id, this.pedido.cantidad).subscribe(data => {
      this.spinnerIsVisible = false;
      sessionStorage.idProduct = JSON.stringify(data.msg);
      
    }).add( () => this.openDialog() );
  }

  addProductoFavorito(producto_id, estado){
    let favorito = { 'id': producto_id, 'estado':!estado };
    this.marketService.addProductoFavorito( favorito ).subscribe();
  }


  openDialog(): void {
    let dialogRef = this.dialog.open(ModalComprobacionOferta, {
      width: '600px',
    });
  }

  verCalificaciones(){
    let modalC = this.dialog.open( ModalVerCalificaciones, {
      width: '600px',
      data : {'oferta_id': this.oferta.id }
    });    
  }


  ngOnInit() {
    this.next = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADxSURBVEhLtdQ7akJRFEbhq1ELo3E04lshRYo0goWtY7G2sLDRxsJKCwdg55tMJWkSX4ggmLVB4bCxEM52wYdg8W/wXgweKHr9NC+GMX5RkS+sG+FydUAZZoXxjdsBsUcJZmXxB/fIDkWYVYCMuke2MD0iv708A31Ejpv1Dn1kgzzM+sAR7hF5RjmY9YkT9JEMzKri3pE0zKrDPSB+EIF3LxhCH5jAOxkfQI9PEYdXIfShx2cwGe9Bj8/xCq9kvAM9vkAC3rWhx5dIwrsWnjbehB5f4Q3eyeso/5ju+BopmNXAGTL+BdPxWzV08ZRxg4LgH/UwaZ/ZnR8/AAAAAElFTkSuQmCC';
    this.prev = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADuSURBVEhLtdQ7akJRFIXha2ETBEkRSJfKGQiSGMXGwsJCJ+BkNEjwUaXMYATBQrAQUosz8Ikogvk3GDhsCAhn+cOHYLG2esEksvT1VV4KPezRsjeU2fgQl6szGpAVjv/pQ9IAfnyEB0Rnv7kfHyOD6D7hxyfIIrou/PgUkvEP+PEZHhFdB358jidE14Yf/4FkvA4/vsQzJOWwQnjghCZkFeCPHGHfTtZ/R2qQ9Qp/5IAqZL1hjfCI/U1XIKuIDfyREmS9Y4vwiB21byjLPvEO4RF7RnnIKsMf+YI0O2LPwMYXeIE8e/DfuMv4jSXJL02WaZRNaaEmAAAAAElFTkSuQmCC';

    this.position = 0;

    this.producto = sessionStorage.originFavorites === void 0 ? 'Productos' : 'Favoritos';  
  }
    
}

/* Modal comprobacion compra */


@Component({
  selector: 'modal-comprobacion-oferta',
  templateUrl: 'modal-comprobacion-oferta.html',
})
export class ModalComprobacionOferta{
  
  closeDialog(): void {
    let dialogRef = this.dialog.closeAll();
    let id = JSON.parse(sessionStorage.idProduct);
    this.route.navigate([`detalles-compra-producto/${id}`]);
  }

  constructor(public dialog: MatDialog, private route:Router ) {}

}

@Component({
  selector: 'modal-ver-calificaciones',
  templateUrl: 'modal-ver-calificaciones.html',
  styleUrls: ['./modal-ver-calificaciones.css']
})
export class ModalVerCalificaciones{
  
  public calificaciones: any[];
  public vacio = false;
  public cargando = true;
  public pspinner = {  mode:'indeterminate' };

  closeDialog(): void {
    let dialogRef = this.dialog.closeAll();
  }

  getCalificacioines( oferta_id ){
    this.cargando = true;
    this.market.getCalificaciones( oferta_id ).subscribe(
      (data) => { 
        this.calificaciones = data.items; 
        this.cargando = false;
        this.vacio = data.items.length == 0;
      }
    );
  }

  constructor( public market:MarketService, public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data:any ) {
    this.getCalificacioines(data.oferta_id);
  }

}