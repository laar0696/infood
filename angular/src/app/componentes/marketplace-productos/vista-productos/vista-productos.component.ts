import { Component, OnInit } from '@angular/core';
import { OfertaSchema } from '../../../schemas/ofertaSchema';
import { MarketService } from '../../../servicios/market.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-vista-productos',
  templateUrl: './vista-productos.component.html',
  styleUrls: ['./vista-productos.component.css']
})
export class VistaProductosComponent implements OnInit {

  public ofertas: OfertaSchema[];

  public siteUrl: string = environment.siteUrl;
  
  constructor(private marketService: MarketService) {
    this.marketService.getOfertasHome().subscribe( data => {
      this.ofertas = data;
    });
  }

  ngOnInit() {
  }

}
