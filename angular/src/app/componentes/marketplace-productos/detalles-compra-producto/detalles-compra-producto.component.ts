import { Component, OnInit, ViewChild, Input, Inject } from '@angular/core';
import { OfertaSchema } from '../../../schemas/ofertaSchema';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { MarketService } from '../../../servicios/market.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CompraSchema } from '../../../schemas/compraSchema';
import { ComentarioSchema } from '../../../schemas/comentarioSchema';
import { AuthService } from '../../../servicios/auth.service';



@Component({
  selector: 'app-detalles-compra-producto',
  templateUrl: './detalles-compra-producto.component.html',
  styleUrls: ['./detalles-compra-producto.component.css']
})
export class DetallesCompraProductoComponent implements OnInit {
  public id:number;

  public compra: CompraSchema = null;

  public oferta: OfertaSchema = null;

  public pspinner = {  mode:'indeterminate' };

  public comentario: string = null;

  public imageUrls: string [] = [];

  public completado: boolean = false;

  public routes: Router;


  public prev:string;
  public next:string;
  private position:number;
  public imageCarrouselPrev:string;

  @ViewChild('imagenesOferta') public gallery: any;


  constructor( public auth:AuthService, public dialog: MatDialog, private marketService: MarketService, private route:ActivatedRoute, private router:Router ) {
    this.id = this.route.snapshot.params['id'];
    this.getCompraOferta();
    this.routes = router;

    this.position = 0;
    this.next = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADxSURBVEhLtdQ7akJRFEbhq1ELo3E04lshRYo0goWtY7G2sLDRxsJKCwdg55tMJWkSX4ggmLVB4bCxEM52wYdg8W/wXgweKHr9NC+GMX5RkS+sG+FydUAZZoXxjdsBsUcJZmXxB/fIDkWYVYCMuke2MD0iv708A31Ejpv1Dn1kgzzM+sAR7hF5RjmY9YkT9JEMzKri3pE0zKrDPSB+EIF3LxhCH5jAOxkfQI9PEYdXIfShx2cwGe9Bj8/xCq9kvAM9vkAC3rWhx5dIwrsWnjbehB5f4Q3eyeso/5ju+BopmNXAGTL+BdPxWzV08ZRxg4LgH/UwaZ/ZnR8/AAAAAElFTkSuQmCC';
    this.prev = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADuSURBVEhLtdQ7akJRFIXha2ETBEkRSJfKGQiSGMXGwsJCJ+BkNEjwUaXMYATBQrAQUosz8Ikogvk3GDhsCAhn+cOHYLG2esEksvT1VV4KPezRsjeU2fgQl6szGpAVjv/pQ9IAfnyEB0Rnv7kfHyOD6D7hxyfIIrou/PgUkvEP+PEZHhFdB358jidE14Yf/4FkvA4/vsQzJOWwQnjghCZkFeCPHGHfTtZ/R2qQ9Qp/5IAqZL1hjfCI/U1XIKuIDfyREmS9Y4vwiB21byjLPvEO4RF7RnnIKsMf+YI0O2LPwMYXeIE8e/DfuMv4jSXJL02WaZRNaaEmAAAAAElFTkSuQmCC';

   }

   changeImage(i:number){
    this.imageCarrouselPrev = this.imageUrls[i];
    this.position = i;
  }

  nextImage(){
    if( this.position < this.imageUrls.length-1 ){
      this.position++;
      this.imageCarrouselPrev = this.imageUrls[this.position];
    } 
  }

  prevImage(){
    if(this.position > 0){
      this.position--;
      this.imageCarrouselPrev = this.imageUrls[this.position];
    }
  }

   completar(){
     this.marketService.setEstatusCompraOferta(this.id, this.completado).subscribe(
      data => {}
     );
   }


   getCompraOferta(){

    this.marketService.getCompraOferta( this.id ).subscribe( data => {
      this.compra = data.items[0];
      this.completado = this.compra.completado == 1;
      
      this.oferta = this.compra.oferta;
      if( !this.compra.comentarios ){
        this.compra.comentarios = [];
      } 
      this.setImagenes( data.items[0].oferta.imagenes );
      window.scrollTo(0, 0);
    }).add(() => this.imageCarrouselPrev = this.imageUrls[0] ); 
   }

   setImagenes( imagenes:any[] ){
      Observable.concat(imagenes).subscribe({
        next: v => this.imageUrls.push(v.imagen),
        complete: () => { 
          if( this.gallery ){
            this.gallery.imageUrls = this.imageUrls;
            this.imageCarrouselPrev = this.imageUrls[0];
          }  
        }
      });      
   }


   enviarComentario(){
     if( this.isVoid(this.comentario) ){
       return false;
     }
     this.marketService.addComentarioCompraOferta( this.id, this.comentario ).subscribe( data => {
       this.comentario = null;
       this.compra.comentarios.push( data.items[0] ); 
     });
   }

   isVoid(cad:string):boolean{
    if( cad == null ) return true;
    return cad.replace(' ','').length == 0;
   }

  calificar(){
    this.dialog.open( ModalCalificacion, {
       width:'500px',
       data: {'compra_id': this.id } 
      });
  }

  ngOnInit() {
    
  }

  redirect(to:number){
    switch(to){
      case 0:
        this.routes.navigate(['dashboard']);
      break;
      case 1:
        sessionStorage.setItem('displayInfood','comprar');
        this.routes.navigate(['dashboard']);
      break;
    }
  }

}

@Component({
  selector: 'modal-calificar',
  templateUrl: 'calificar.html',
  styleUrls: ['calificar.css'],
})
export class ModalCalificacion{

  public compra_id:number;  
  public calificacion = 0;
  public comentario:string;

  closeDialog(): void {
    let dialogRef = this.dialog.closeAll();
  }

  calificar(){
    this.market.addCalificacionCompra(this.compra_id, this.calificacion, this.comentario).subscribe();
    this.closeDialog();
  }

  constructor(public dialog: MatDialog, private market:MarketService, @Inject(MAT_DIALOG_DATA) public data: any ) {
    this.compra_id = data.compra_id;
  }

}