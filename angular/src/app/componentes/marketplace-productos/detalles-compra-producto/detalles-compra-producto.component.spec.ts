import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesCompraProductoComponent } from './detalles-compra-producto.component';

describe('DetallesCompraProductoComponent', () => {
  let component: DetallesCompraProductoComponent;
  let fixture: ComponentFixture<DetallesCompraProductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallesCompraProductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesCompraProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
