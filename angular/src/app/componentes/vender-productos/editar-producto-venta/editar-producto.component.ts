import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IMyDpOptions,IMyDate } from 'mydatepicker';
import { NuevoProductoService } from '../../../servicios/nuevo-producto.service';
import { CertificacionSchema } from '../../../schemas/certificacionSchema';
import { VenderSchema } from '../../../schemas/vender-schema';
import { ImageProductSchema } from '../../../schemas/image-product-schema';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-editar-producto',
  templateUrl: './editar-producto.component.html',
  styleUrls: ['./editar-producto.component.css']
})
export class EditarProductoVentaComponent implements OnInit {

  @ViewChild('imgProduct') imgProduct:ElementRef;
  @ViewChild('imageRelated') imageRelated:ElementRef;
 
  pspinner = {  mode:'indeterminate' };
  spinerIsVisible= true;

  router:Router;

  actualDate = new Date();
  certificatesIsVisible = false;
  
  modalCertificatesIsVisible = false;
  modalConfirmIsVisible = false;

  certificates:CertificacionSchema[];
  venderSchema:VenderSchema;
  
  categories=[];
  products=[];
  countries = [];
  unities = [];
  states = [];
  presentations = [];

  images:ImageProductSchema[]=[];

  certificates_images:CertificacionSchema[] = [];
  certificates_id:number[] = []
  checkCertificate:boolean[] = [];

  error:string[] = [];
  errorExist = false;

  constructor( public newProductService:NuevoProductoService, 
               public activeRoute:ActivatedRoute, 
               _router:Router ) { 
                 this.router = _router;
               }

  public hastaOption: IMyDpOptions = null;
  public desdeOption: IMyDpOptions = null;

  seldate:IMyDate = null;
  seldate2:IMyDate = null;

  ngOnInit() {

    this.desdeOption={
        dateFormat: 'dd/mm/yyyy',
        height:'1cm',
        width:'85%',
        editableDateField: false,
        componentDisabled:true
    }

    this.hastaOption = {
      dateFormat: 'dd/mm/yyyy',
      height:'1cm',
      width:'85%',
      editableDateField: false,
      todayBtnTxt: ' Hoy',
      disableUntil: { 
                      year: this.actualDate.getFullYear(), 
                      month: this.actualDate.getMonth()+1,
                      day: this.actualDate.getDate()-1
                    }
    };

    this.newProductService.getBaseData().subscribe( res => {
      this.certificates = res.certificates;
      this.categories = res.categories;
      this.countries = res.countries;
      this.unities = res.unities;
      this.presentations = res.presentation;

      sessionStorage.setItem('certificates',JSON.stringify(res.certificates));

    }).add( () => {
      this.newProductService
      .getEditApplicationData( parseInt( this.activeRoute.snapshot.paramMap.get('id')) )
      .subscribe(res => {
          this.venderSchema = res.ofert;
          
          res.images.forEach(image => {
            this.images.push({type:'default',imagen: image.imagen, data:image});
          });

          res.certificatesOffert.forEach(certificate =>{
            
            let image = this.certificates.find( cert => cert.id == certificate.certificacion_id );
            
            image !== void 0 ? this.certificates_images.push({id:image.id,nombre:image.nombre ,imagen:image.imagen})
            : '';

          });
          this.venderSchema.venta_id = parseInt(this.activeRoute.snapshot.paramMap.get('id'));
        })
        .add( () => {
          if(this.venderSchema.estado_id != 0){
            this.countries.forEach( country => {
              for(let state of country.states){
                if( state.id == this.venderSchema.estado_id ){
                  this.venderSchema.pais_id = state.pais_id;
                  this.states = country.states;
                }
              };
              
            });
          }
          else{
            this.venderSchema.pais_id = 0
          }
        }).add( () => {
          this.categories.forEach( category => {
            for(let product of category.products){
              if( product.id == this.venderSchema.producto_id ){
                this.venderSchema.categoria = product.categoria_id;
                this.products = category.products;
              }
            };
          });
        }) 
        .add(()=>{
          this.venderSchema.photo = this.images;
          this.venderSchema.certificates = this.certificates_images;
        })
        .add( ()=> {
          this.certificates.forEach( cert =>{
            let isHere = this.certificates_images.find(cert_image => cert_image.id === cert.id);
            isHere === void 0 ? this.checkCertificate.push(false) : this.checkCertificate.push(true);
          });
        }).add( () => {
          //console.log( this.venderSchema );
          let aux = this.venderSchema.disponible_desde.split(' ');
          let date = aux[0].split('-');
          
          this.seldate = {year: parseInt(date[0]), month: parseInt(date[1]) , day: parseInt(date[2]) };

          let aux2 = this.venderSchema.disponible_hasta.split(' ');
          let date2 = aux2[0].split('-');
          
          this.seldate2 = {year: parseInt(date2[0]), month: parseInt(date2[1]) , day: parseInt(date2[2]) };
        });
    }).add( () => this.spinerIsVisible = false );

    this.venderSchema = {
      producto_id:0,
      variedad: null,
      otras_caracteristicas: null,
      volumen_total: null,
      unidad_pedido_minimo_id: 0,
      unidad_id: 0,
      precio_unidad: 0,
      presentacion_id: 0,
      tamano: 0,
      pedido_minimo: 0,
      unidad_presentacion_id: 0,
      precio: null,
      pais_id: 0,
      estado_id: 0,
      ciudad: null,
      codigo_postal: null,
      disponible_desde: null,
      disponible_hasta: null,
      resena: null,
      photo:this.images,
      certificates:this.certificates_images,
      usuario_id: JSON.parse(sessionStorage.userData).wp_user_id,
      categoria:0,
      venta_id:0
    };
  }

  openInputImageProduct(){
    this.images.length < 5  ? this.imgProduct.nativeElement.click() : ''; 
  }

  dropCertificatesList(){
    this.certificatesIsVisible = this.certificatesIsVisible ? false : true;
  }

  closeCertificatesList(){
    this.certificatesIsVisible = false;
  }

  chargeStates(){
    const states = this.countries.find( res => res.id == this.venderSchema.pais_id );
    this.states = states.states;
    this.venderSchema.estado_id = 0;
  }

  chargeProducts(){
    const product = this.categories.find( res => res.id == this.venderSchema.categoria );
    this.products = product.products;
    this.venderSchema.producto_id = 0;
  }

  chargeImageNew(event){
    let e = event.target.files;
    e !== void 0 && e.length ? this.saveImages(0,e) : '';
  }

  saveImages( index:number, files ){
    if(index < files.length){
      const reader = new FileReader();  
      reader.onload = (event: any) => {
        this.images.length < 5 ? this.images.push({type:'up',imagen:event.target.result}):'';
      }
      
      reader.readAsDataURL(files[index]);
        
      this.saveImages( index+1, files);
    };
  }

  eraseImage(index){
    if(this.images.length > 1 && this.images[index].type !== 'up'){
      let id = this.images[index].data.id;
      this.newProductService.eraseImageSolicitute(id).subscribe( res => this.images.splice(index, 1) );
    }else{
      this.images.splice(index, 1);
    }
  }

  changeDefaultImagesProduct(){
    const image = this.products.find(res => res.id == this.venderSchema.producto_id);
    if( this.images.length === 0 || this.images.length <= 1 ) {
      this.images[0] = {type:'default',imagen:image.imagen_default};
    } 
  }

  selectCertified(id,i){
    
    let found_image = this.certificates_images.find( data => data.id == id );
    let index = this.certificates_images.indexOf( id );

    if ( found_image === void 0 ) {
      let found = this.certificates.find( data => data.id == id);
      this.certificates_images.push(found);
    }
    else this.certificates_images.splice( index, 1 );
    
    this.checkCertificate[i] = !this.checkCertificate[i] ? true : false;
    
  }

  addCertificate(){
    this.modalCertificatesIsVisible = true;
  }

  closeCertificate(evnt){
    //this.certificates = null;
    let certificates = JSON.parse(sessionStorage.getItem('certificates'));

    if(certificates !== void 0 && certificates !== null){
      this.certificates = certificates;
    }
    this.modalCertificatesIsVisible = evnt;
  }
  
  saveOfert(){
    this.spinerIsVisible = true;
    this.newProductService.updateApplicationData(this.venderSchema)
    .subscribe( res => { 

      if(res !== null){
        if( res.error !== void 0 ) {
          this.errorExist = true;
          this.error = res.error;
          window.scroll(0,0);
        }
      }else{ 
        this.modalConfirmIsVisible=true;
        sessionStorage.setItem('displayInfood','vender');
      }
    })
    .add( () => this.spinerIsVisible = false );
  }

  cancel(){
    sessionStorage.setItem('displayInfood','vender');
    this.router.navigate(['dashboard']);
  }

  redirect(to:number){
    switch(to){
      case 0:
        this.router.navigate(['dashboard']);
      break;
      case 1:
        sessionStorage.setItem('displayInfood','vender');
        this.router.navigate(['dashboard']);
      break;
    }
  }

  navigateToReferencePrice(){
    window.open('http://infood.com.mx');
  }

}