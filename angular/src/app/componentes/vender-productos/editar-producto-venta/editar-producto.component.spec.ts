import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarProductoVentaComponent } from './editar-producto.component';

describe('EditarProductoVentaComponent', () => {
  let component: EditarProductoVentaComponent;
  let fixture: ComponentFixture<EditarProductoVentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarProductoVentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarProductoVentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
