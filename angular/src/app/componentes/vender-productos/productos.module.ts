import { MyDatePickerModule } from 'mydatepicker';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NuevoProductoVentaComponent } from './nuevo-producto-venta/nuevo-producto.component';
import { EditarProductoVentaComponent } from './editar-producto-venta/editar-producto.component';
import { DetalleProductoVentaComponent } from './detalle-producto-venta/detalle-producto.component';
import { ListaProductosVentaComponent } from './lista-productos-venta/lista-productos.component';
import { DashboardModule } from '../dashboard/dashboard.module';
import { ClickOutsideModule } from 'ng4-click-outside';
import { FormsModule } from '@angular/forms';
import { ModalNuevaCertificacionComponent } from './modal-nueva-certificacion/modal-nueva-certificacion.component';
import { ModalAvisoComponent } from './modal-aviso/modal-aviso.component';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { MaterialModule } from '../../material/material.module';
//import { ModalNuevaCertificacionComponent } from './modal-nueva-certificacion/modal-nueva-certificacion.component';

@NgModule({
  imports: [
    CommonModule, MyDatePickerModule, DashboardModule, ClickOutsideModule,FormsModule, AngularOpenlayersModule, MaterialModule
  ],
  declarations: [NuevoProductoVentaComponent, EditarProductoVentaComponent,  DetalleProductoVentaComponent, ListaProductosVentaComponent, ModalNuevaCertificacionComponent, ModalAvisoComponent /*ModalNuevaCertificacionComponent*/],
  exports: [ ModalAvisoComponent, ModalNuevaCertificacionComponent ]
})
export class ProductosModule { }
