import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoProductoVentaComponent } from './nuevo-producto.component';

describe('NuevoProductoVentaComponent', () => {
  let component: NuevoProductoVentaComponent;
  let fixture: ComponentFixture<NuevoProductoVentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoProductoVentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoProductoVentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
