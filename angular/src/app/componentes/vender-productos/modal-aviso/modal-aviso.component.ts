import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal-aviso',
  templateUrl: './modal-aviso.component.html',
  styleUrls: ['./modal-aviso.component.css']
})
export class ModalAvisoComponent implements OnInit {

  constructor(_router:Router) {
    this.router = _router;
  }

  router:Router;

  ngOnInit() {
  }

  closeDialog(){
    this.router.navigate(['dashboard']);
  }
}
