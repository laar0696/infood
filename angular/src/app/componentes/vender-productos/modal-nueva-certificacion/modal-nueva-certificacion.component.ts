import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { CertificateService } from '../../../servicios/certificate.service';
import { CertificacionSchema } from '../../../schemas/certificacionSchema';

@Component({
  selector: 'app-modal-nueva-certificacion',
  templateUrl: './modal-nueva-certificacion.component.html',
  styleUrls: ['./modal-nueva-certificacion.component.css']
})
export class ModalNuevaCertificacionComponent implements OnInit {

  @Output('display') display = new EventEmitter<boolean>();
  @ViewChild('imgProduct') imgProduct: ElementRef;

  certificateNew:CertificacionSchema;
  images:string[]= [];
  error:string[] = [];
  errorExist = false;

  constructor(private certificate:CertificateService) { }

  ngOnInit() {
    this.certificateNew = {
      nombre: null,
      imagen: null
    }
  }

  closeDialog(){
    this.display.emit(false);
  }

  openInputImageProduct(){
    this.imgProduct.nativeElement.click();
  }

  chargeImageNew(event){
    let e = event.target.files;

    if(e !== void 0 && e[0]){
      const reader = new FileReader();  
      
      reader.onload = (event: any) => {
        this.images[0] = event.target.result;
        this.certificateNew.imagen = event.target.result;
      }

      reader.readAsDataURL(e[0]);
    }
  }

  saveCertificate(){
    this.certificate.createCertificate(this.certificateNew)
    .subscribe( data => {
      if( data.error !== void 0 ){
        this.error = data.error;
        this.errorExist = true;
      }
      else{ 
        sessionStorage.certificates = JSON.stringify(data);
      }
    }).add(() => this.errorExist ? '' :  this.display.emit(false));
  }

}
