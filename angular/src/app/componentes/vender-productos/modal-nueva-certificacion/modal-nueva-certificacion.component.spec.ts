import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalNuevaCertificacionComponent } from './modal-nueva-certificacion.component';

describe('ModalNuevaCertificacionComponent', () => {
  let component: ModalNuevaCertificacionComponent;
  let fixture: ComponentFixture<ModalNuevaCertificacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalNuevaCertificacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalNuevaCertificacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
