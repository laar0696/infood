import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-loading-modal',
  templateUrl: './loading-modal.component.html',
  styleUrls: ['./loading-modal.component.css']
})
export class LoadingModalComponent implements OnInit {

  @Input()  msg;

  public pspinner = {  mode:'indeterminate' };

  constructor(public dialog: MatDialog) {}

  ngOnInit(){}

}
