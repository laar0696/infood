import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService as SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider} from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import { NgForm } from '@angular/forms';
import { AuthService } from '../../servicios/auth.service';
import { TokenService } from '../../servicios/token.service';
import { JarwisService } from '../../servicios/jarwis.service';
import { LoadingModalComponent } from '../loading-modal/loading-modal.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  public terminos:boolean = false;
  captcha:boolean = false;

  public form = {
    user_email: null,
    user_pass: null
  };
  private user: SocialUser;
  private loggedIn: boolean;

  public error = null;

  constructor(
    private Jarwis: JarwisService,
    private Token: TokenService,
    private router: Router,
    private Auth: AuthService,
    private authService: SocialAuthService,
    private modal: MatDialog
  ) { 

  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(() => {
      this.authService.authState.subscribe((user) => {
        this.user = user;
        this.loggedIn = (user != null);
        if (this.loggedIn) {
          this.loginGoogle();
        }
      });
      
    });
  }

  signInWithFacebook(): void{
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(()=> {
      this.authService.authState.subscribe((user) => {
        this.user = user;
        this.loggedIn = (user != null);
        if (this.loggedIn) {
          this.loginFacebook();
        }
      });
    });
  }

  loginGoogle() {
    this.modal.open(LoadingModalComponent, {
      disableClose: true
    });
    this.Jarwis.loginGoogle({ 
      'email': this.user.email,
      'firstName': this.user.firstName, 
      'lastName': this.user.lastName, 
      'photoUrl': this.user.photoUrl 
    }).subscribe(
      data => {this.handleResponse(data)},
      error => {
        this.handleError(error);
        this.modal.closeAll();
      }
    );
  }

  loginFacebook() {
    this.modal.open(LoadingModalComponent, {
      disableClose: true
    });
    this.Jarwis.loginFacebook({ 'user': this.user.email }).subscribe(
      data => this.handleResponse(data),
      error => {
        this.handleError(error);
        this.modal.closeAll();
      }
    );
  }

  onSubmit(form) {
    if ( form.valid ) {
      this.modal.open(LoadingModalComponent, {
        disableClose: true
      });
      this.Jarwis.login(this.form).subscribe(
        data => {
          sessionStorage.setItem('admin_user', JSON.stringify( data ) );
          this.handleResponse(data);
        },
        error => {
          this.modal.closeAll();
          this.handleError(error)
        }
      );
    } else {
      if (form.form.controls.email.status == "INVALID") {
        this.error = "Dirección de correo inválida";
      } else {
        this.error = "Debes completar todos los campos";
      }
    }
  }

  handleResponse(data) {
    this.Token.handle(data.access_token);
    this.Auth.changeAuthStatus(true);
    this.Auth.administrador(data.admin);
    this.modal.closeAll();
    this.router.navigate(['dashboard']);
  }

  handleError(error) {
    this.error = error.error.error;
  }

  ngOnInit() {
    
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
    });
  }




}
