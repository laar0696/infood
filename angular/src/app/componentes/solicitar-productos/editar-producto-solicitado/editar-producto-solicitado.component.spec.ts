import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarProductoSolicitadoComponent } from './editar-producto-solicitado.component';

describe('EditarProductoSolicitadoComponent', () => {
  let component: EditarProductoSolicitadoComponent;
  let fixture: ComponentFixture<EditarProductoSolicitadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarProductoSolicitadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarProductoSolicitadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
