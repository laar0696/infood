import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetalleProductoSolicitadoComponent } from './detalle-producto-solicitado/detalle-producto-solicitado.component';
import { EditarProductoSolicitadoComponent } from './editar-producto-solicitado/editar-producto-solicitado.component';
import { NuevoProductoSolicitadoComponent } from './nuevo-producto-solicitado/nuevo-producto-solicitado.component';
import { DashboardModule } from '../dashboard/dashboard.module';
import { FormsModule } from '@angular/forms';
import { ClickOutsideModule } from 'ng4-click-outside';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { MyDatePickerModule } from 'mydatepicker';
import { ProductosModule } from '../vender-productos/productos.module';
import { MaterialModule } from '../../material/material.module';

@NgModule({
  imports: [
    CommonModule, DashboardModule, FormsModule , ClickOutsideModule, AngularOpenlayersModule, MyDatePickerModule, ProductosModule, MaterialModule
  ],
  declarations: [DetalleProductoSolicitadoComponent, EditarProductoSolicitadoComponent, NuevoProductoSolicitadoComponent]
})
export class SolicitarProductosModule {}
