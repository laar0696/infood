import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { DetalleProductoSchema } from '../../../schemas/detalle-producto-schema';
import { DetalleSolicitudService } from '../../../servicios/detalle-solicitud.service';
import { SourceRasterComponent } from 'ngx-openlayers';

interface RasterData {
  brightness: number;
  contrast: number;
}

@Component({
  selector: 'app-detalle-producto-solicitado',
  templateUrl: './detalle-producto-solicitado.component.html',
  styleUrls: ['./detalle-producto-solicitado.component.css']
})

export class DetalleProductoSolicitadoComponent implements OnInit {

  constructor(router:Router, private detalleService:DetalleSolicitudService  ,public activeRoute:ActivatedRoute  ) { this.routes = router; }
 
  @ViewChild('txtComment') txtComment:ElementRef;
  @ViewChild(SourceRasterComponent) currentRasterSource;

  routes:Router;
  detalleInfo:DetalleProductoSchema;
  imageCarrouselPrev:string;

  origin:string;

  next:string;
  prev:string;

  lat:number;
  lon:number;

  position:number;
  id_offert:number;

  threads = 4;
  brightness = 0;
  contrast = 0;
  operationType = 'image';
  selectLayer = 'osm';
  lib: any = {
    brightness: this.brightness,
    contrast: this.contrast,
  };
  
  spinerIsVisible = true;

  public pspinner = {  mode:'indeterminate' };

  ngOnInit() {
    window.scroll(0,0);
    this.next = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADxSURBVEhLtdQ7akJRFEbhq1ELo3E04lshRYo0goWtY7G2sLDRxsJKCwdg55tMJWkSX4ggmLVB4bCxEM52wYdg8W/wXgweKHr9NC+GMX5RkS+sG+FydUAZZoXxjdsBsUcJZmXxB/fIDkWYVYCMuke2MD0iv708A31Ejpv1Dn1kgzzM+sAR7hF5RjmY9YkT9JEMzKri3pE0zKrDPSB+EIF3LxhCH5jAOxkfQI9PEYdXIfShx2cwGe9Bj8/xCq9kvAM9vkAC3rWhx5dIwrsWnjbehB5f4Q3eyeso/5ju+BopmNXAGTL+BdPxWzV08ZRxg4LgH/UwaZ/ZnR8/AAAAAElFTkSuQmCC';
    this.prev = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADuSURBVEhLtdQ7akJRFIXha2ETBEkRSJfKGQiSGMXGwsJCJ+BkNEjwUaXMYATBQrAQUosz8Ikogvk3GDhsCAhn+cOHYLG2esEksvT1VV4KPezRsjeU2fgQl6szGpAVjv/pQ9IAfnyEB0Rnv7kfHyOD6D7hxyfIIrou/PgUkvEP+PEZHhFdB358jidE14Yf/4FkvA4/vsQzJOWwQnjghCZkFeCPHGHfTtZ/R2qQ9Qp/5IAqZL1hjfCI/U1XIKuIDfyREmS9Y4vwiB21byjLPvEO4RF7RnnIKsMf+YI0O2LPwMYXeIE8e/DfuMv4jSXJL02WaZRNaaEmAAAAAElFTkSuQmCC';

    this.origin = sessionStorage.originProductDetailsSolicituted != void 0 ? 'Administrar Solicitudes' : 'Solicitar';

    this.position = 0;

    this.id_offert = parseInt( this.activeRoute.snapshot.paramMap.get('id') );

    this.detalleService.getDataSolicitude(this.id_offert)
    .subscribe( res => {
      this.detalleInfo = res;
    })
    .add( ()=> { 
        this.imageCarrouselPrev = this.detalleInfo.imagenes[0].imagen;
        this.lat = this.detalleInfo.lat;
        this.lon = this.detalleInfo.lon;
    }).add( () => this.spinerIsVisible = false);

   this.detalleInfo = {
        producto:null,
        unidad:null,
        precio_unidad:null,
        caracteristicas:null,
        productor:null,
        produccion_total:null,
        presentacion:null,
        tamano:null,
        desde:null,
        hasta:null,
        certificados:[],
        resena:null,
        comentarios:[],
        imagenes:[],
        tipo_precio:null,
        origen:null,
        ciudad:null
      } 
  }

  beforeOperations(event) {
    const data: RasterData = event.data;
    data.brightness = this.brightness;
    data.contrast = this.contrast;
  }

  operation(imageDatas: [ImageData], data: RasterData) {
    let [imageData] = imageDatas;
    return imageData;
  }

  afterOperations() {}

  updateRaster() {
    this.currentRasterSource.instance.changed();
  }

  changeImage(i:number){
    this.imageCarrouselPrev = this.detalleInfo.imagenes[i].imagen;
    this.position = i;
  }

  nextImage(){
    if( this.position < this.detalleInfo.imagenes.length-1 ){
      this.position++;
      this.imageCarrouselPrev = this.detalleInfo.imagenes[this.position].imagen;
    } 
  }

  prevImage(){
    if(this.position > 0){
      this.position--;
      this.imageCarrouselPrev = this.detalleInfo.imagenes[this.position].imagen;
    }
  }

  editSolicitud(){
    this.routes.navigate([`editar-producto-solicitado/${this.id_offert}`]);
  }

  cancel(){
    this.origin !== 'Administrar Solicitudes' ? sessionStorage.setItem('displayInfood','solicitar')
    : sessionStorage.setItem('displayInfood','solicitar-admin');
    
    sessionStorage.removeItem('originProductDetailsSolicituted');
    this.routes.navigate(['dashboard']);
  }

  saveComment(){
    let coment = this.txtComment.nativeElement.value;
    this.txtComment.nativeElement.value = '';
    this.detalleService.setComment(coment,this.id_offert).subscribe( res=>{
      if(res === 'false'){}else{
        this.detalleInfo.comentarios = res;
      }
    });
  }

  redirect(to:number){
    switch(to){
      case 0:
        this.routes.navigate(['dashboard']);
      break;
      case 1:
        this.cancel();
      break;
    }
  }

}
