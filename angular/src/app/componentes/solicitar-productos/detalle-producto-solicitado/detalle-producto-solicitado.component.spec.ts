import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleProductoSolicitadoComponent } from './detalle-producto-solicitado.component';

describe('DetalleProductoSolicitadoComponent', () => {
  let component: DetalleProductoSolicitadoComponent;
  let fixture: ComponentFixture<DetalleProductoSolicitadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleProductoSolicitadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleProductoSolicitadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
