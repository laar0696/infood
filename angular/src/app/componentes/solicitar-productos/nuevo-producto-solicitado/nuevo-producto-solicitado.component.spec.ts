import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoProductoSolicitadoComponent } from './nuevo-producto-solicitado.component';

describe('NuevoProductoSolicitadoComponent', () => {
  let component: NuevoProductoSolicitadoComponent;
  let fixture: ComponentFixture<NuevoProductoSolicitadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoProductoSolicitadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoProductoSolicitadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
