import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { NuevoProductoService } from '../../../servicios/nuevo-producto.service';
import { CertificacionSchema } from '../../../schemas/certificacionSchema';
import { ImageProductSchema } from '../../../schemas/image-product-schema';
import { Router } from '@angular/router';
import { SoliciteSchema } from '../../../schemas/solicitud-schema';
import { SolicitutesService } from '../../../servicios/solicitutes.service';

@Component({
  selector: 'app-nuevo-producto-solicitado',
  templateUrl: './nuevo-producto-solicitado.component.html',
  styleUrls: ['./nuevo-producto-solicitado.component.css']
})
export class NuevoProductoSolicitadoComponent implements OnInit {

  @ViewChild('imgProduct') imgProduct:ElementRef;
  @ViewChild('imageRelated') imageRelated:ElementRef;

  router:Router;
  actualDate = new Date();

  certificatesIsVisible = false;
  
  modalCertificatesIsVisible = false;
  modalConfirmIsVisible = false;

  certificates:CertificacionSchema[];
  solicitudSchema:SoliciteSchema;
  
  categories=[];
  products=[];
  countries = [];
  unities = [];
  states = [];
  presentations = [];

  images:ImageProductSchema[]=[];

  certificates_images:CertificacionSchema[] = [];
  certificates_id:number[] = []
  checkCertificate:boolean[] = [];

  error:string[] = [];
  errorExist = false;

  constructor( public newProductService:SolicitutesService, _router:Router ) {
    this.router = _router;
  }

  public myDatePickerOptions: IMyDpOptions = null;
  

  ngOnInit() {

    this.myDatePickerOptions = {
      dateFormat: 'dd/mm/yyyy',
      height:'1cm',
      width:'85%',
      editableDateField: false,
      todayBtnTxt: ' Hoy',
      disableUntil: { 
                      year: this.actualDate.getFullYear(), 
                      month: this.actualDate.getMonth()+1,
                      day: this.actualDate.getDate()-1
                    }
    };  

    this.newProductService.getBaseData().subscribe( res => {
      this.certificates = res.certificates;
      this.categories = res.categories;
      this.countries = res.countries;
      this.unities = res.unities;
      this.presentations = res.presentation;

      sessionStorage.setItem('certificates',JSON.stringify(res.certificates));

      res.certificates.forEach( () => {
        this.checkCertificate.push(false);
      });

    });

    this.solicitudSchema = {
      producto_id:0,
      variedad: null,
      otras_caracteristicas: null,
      volumen_total: null,
      unidad_id: 0,
      precio_unidad: null,
      presentacion_id: 0,
      tamano: null,
      unidad_presentacion_id: 0,
      pais_id: 0,
      estado_id: 0,
      ciudad: null,
      codigo_postal: null,
      requerido_desde: null,
      requerido_hasta: null,
      resena: null,
      photo:this.images,
      certificates:this.certificates_images,
      categoria:0
    };

  }

  openInputImageProduct(){
    this.images.length < 5  ? this.imgProduct.nativeElement.click() : ''; 
  }

  dropCertificatesList(){
    this.certificatesIsVisible = this.certificatesIsVisible ? false : true;
  }

  closeCertificatesList(){
    this.certificatesIsVisible = false;
  }

  chargeStates(){
    const states = this.countries.find( res => res.id == this.solicitudSchema.pais_id );
    this.states = states.states;
    this.solicitudSchema.estado_id = 0;
  }

  chargeProducts(){
    const product = this.categories.find( res => res.id == this.solicitudSchema.categoria );
    this.products = product.products;
    this.solicitudSchema.producto_id = 0;
  }

  chargeImageNew(event){
    let e = event.target.files;
    e !== void 0 && e.length ? this.saveImages(0,e) : '';
  }

  saveImages( index:number, files ){
    if(index < files.length){
      const reader = new FileReader();  
      reader.onload = (event: any) => {
        this.images.length < 5 ? this.images.push({type:'up',imagen:event.target.result}):'';
      }
      
      reader.readAsDataURL(files[index]);
        
      this.saveImages( index+1, files);
    };
  }

  eraseImage(index){
    this.images.splice(index, 1);;    
  }

  changeDefaultImagesProduct(){
    const image = this.products.find(res => res.id == this.solicitudSchema.producto_id);
    if( this.images.length === 0 || this.images.length <= 1 ) {
      this.images[0] = {type:'default',imagen:image.imagen_default};
    } 
  }

  selectCertified(id,i){
    
    let found_image = this.certificates_images.find( data => data.id == id );
    let index = this.certificates_images.indexOf( id );

    if ( found_image === void 0 ) {
      let found = this.certificates.find( data => data.id == id);
      this.certificates_images.push(found);
    }
    else this.certificates_images.splice( index, 1 );
    
    this.checkCertificate[i] = !this.checkCertificate[i] ? true : false;
    
  }

  addCertificate(){
    this.modalCertificatesIsVisible = true;
  }

  closeCertificate(evnt){
    //this.certificates = null;
    let certificates = JSON.parse(sessionStorage.getItem('certificates'));

    if(certificates !== void 0 && certificates !== null){
      this.certificates = certificates;
    }
    this.modalCertificatesIsVisible = evnt;
  }
  
  saveOfert(){
    this.newProductService.setDataApplication(this.solicitudSchema)
    .subscribe( res => { 

      if(res !== null){
        if( res.error !== void 0 ) {
          this.errorExist = true;
          this.error = res.error;
        }
      }else {
        this.modalConfirmIsVisible=true;
        sessionStorage.setItem('displayInfood','solicitar');
      };
    });
  }

  cancel(){
    sessionStorage.setItem('displayInfood','solicitar');
    this.router.navigate(['dashboard']);
  }

  redirect(to:number){
    switch(to){
      case 0:
        this.router.navigate(['dashboard']);
      break;
      case 1:
        sessionStorage.setItem('displayInfood','solicitar');
        this.router.navigate(['dashboard']);
      break;
    }
  }

  navigateToReferencePrice(){
    window.open('http://infood.com.mx');
  }
}
