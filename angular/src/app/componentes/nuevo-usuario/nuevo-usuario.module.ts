import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NuevoUsuarioComponent } from './nuevo-usuario/nuevo-usuario.component';
import { BtnRegistrarComponent } from './btn-registrar/btn-registrar.component';
import { CaptchaComponent } from './captcha/captcha.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [NuevoUsuarioComponent, BtnRegistrarComponent, CaptchaComponent]
})
export class NuevoUsuarioModule { }
