import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent implements OnInit {

  public busqueda = "";
  public siteUrl:string = environment.siteUrl;
  
  constructor( private router : Router) { }

  ngOnInit() {
  }

}
