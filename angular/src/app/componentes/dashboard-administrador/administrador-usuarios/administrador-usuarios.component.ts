import { Component, OnInit, Inject, Input, Output, ElementRef, ViewChild } from '@angular/core';
import { UsersService } from '../../../servicios/users.service';
import { DashboardPersonalService } from '../../../servicios/dashboard-personal.service';
import { Userschema } from '../../../schemas/userschema';
import { MatDialog, DialogRole } from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-administrador-usuarios',
  templateUrl: './administrador-usuarios.component.html',
  styleUrls: ['./administrador-usuarios.component.css']
})
export class AdministradorUsuariosComponent implements OnInit {
  pspinner = {  mode:'indeterminate' };
  spinerIsVisible= true;

  public deshabilitado = 0;
  public users = [];
  public index:number;
  public buscar;
  public allUsersView = true;
  public editUserView = false;
  info = false;
  infoData: Userschema;
  @Input() infoUser : Userschema;
  public imgPerfil = 'assets/img/avatar.png';
  public name = '';
  countryId = 1;

  constructor(private usersService: UsersService, private dashboardService:DashboardPersonalService, public dialog: MatDialog) { }

  ngOnInit() {
    this.allUsersView = true;
    this.editUserView = false;
    this.usersService.getAllUsers()
      .subscribe(data => {
        this.users = data;
      })
      .add( () => this.spinerIsVisible = false);
    // console.log(this.usuarios);
  }

  ngOnChanges(){
    if(this.infoUser){
      this.infoUser = JSON.parse(sessionStorage.userData);
      // this.imgEmpresa = this.infoData.enterprise_avatar;
      // this.countries = JSON.parse(sessionStorage.countries);
      // this.states = [];
    }
  }

  statusChange(id) {
    this.spinerIsVisible = true;
    this.deshabilitado = id;
    this.usersService.changeStatus(id)
    .subscribe(data => {
      this.deshabilitado = 0;
    })
    .add( () => this.spinerIsVisible = false);
  }

  editUser(id) {
    this.spinerIsVisible = true;
    this.dashboardService.getUserData(id).subscribe( res => {

      this.imgPerfil = this.isNull(res.usermarket.perfil_avatar);
      this.name = this.isNull(res.usermarket.name) +' '+ this.isNull(res.usermarket.ap_paterno);

      sessionStorage.setItem('countries',JSON.stringify(res.countries));
      sessionStorage.setItem('states',JSON.stringify(res.states));
      sessionStorage.setItem('user_type', JSON.stringify(res.user_type));
      sessionStorage.setItem('users_type', JSON.stringify(res.users_type));

      for (let i = 0; i < res.countries.length; i++) {
        for (let j = 0; j < res.countries[i].states.length; j++) {
          if (res.countries[i].states[j].id == this.isNull(res.usermarket.estado_id)) {
            this.countryId = res.countries[i].id;
            break;
          }
        };
      };

      sessionStorage.setItem('admin_user_editando',  res.usermarket.administrador );
      sessionStorage.setItem('estatus_user_editando', res.usermarket.estatus );
      
      this.infoUser = {
        token:localStorage.token,
        administrador: this.isNull(res.usermarket.administrador),
        estatus: this.isNull(res.usermarket.estatus),
        nickname: this.isNull(res.usermarket.user_wp.user_nicename) ,
        user_id: this.isNull(res.usermarket.id),
        wp_user_id: this.isNull(res.usermarket.wp_user_id),
        first_name: this.isNull(res.usermarket.name),
        last_name: this.isNull(res.usermarket.ap_paterno),
        secont_last_name: this.isNull(res.usermarket.ap_materno),
        // billing_email:this.isNull(res.usermarket.email),
        billing_email:this.isNull(res.usermeta.email),
        billing_company:this.isNull(res.usermeta.billing_company),
        email_alt: this.isNull(res.usermarket.email_alt),
        enterprise_phone: this.isNull(res.usermarket.telefono_empresa),
        billing_name: '',
        rfc: this.isNull(res.usermarket.rfc),
        cp: this.isNull(res.usermeta.cp),
        billing_phone: this.isNull(res.usermarket.telefono_empresa),
        celphone: this.isNull(res.usermarket.celular) ,
        phone: this.isNull(res.usermeta.billing_phone),
        billing_address: this.isNull(res.usermeta.billing_address),
        comertial_name: this.isNull(res.usermarket.nombre_comercial),
        semblance: this.isNull(res.usermarket.semblanza),
        profile_avatar : this.isNull(res.usermarket.perfil_avatar),
        enterprise_avatar: this.isNull(res.usermarket.empresa_avatar),
        image_profile: '',
        image_enterprise: '',
        estado_id: this.isNull(res.usermarket.estado_id),
        country_id: this.isNull(this.countryId),
        ciudad: this.isNull(res.usermarket.ciudad),
        user_type: this.isNull(res.usermarket.tipo_usuario_id)
      }

      this.allUsersView = false;
      this.editUserView = true;
    })
    .add( () => {
      sessionStorage.setItem('userData',JSON.stringify(this.infoUser));
    })
    .add( () => this.info=true)
    .add( () => this.spinerIsVisible = false);
  }

  usersList() {
    this.allUsersView = true;
    this.editUserView = false;
  }

  deleteUser(userClone) {
    let dialogRef = this.dialog.open(ModalConfirmarEliminacion, {
      width: '600px',
      data: {user: userClone}
    });
  }

  isNull(data){ 
    return data = data === void 0 || data === null ? '' : data; 
  }

  search() {
    this.spinerIsVisible = true;
    this.users = [{name: 'Buscando...'}];
    this.usersService.searchUser(this.buscar)
    .subscribe(data => {
      this.users = data;
      if(this.users.length == 0) {
        this.users = [{name: 'Sin resultados'}];
      }
    })
    .add( () => this.spinerIsVisible = false);
  }

}


/* Modal confirmar eliminacion */

@Component({
  selector: 'modal-confirmar-eliminacion',
  templateUrl: 'modal-confirmar-eliminacion.html',
})

export class ModalConfirmarEliminacion {
  
  closeDialog(): void {
    let dialogRef = this.dialog.closeAll();
  }

  borrando = false;

  constructor(
    private usersService: UsersService,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any 
  ) {}

  borrar(userClone){
    this.borrando = true;
    this.usersService.deleteUser(userClone.id)
    .subscribe(data => {
      this.dialog.closeAll();
      window.location.reload();
    });
  }

  cerrar() {
    this.dialog.closeAll();
  }
}