import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministradorSolicitudesComponent } from './administrador-solicitudes.component';

describe('AdministradorSolicitudesComponent', () => {
  let component: AdministradorSolicitudesComponent;
  let fixture: ComponentFixture<AdministradorSolicitudesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministradorSolicitudesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministradorSolicitudesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
