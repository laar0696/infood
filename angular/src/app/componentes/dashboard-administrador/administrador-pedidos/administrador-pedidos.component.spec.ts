import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministradorPedidosComponent } from './administrador-pedidos.component';

describe('AdministradorPedidosComponent', () => {
  let component: AdministradorPedidosComponent;
  let fixture: ComponentFixture<AdministradorPedidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministradorPedidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministradorPedidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
