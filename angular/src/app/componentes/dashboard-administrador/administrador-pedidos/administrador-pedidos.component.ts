import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MarketService } from '../../../servicios/market.service';
import { Router } from '@angular/router';
import { OfertaSchema } from '../../../schemas/ofertaSchema';

@Component({
  selector: 'app-administrador-pedidos',
  templateUrl: './administrador-pedidos.component.html',
  styleUrls: ['./administrador-pedidos.component.css']
})
export class AdministradorPedidosComponent implements OnInit {

  public productos: OfertaSchema[];
  public pspinner = {  mode:'indeterminate' };
  public vacio = false;
  public legend:string = 'Desea eliminar este pedido';
  public method:string = 'delete-order-confirmation';
  private id_solicitate:number = 0;
  public modalDisplay:boolean = false;
  private classType = '';

  constructor( public dialog: MatDialog, private marketService: MarketService, private router:Router ) {
    this.getCompras();
  }

  public paginatorConf = {
    length: 0,
    pageSize: 8,
    pageIndex: 0,
    pageSizeOptions: [ 2, 4, 8, 16 ]
  };

  closeModal($event){
    if($event){
      this.marketService.eliminaCompra(this.id_solicitate, this.classType).subscribe(
        data => {
          let elemento = this.productos.find( (element) : any => {
            return element.id == this.id_solicitate && element.tipo == this.classType;
          });
          let i = this.productos.indexOf( elemento );
          if( i > -1 ){
            this.productos.splice( i, 1);
          }
        }
      ).add( () => this.modalDisplay = false );
    }else{
      this.modalDisplay = false;
    }

  }

  getCompras(){
    this.marketService.getPedidosAdmin(this.paginatorConf.pageIndex,this.paginatorConf.pageSize).subscribe( 
      data => {
        this.vacio = data.items.length > 0 ? false:true;
        this.productos = data.items;
        this.paginatorConf.length = data.paginator.length;
      }    
    );
  }

  getCompra(tipo,id){
    if( tipo == "oferta")
        this.router.navigate([`detalles-compra-producto/${id}`]);
    else if( tipo == "solicitud")
        this.router.navigate([`detalles-compra-solicitud/${id}`]);
    
  }

  eliminar(id, tipo){
    this.classType = tipo;
    this.id_solicitate = id;
    this.modalDisplay = true;
  }

  ngOnInit() {
   
  }

  page(event){
    this.productos = null;
    this.paginatorConf.pageIndex = event.pageIndex + 1;
    this.paginatorConf.pageSize = event.pageSize;
    this.getCompras();
  }


}
