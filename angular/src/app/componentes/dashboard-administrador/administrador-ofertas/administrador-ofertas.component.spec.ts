import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministradorOfertasComponent } from './administrador-ofertas.component';

describe('AdministradorOfertasComponent', () => {
  let component: AdministradorOfertasComponent;
  let fixture: ComponentFixture<AdministradorOfertasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministradorOfertasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministradorOfertasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
