import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MarketService } from '../../../servicios/market.service';
import { Router } from '@angular/router';
import { OfertaSchema } from '../../../schemas/ofertaSchema';

@Component({
  selector: 'app-administrador-ofertas',
  templateUrl: './administrador-ofertas.component.html',
  styleUrls: ['./administrador-ofertas.component.css']
})
export class AdministradorOfertasComponent implements OnInit {

  public productos: OfertaSchema[];
  public pspinner = {  mode:'indeterminate' };
  public vacio = false;

  modalDisplay = false;
  legend:string = '';
  method:string = 'delete-offert-confirmation';
  id_solicitate:number = 0;
  type:string = '';

  constructor( public dialog: MatDialog, private marketService: MarketService, private router:Router ) {
    this.getCompras();
  }

  public paginatorConf = {
    length: 0,
    pageSize: 8,
    pageIndex: 0,
    pageSizeOptions: [ 2, 4, 8, 16 ]
  };

  getCompras(){
    this.marketService.getOfertasAdmin(this.paginatorConf.pageIndex, this.paginatorConf.pageSize).subscribe( 
      data => {
        this.vacio = data.items.length > 0 ? false:true;
        this.productos = data.items;
        this.paginatorConf.length = data.paginator.length; 
      }    
    );
  }

  closeModal($event){
    if($event) {
      this.marketService.eliminaCompra(this.id_solicitate, this.type).subscribe(
        data => {
          let elemento = this.productos.find( (element) : any => {
            return element.id == this.id_solicitate && element.tipo == this.type;
          });
          let i = this.productos.indexOf( elemento );
          if( i > -1 ){
            this.productos.splice( i, 1);
          }
        }
      ).add( () => this.modalDisplay = false );
    }else this.modalDisplay = false;
  }

  getCompra(tipo,id){
    if( tipo == "oferta")
        this.router.navigate([`detalles-compra-producto/${id}`]);
    else if( tipo == "solicitud")
        this.router.navigate([`detalles-compra-solicitud/${id}`]);
    
  }

  eliminar(id, tipo){
    this.id_solicitate = id;
    this.type = tipo;
    let el:any = this.productos.find( (element) => element.id == id && element.tipo == tipo);
    this.legend = '¿Desea eliminar la oferta de ' + el.producto.nombre + '?';
    this.modalDisplay = true;
  }

  page(event){
    this.productos = null;
    this.paginatorConf.pageIndex = event.pageIndex + 1;
    this.paginatorConf.pageSize = event.pageSize;
    this.getCompras();
  }

  ngOnInit() {
   
  }




}
