import { DashboardAdministradorModule } from './dashboard-administrador.module';

describe('DashboardAdministradorModule', () => {
  let dashboardAdministradorModule: DashboardAdministradorModule;

  beforeEach(() => {
    dashboardAdministradorModule = new DashboardAdministradorModule();
  });

  it('should create an instance', () => {
    expect(dashboardAdministradorModule).toBeTruthy();
  });
});
