import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DashboardPersonalService } from '../../../servicios/dashboard-personal.service';
import { Userschema } from '../../../schemas/userschema';
import { AdministradorUsuariosComponent } from '../administrador-usuarios/administrador-usuarios.component';
import { UsuariosComponent } from '../../dashboard/usuarios/usuarios.component';

@Component({
  selector: 'app-dashboard-administrative',
  templateUrl: './dashboard-administrative.component.html',
  styleUrls: ['./dashboard-administrative.component.css']
})
export class DashboardAdministrativeComponent implements OnInit {
  @ViewChild(AdministradorUsuariosComponent) usuariosComponent:AdministradorUsuariosComponent;
  
  pspinner = {  mode:'indeterminate' };
  spinerIsVisible= true;

  infoUser : Userschema;
  countryId = 1;
  dashboardPerfilRoute:string;

  perfil:boolean = true;
  solicitar:boolean = false;
  favoritos:boolean = false;
  comprar:boolean = false;
  vender:boolean = false;
  usuarios:boolean = false;
  productos:boolean = false;
  solicitudes:boolean = false;
  ofertas: boolean = false;
  pedidos: boolean = false;

  public imgPerfil = 'assets/img/avatar.png';
  public name = '';
  public userType = sessionStorage.getItem('userType');

  info = false;
  application = false;

  constructor(private dashboardService:DashboardPersonalService, public dialog: MatDialog) { 
    sessionStorage.validForm = 'true';
  }
  solicitute = false;
  allAdmin = false;

  ngOnInit() {
    /* 
      Abre el componente respectivo luego de crear un nuevo producto o solicitud, 
      no manipula ningun dato.
    */
    if(sessionStorage.displayInfood !== void 0){
      
      sessionStorage.displayInfood === 'vender' ? this.abreVender() :
      sessionStorage.displayInfood === 'solicitar' ? this.abreSolicitar() : 
      sessionStorage.displayInfood === 'productos-admin' ? this.abreProductos() : 
      sessionStorage.displayInfood === 'solicitar-admin' ? this.abreSolicitudes() : 
      sessionStorage.displayInfood === 'oferta-admin' ? this.abreOfertas() : 
      sessionStorage.displayInfood === 'comprar' ? this.abreComprar() :
      sessionStorage.displayInfood === 'favoritos' ? this.abreComprar() : '';

      sessionStorage.removeItem('displayInfood');
    }else{
      this.dashboardPerfilRoute = 'Perfil';
    }

    this.dashboardService.getPersonalData(1).subscribe( res => {
      this.imgPerfil = this.isNull(res.usermarket.perfil_avatar);
      this.name = this.isNull(res.usermarket.name) +' '+ this.isNull(res.usermarket.ap_paterno);

      sessionStorage.setItem('countries',JSON.stringify(res.countries));
      sessionStorage.setItem('userType', JSON.stringify(this.isNull(res.usermarket.administrador)));
      sessionStorage.setItem('userId', res.usermarket.wp_user_id);
      sessionStorage.setItem('user_type', JSON.stringify(res.user_type));
      sessionStorage.setItem('users_type', JSON.stringify(res.users_type));
      
      for (let i = 0; i < res.countries.length; i++) {
        for (let j = 0; j < res.countries[i].states.length; j++) {
          if (res.countries[i].states[j].id == this.isNull(res.usermarket.estado_id)) {
            this.countryId = res.countries[i].id;
            break;
          }
        };
      };

      this.infoUser = {
        token:localStorage.token,
        administrador: this.isNull(res.usermarket.administrador),
        estatus: this.isNull(res.usermarket.estatus),
        nickname: this.isNull(res.usermarket.user_wp.user_nicename),
        user_id: this.isNull(res.usermarket.id),
        wp_user_id: this.isNull(res.usermarket.wp_user_id),
        first_name: this.isNull(res.usermarket.name),
        last_name: this.isNull(res.usermarket.ap_paterno),
        secont_last_name: this.isNull(res.usermarket.ap_materno),
        billing_email:this.isNull(res.usermeta.email),
        billing_company:this.isNull(res.usermeta.billing_company),
        email_alt: this.isNull(res.usermarket.email_alt),
        enterprise_phone: this.isNull(res.usermarket.telefono_empresa),
        billing_name: '',
        rfc: this.isNull(res.usermarket.rfc),
        cp: this.isNull(res.usermeta.cp),
        billing_phone: this.isNull(res.usermarket.telefono_empresa),
        celphone: this.isNull(res.usermarket.celular) ,
        phone: this.isNull(res.usermeta.billing_phone),
        billing_address: this.isNull(res.usermeta.billing_address),
        comertial_name: this.isNull(res.usermarket.nombre_comercial),
        semblance: this.isNull(res.usermarket.semblanza),
        profile_avatar : this.isNull(res.usermarket.perfil_avatar),
        enterprise_avatar: this.isNull(res.usermarket.empresa_avatar),
        image_profile: '',
        image_enterprise: '',
        estado_id: this.isNull(res.usermarket.estado_id),
        country_id: this.isNull(this.countryId),
        ciudad: this.isNull(res.usermarket.ciudad),
        user_type: this.isNull(res.usermarket.tipo_usuario_id)
      }
    })
    .add( () => {
      sessionStorage.setItem('userData',JSON.stringify(this.infoUser));
    })
    .add( () => {this.info=true; this.allAdmin=true})
    .add( () => this.spinerIsVisible = false);
  }

  isNull(data){ 
    return data = data === void 0 || data === null ? '' : data; 
  }

  abrePerfil(){
    this.spinerIsVisible = true;
    this.dashboardService.getPersonalData(1).subscribe( res => {

      this.imgPerfil = this.isNull(res.usermarket.perfil_avatar);
      this.name = this.isNull(res.usermarket.name) +' '+ this.isNull(res.usermarket.ap_paterno);

      sessionStorage.setItem('countries',JSON.stringify(res.countries));
      sessionStorage.setItem('userType', JSON.stringify(this.isNull(res.usermarket.administrador)));
      sessionStorage.setItem('userId', res.usermarket.wp_user_id);
      sessionStorage.setItem('user_type', JSON.stringify(res.user_type));
      sessionStorage.setItem('users_type', JSON.stringify(res.users_type));
      
      for (let i = 0; i < res.countries.length; i++) {
        for (let j = 0; j < res.countries[i].states.length; j++) {
          if (res.countries[i].states[j].id == this.isNull(res.usermarket.estado_id)) {
            this.countryId = res.countries[i].id;
            break;
          }
        };
      };

      this.infoUser = {
        token:localStorage.token,
        administrador: this.isNull(res.usermarket.administrador),
        estatus: this.isNull(res.usermarket.estatus),
        nickname: this.isNull(res.usermarket.user_wp.user_nicename) ,
        user_id: this.isNull(res.usermarket.id),
        wp_user_id: this.isNull(res.usermarket.wp_user_id),
        first_name: this.isNull(res.usermarket.name),
        last_name: this.isNull(res.usermarket.ap_paterno),
        secont_last_name: this.isNull(res.usermarket.ap_materno),
        billing_email:this.isNull(res.usermeta.email),
        billing_company:this.isNull(res.usermeta.billing_company),
        email_alt: this.isNull(res.usermarket.email_alt),
        enterprise_phone: this.isNull(res.usermarket.telefono_empresa),
        billing_name: '',
        rfc: this.isNull(res.usermarket.rfc),
        cp: this.isNull(res.usermeta.cp),
        billing_phone: this.isNull(res.usermarket.telefono_empresa),
        celphone: this.isNull(res.usermarket.celular) ,
        phone: this.isNull(res.usermeta.billing_phone),
        billing_address: this.isNull(res.usermeta.billing_address),
        comertial_name: this.isNull(res.usermarket.nombre_comercial),
        semblance: this.isNull(res.usermarket.semblanza),
        profile_avatar : this.isNull(res.usermarket.perfil_avatar),
        enterprise_avatar: this.isNull(res.usermarket.empresa_avatar),
        image_profile: '',
        image_enterprise: '',
        estado_id: this.isNull(res.usermarket.estado_id),
        country_id: this.isNull(this.countryId),
        ciudad: this.isNull(res.usermarket.ciudad),
        user_type: this.isNull(res.usermarket.tipo_usuario_id)
      }
      this.cierraTodo();
      this.perfil = true;
    })
    .add( () => {
      sessionStorage.setItem('userData',JSON.stringify(this.infoUser));
    })
    .add( () =>{
      this.dashboardService.retrieveUserApplications()
      .subscribe( res => sessionStorage.setItem('vender',JSON.stringify(res)) )   
      .add(() => this.application = true); 
    }).add( () => 
    this.dashboardService.retrieveUserSolicitutes()
      .subscribe(
        response => sessionStorage.setItem('solicitar',JSON.stringify(response))
      ).add( () => this.solicitute = true)
    )
    .add( () => {this.info=true; this.allAdmin=true})
    .add( () => this.spinerIsVisible = false);

    this.dashboardPerfilRoute = 'Perfil';
    
  }

  abreVender(){
    this.cierraTodo();
    this.vender = true;
    this.dashboardPerfilRoute = 'Vender';
  }

  abreSolicitar(){
    this.cierraTodo();
    this.solicitar = true;
    this.dashboardPerfilRoute = 'Solicitar';
  }

  abreFavoritos(){
    this.cierraTodo();
    this.favoritos = true;
    this.dashboardPerfilRoute = 'Favoritos';
  }

  abreComprar(){
    this.cierraTodo();
    this.comprar = true;
    this.dashboardPerfilRoute = 'Comprar';
  }

  abreUsuarios(){
    this.cierraTodo();
    if (this.usuariosComponent){
      this.usuariosComponent.usersList();
    }
    this.usuarios = true;
    this.dashboardPerfilRoute = 'Usuarios';
  }

  abreProductos(){
    this.cierraTodo();
    this.productos = true;
    this.dashboardPerfilRoute = 'Productos';
  }

  abreSolicitudes(){
    this.cierraTodo();
    this.solicitudes = true;
    this.dashboardPerfilRoute = 'Solicitudes';
  }

  abreOfertas(){
    this.cierraTodo();
    this.ofertas = true;
    this.dashboardPerfilRoute = 'Ofertas';
  }

  abrePedidos(){
    this.cierraTodo();
    this.pedidos = true;
    this.dashboardPerfilRoute = 'Pedidos';
  }

  cierraTodo(){
    this.perfil = false;
    this.vender = false;
    this.solicitar = false;
    this.favoritos = false;
    this.comprar = false;
    this.usuarios = false;
    this.productos = false;
    this.solicitudes = false;
    this.pedidos = false;
    this.ofertas = false;

    window.scroll(0,0);
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(ModalCambiarContrasena, {
      width: '600px',
    });
  }

}

/* Modal cambiar contraseña */

@Component({
  selector: 'modal-cambiar-contrasena',
  templateUrl: '../../dashboard/modal-cambiar-contrasena.html',
})

export class ModalCambiarContrasena {
  
  closeDialog(): void {
    let dialogRef = this.dialog.closeAll();
  }
  password = '';
  confirmPassword = '';
  updated = false;
  msg = '';

  constructor(private dashboardService:DashboardPersonalService, public dialog: MatDialog) {}

  savePassword(){
    this.updated = false;
    if (this.password != this.confirmPassword) {
      this.updated = true;
      this.msg = 'Las contraseñas no coinciden';
    } else if ( this.password.length < 5) {
      this.updated = true;
      this.msg = 'Debe contener al menos 5 caracteres';
    } else {
      this.dashboardService.updatePassword(this.password).subscribe(res => {
        this.updated = true;
        this.msg = res[0];
      });
    }
  }
}