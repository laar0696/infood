import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAdministrativeComponent } from './dashboard-administrative.component';

describe('DashboardAdministrativeComponent', () => {
  let component: DashboardAdministrativeComponent;
  let fixture: ComponentFixture<DashboardAdministrativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardAdministrativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardAdministrativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
