import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IMyDpOptions,IMyDate } from 'mydatepicker';
import { CertificacionSchema } from '../../../schemas/certificacionSchema';
import { ImageProductSchema } from '../../../schemas/image-product-schema';
import { ActivatedRoute, Router } from '@angular/router';
import { SoliciteSchema } from '../../../schemas/solicitud-schema';
import { SolicitutesService } from '../../../servicios/solicitutes.service';
import { AdministradorProductosSolicitudesService } from '../../../servicios/administrador-productos.solicitudes.service';

@Component({
  selector: 'app-editar-solicitudes-administrador',
  templateUrl: './editar-solicitudes-administrador.component.html',
  styleUrls: ['./editar-solicitudes-administrador.component.css']
})
export class EditarSolicitudesAdministradorComponent implements OnInit {

  @ViewChild('imgProduct') imgProduct:ElementRef; 
  @ViewChild('imageRelated') imageRelated:ElementRef;

  pspinner = {  mode:'indeterminate' };
  spinerIsVisible= true;

  router:Router;

  actualDate = new Date();
  certificatesIsVisible = false;
  
  modalCertificatesIsVisible = false;
  modalConfirmIsVisible = false;

  certificates:CertificacionSchema[];
  solicitudSchema:SoliciteSchema;
  displayEstatus=false;
  isEnabled=true;
  
  categories=[];
  products=[];
  countries = [];
  unities = [];
  states = [];
  presentations = [];

  images:ImageProductSchema[]=[];

  certificates_images:CertificacionSchema[] = [];
  certificates_id:number[] = []
  checkCertificate:boolean[] = [];

  error:string[] = [];
  errorExist = false;


  constructor( public newProductService:SolicitutesService, _router:Router,
    public activeRoute:ActivatedRoute,  private productStatus:AdministradorProductosSolicitudesService 
  ) { this.router = _router;}

  public hastaOption: IMyDpOptions = null;
  public desdeOption: IMyDpOptions = null;

  seldate:IMyDate = null;
  seldate2:IMyDate = null;


  //public myDatePickerOptions: IMyDpOptions = null;

  ngOnInit() {
    //window.scroll(0,0);
    this.desdeOption={
      dateFormat: 'dd/mm/yyyy',
      height:'1cm',
      width:'85%',
      editableDateField: false,
      componentDisabled:true
    }

    this.hastaOption = {
      dateFormat: 'dd/mm/yyyy',
      height:'1cm',
      width:'85%',
      editableDateField: false,
      todayBtnTxt: ' Hoy',
      disableUntil: { 
                      year: this.actualDate.getFullYear(), 
                      month: this.actualDate.getMonth()+1,
                      day: this.actualDate.getDate()-1
                    }
    };
    

    this.newProductService.getBaseData().subscribe( res => {
      this.certificates = res.certificates;
      this.categories = res.categories;
      this.countries = res.countries;
      this.unities = res.unities;
      this.presentations = res.presentation;

      sessionStorage.setItem('certificates',JSON.stringify(res.certificates));
    }).add(() =>{
      this.newProductService
    .getEditApplicationData( parseInt( this.activeRoute.snapshot.paramMap.get('id')) )
    .subscribe(res => {
        this.solicitudSchema = res.ofert;

        if(res.permisionOptions == 1){
          this.displayEstatus = true;
        }
        else{
          this.router.navigate([`editar-producto-solicitado/${this.activeRoute.snapshot.paramMap.get('id')}`]);
        }
        
        res.images.forEach(image => {
          this.images.push({type:'default',imagen: image.imagen, data:image});
        });
        res.certificatesOffert.forEach(certificate =>{
          let image = this.certificates.find( cert => cert.id == certificate.certificacion_id );
          image !== void 0 ? this.certificates_images.push({id:image.id,nombre:image.nombre ,imagen:image.imagen}): '';
        });
        this.solicitudSchema.id = parseInt(this.activeRoute.snapshot.paramMap.get('id'));
      })
      .add( () => {
        if(this.solicitudSchema.estado_id != 0){
          this.countries.forEach( country => {
            this.categories.forEach( category => {
              for(let product of category.products){
                if( product.id == this.solicitudSchema.producto_id ){
                  this.solicitudSchema.categoria = product.categoria_id;
                  this.products = category.products;
                }
              };
            });
          });
        }else this.solicitudSchema.pais_id = 0;
      }).add( () => {
        this.categories.forEach( category => {
          for(let product of category.products){
            if( product.id == this.solicitudSchema.producto_id ){
              this.solicitudSchema.categoria = product.categoria_id;
              this.products = category.products;
            }
          };
        });
      }) 
      .add(()=>{
        this.solicitudSchema.photo = this.images;
        this.solicitudSchema.certificates = this.certificates_images;
        this.isEnabled = this.solicitudSchema.aprobado == 1 ? true:false;
      })
      .add( ()=> {
        this.certificates.forEach( cert =>{
          let isHere = this.certificates_images.find(cert_image => cert_image.id === cert.id);
          isHere === void 0 ? this.checkCertificate.push(false) : this.checkCertificate.push(true);
        });
      })
      .add( () => {
        let aux = this.solicitudSchema.requerido_desde.split(' ');
        let date = aux[0].split('-');
        this.seldate = {year: parseInt(date[0]), month: parseInt(date[1]) , day: parseInt(date[2]) };

        let aux2 = this.solicitudSchema.requerido_hasta.split(' ');
        let date2 = aux2[0].split('-');
        this.seldate2 = {year: parseInt(date2[0]), month: parseInt(date2[1]) , day: parseInt(date2[2]) };
      }).add( () => {
        this.countries.forEach( country => {
          for(let state of country.states){
            if( state.id == this.solicitudSchema.estado_id ){
              this.solicitudSchema.pais_id = state.pais_id;
              this.states = country.states;
            }
          };
        });
      });
    }).add( () => this.spinerIsVisible = false);

    this.solicitudSchema = {
      producto_id:0,
      variedad: null,
      otras_caracteristicas: null,
      volumen_total: null,
      unidad_id: 0,
      precio_unidad: null,
      presentacion_id: 0,
      tamano: null,
      unidad_presentacion_id: 0,
      pais_id: 0,
      estado_id: 0,
      ciudad: null,
      codigo_postal: null,
      requerido_desde: null,
      requerido_hasta: null,
      resena: null,
      photo:this.images,
      certificates:this.certificates_images,
      categoria:0
    };
  }

  openInputImageProduct(){
    this.images.length < 5  ? this.imgProduct.nativeElement.click() : ''; 
  }

  dropCertificatesList(){
    this.certificatesIsVisible = this.certificatesIsVisible ? false : true;
  }

  closeCertificatesList(){
    this.certificatesIsVisible = false;
  }

  chargeStates(){
    const states = this.countries.find( res => res.id == this.solicitudSchema.pais_id );
    this.states = states.states;
    this.solicitudSchema.estado_id = 0;
  }

  chargeProducts(){
    const product = this.categories.find( res => res.id == this.solicitudSchema.categoria );
    this.products = product.products;
    this.solicitudSchema.producto_id = 0;
  }

  chargeImageNew(event){
    let e = event.target.files;
    e !== void 0 && e.length ? this.saveImages(0,e) : '';
  }

  saveImages( index:number, files ){
    if(index < files.length){
      const reader = new FileReader();  
      reader.onload = (event: any) => {
        this.images.length < 5 ? this.images.push({type:'up',imagen:event.target.result}):'';
      }
      
      reader.readAsDataURL(files[index]);
        
      this.saveImages( index+1, files);
    };
  }

  eraseImage(index){
    this.images.splice(index, 1);;    
  }

  changeDefaultImagesProduct(){
    let image = this.products.find(res => res.id == this.solicitudSchema.producto_id);
    if( this.images.length === 0 || this.images.length <= 1 ) {
      this.images[0] = {type:'default',imagen:image.imagen_default};
    } 
  }

  selectCertified(id,i){
    
    let found_image = this.certificates_images.find( data => data.id == id );
    let index = this.certificates_images.indexOf( id );

    if ( found_image === void 0 ) {
      let found = this.certificates.find( data => data.id == id);
      this.certificates_images.push(found);
    }
    else this.certificates_images.splice( index, 1 );
    
    this.checkCertificate[i] = !this.checkCertificate[i] ? true : false;
    
  }

  addCertificate(){
    this.modalCertificatesIsVisible = true;
  }

  closeCertificate(evnt){
    //this.certificates = null;
    let certificates = JSON.parse(sessionStorage.getItem('certificates'));

    if(certificates !== void 0 && certificates !== null){
      this.certificates = certificates;
    }
    this.modalCertificatesIsVisible = evnt;
  }
  
  saveOfert(){
    this.newProductService.updateApplicationData(this.solicitudSchema)
    .subscribe( res => { 

      if(res !== null){
        if( res.error !== void 0 ) {
          this.errorExist = true;
          this.error = res.error;
        }
      }else {
        this.modalConfirmIsVisible=true;
        sessionStorage.setItem('displayInfood','solicitar-admin');
      };
    
    });
  }

  cancel(){
    sessionStorage.setItem('displayInfood','solicitar-admin');
    this.router.navigate(['dashboard']);
  }

  enableProduct(){
    this.productStatus.changeStatusSolicitude( parseInt( this.activeRoute.snapshot.paramMap.get('id')) )
    .subscribe(() => {
      this.isEnabled = this.isEnabled ? false : true;
    });
  }

  redirect(to:number){
    switch(to){
      case 0:
        this.router.navigate(['dashboard']);
      break;
      case 1:
        sessionStorage.setItem('displayInfood','solicitar-admin');
        this.router.navigate(['dashboard']);
      break;
    }
  }


  navigateToReferencePrice(){
    window.open('http://infood.com.mx');
  }

}

