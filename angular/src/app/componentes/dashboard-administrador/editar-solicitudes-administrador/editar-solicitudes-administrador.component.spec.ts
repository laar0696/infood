import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarSolicitudesAdministradorComponent } from './editar-solicitudes-administrador.component';

describe('EditarSolicitudesAdministradorComponent', () => {
  let component: EditarSolicitudesAdministradorComponent;
  let fixture: ComponentFixture<EditarSolicitudesAdministradorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarSolicitudesAdministradorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarSolicitudesAdministradorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
