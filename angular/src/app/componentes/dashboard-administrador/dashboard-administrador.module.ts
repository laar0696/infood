import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardAdministrativeComponent, ModalCambiarContrasena } from './dashboard-administrative/dashboard-administrative.component';
import { DashboardModule } from '../dashboard/dashboard.module';
import { AdministradorUsuariosComponent } from './administrador-usuarios/administrador-usuarios.component';
import { AdministradorProductosComponent } from './administrador-productos/administrador-productos.component';
import { AdministradorSolicitudesComponent } from './administrador-solicitudes/administrador-solicitudes.component';
import { FormsModule } from '@angular/forms';
import { EditarProductosAdministradorComponent } from './editar-productos-administrador/editar-productos-administrador.component';
import { MyDatePickerModule } from 'mydatepicker';
import { ProductosModule } from '../vender-productos/productos.module';
import { EditarSolicitudesAdministradorComponent } from './editar-solicitudes-administrador/editar-solicitudes-administrador.component';
import { AdministradorPedidosComponent } from './administrador-pedidos/administrador-pedidos.component';
import { AdministradorOfertasComponent } from './administrador-ofertas/administrador-ofertas.component';
import { MaterialModule } from '../../material/material.module';
import { ModalConfirmarEliminacion } from './administrador-usuarios/administrador-usuarios.component';

@NgModule({
  imports: [
    CommonModule,DashboardModule,FormsModule, MyDatePickerModule, ProductosModule, MaterialModule
    
  ],
  declarations: [
    DashboardAdministrativeComponent,
    AdministradorUsuariosComponent,
    AdministradorProductosComponent,
    ModalCambiarContrasena,
    AdministradorSolicitudesComponent,
    EditarProductosAdministradorComponent,
    EditarSolicitudesAdministradorComponent,
    AdministradorPedidosComponent,
    AdministradorOfertasComponent,
    ModalConfirmarEliminacion
  ],
  entryComponents: [
    ModalCambiarContrasena,
    ModalConfirmarEliminacion
  ],
  exports: [
    ModalCambiarContrasena,
    ModalConfirmarEliminacion
  ]
})
export class DashboardAdministradorModule { }
