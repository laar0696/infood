import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ProductSchema } from '../../../schemas/productSchema';
import { Router } from '@angular/router';
import { VenderService } from '../../../servicios/vender.service';
import { DashboardPersonalService } from '../../../servicios/dashboard-personal.service';

@Component({
  selector: 'app-administrador-productos',
  templateUrl: './administrador-productos.component.html',
  styleUrls: ['./administrador-productos.component.css']
})
export class AdministradorProductosComponent implements OnInit {

  pspinner = {  mode:'indeterminate' };
  spinerIsVisible= true;

  productos:ProductSchema[];
  productosAux: ProductSchema[];
  
  modalDisplay = false;
  legend:string = '';
  method:string = '';
  id_solicitate:number = 0;

  public paginatorConf = {
    length: 0,
    pageSize: 8,
    pageSizeOptions: [ 2, 4, 8, 16 ],
    pageIndex : 0
  };

  search:string = '';

  @Input () infoUser:boolean;

  constructor(router:Router,private venderHttp:VenderService, private dashService:DashboardPersonalService) { this.routes = router; }
  routes:Router;

  ngOnInit() { 
    this.dashService.retrieveAllUserApplications()
      .subscribe( res =>{ 
        this.productos = res;
        this.productosAux = res;
        sessionStorage.totalProduct = JSON.stringify(res);
        this.paginatorConf.length = this.productos.length;
      })
      .add( () => {
        this.productos.reverse()
        this.getData(0,this.paginatorConf.pageSize);
      })
      .add( () => this.spinerIsVisible = false );
  }

  ngOnChanges(){}

  eliminar(index:number) {
    this.id_solicitate =  this.productos[index].id;
    this.legend = `¿Desea eliminar el contenido de ${this.productos[index].nombre}?`;
    this.method = 'eliminar-producto-administrador';
    this.modalDisplay = true;
  }

  editar(index:number) {
    let id = this.productos[index].id;
    sessionStorage.setItem('origin','administrative');
    this.routes.navigate(['editar-producto-administrativo/'+id])
  }

  detalle(index:number) {
    let id = this.productos[index].id;
    sessionStorage.setItem('originProductDetailsSolicituted','administrador');
    this.routes.navigate(['detalle-producto-venta/'+id])
  }

  closeModal(event){
    if(event){
      this.spinerIsVisible = true;
    }else{

      if( sessionStorage.totalSolicitutes !== void 0 ) {
        this.productosAux = JSON.parse(sessionStorage.totalProduct);
        this.getData(this.paginatorConf.pageIndex,this.paginatorConf.pageSize);
        this.paginatorConf.length = JSON.parse(sessionStorage.totalProduct).length;
        this.productos.reverse();
      }

      this.spinerIsVisible = false;
      this.modalDisplay = false;
    }
  }

  findIn(){

    let aux = [];
    this.productos = JSON.parse(sessionStorage.totalProduct);
    
    if( this.search.length > 0 ){
      this.productos.forEach(element => {
        if(element.nombre.toLowerCase().match(this.search.toLowerCase())) aux.push(element);
      });
      
      this.productosAux = aux.reverse();
    }else{
      this.productosAux = JSON.parse(sessionStorage.totalProduct).reverse();
    }
  
    this.getData( 0 , this.paginatorConf.pageSize );
  }

  page(event){
    this.paginatorConf.pageIndex = event.pageIndex;
    this.paginatorConf.pageSize = event.pageSize;
    this.getData(event.pageIndex,event.pageSize);
  }

  getData(pageIndex:number, pageSize:number){
    this.paginatorConf.length = this.productosAux.length;
    if( this.productosAux.length < pageSize ){
      this.productos = [];
      this.productos = this.productosAux.map( (obj) =>({...obj}));
    }else{
      this.productos = this.productosAux.slice( (pageIndex * pageSize), (pageIndex * pageSize) + pageSize );  
    }
  }


}
