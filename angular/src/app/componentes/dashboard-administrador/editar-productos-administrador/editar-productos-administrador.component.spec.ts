import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarProductosAdministradorComponent } from './editar-productos-administrador.component';

describe('EditarProductosAdministradorComponent', () => {
  let component: EditarProductosAdministradorComponent;
  let fixture: ComponentFixture<EditarProductosAdministradorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarProductosAdministradorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarProductosAdministradorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
