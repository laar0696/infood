import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JarwisService } from '../../servicios/jarwis.service';
import { TokenService } from '../../servicios/token.service';
import { AuthService } from '../../servicios/auth.service';
import { MatDialog } from '@angular/material';
import { LoadingModalComponent } from '../loading-modal/loading-modal.component';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public captcha:boolean = false;
  public terminos:boolean = true;
  public btn_msg = "CREAR CUENTA";

  public form = {
    email: null,
    name: null,
    password: null,
    password_confirmation: null
  };

  public error = null;

  constructor(
    private Jarwis: JarwisService,
    private Token: TokenService,
    private router: Router,
    private Auth: AuthService,
    private modal:MatDialog
  ) { }

  onSubmit(form) {
    if (this.terminos && this.captcha && form.valid) {
      this.btn_msg = "REGISTRANDO CUENTA..";
      this.cargando();
      this.Jarwis.signup(this.form).subscribe(
        data => {
          this.handleResponse(data);
        },
        error => {
          this.handleError(error);
          this.btn_msg = "CREAR CUENTA"; 
          this.modal.closeAll();   
        }
      );
    } else {
      if (!form.valid) {
        if (form.form.controls.email.status == "INVALID") {
          this.error = "Direccion de correo inválida";
        } else {
          this.error = "Debes completar todos los campos"
        }
      } else {
        this.error = "Debes resolver el captcha y aceptar los terminos"
      }
    }
  }

  resolved(captchaResponse: string) {
    this.captcha = true;
  }

  handleResponse(data) {
    this.Token.handle(data.access_token);
    this.Auth.changeAuthStatus(true);
    //this.router.navigateByUrl('/dashboard');
    this.modal.closeAll();
    this.router.navigate([`dashboard`]);
  }

  handleError(error) {
    this.error = error.error.error;
  }

  ngOnInit() {
    
  }

  cargando(){
    this.modal.open(LoadingModalComponent,{
      disableClose: true
    });
  }


}
