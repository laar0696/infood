import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SolicitudSchema } from '../../../schemas/solicitudSchema';
import { MarketService } from '../../../servicios/market.service';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-detalles-solicitud',
  templateUrl: './detalles-solicitud.component.html',
  styleUrls: ['./detalles-solicitud.component.css']
})
export class DetallesSolicitudComponent implements OnInit {

  next:string;
  prev:string;
  position:number;

  public id:number;

  public solicitud:SolicitudSchema;

  public pedido = { cantidad: 1, total: 0 };

  public pspinner = {  mode:'indeterminate' };

  public solicitudesSimilares: SolicitudSchema[] = [];

  public imageUrls: string [] = [];

  public actualImage;
  @ViewChild('imagenesSolicitud') public gallery: any;

  spinnerIsVisible= true;

  constructor( public dialog: MatDialog, private marketService: MarketService, private route:ActivatedRoute, private router:Router ) {
    this.id = this.route.snapshot.params['id'];
    this.getSolicitud();
   }

   public paginatorConf = {
    length: 0,
    pageSize: 8,
    pageIndex: 0,
    pageSizeOptions: [ 2, 4, 8, 16 ]
  };

  redirect(to:number) {
    switch (to) {
      case 0:
        this.router.navigate(['/']);
        break;
        case 1:
        this.router.navigate(['marketplace-busqueda-productos']);
        break;
    }
  }

  getSolicitud(){
    this.solicitud = null;
    this.imageUrls = [];
    window.scrollTo(0, 0);
    this.marketService.getSolicitud( this.id ).subscribe( data => {
      this.solicitud = data;
      if( this.solicitudesSimilares.length == 0) 
        this.getSolicitudesSimilares( this.solicitud.producto_id );
      this.setImagenes( data.imagenes );
    }).add( () => this.spinnerIsVisible = false );
   }

   page(event){
    this.solicitudesSimilares = [];
    this.paginatorConf.pageIndex = event.pageIndex + 1;
    this.paginatorConf.pageSize = event.pageSize;
    this.getSolicitudesSimilares(this.solicitud.producto_id);    
   }

   setImagenes( imagenes:any[] ){
    this.gallery = imagenes;
    this.actualImage = this.gallery[0]?this.gallery[0].imagen:null;
   }

   changeImage(i:number){
    this.actualImage = this.gallery[i].imagen;
    this.position = i;
  }

  nextImage(){
    if( this.position < this.gallery.length-1 ){
      this.position++;
      this.actualImage = this.gallery[this.position].imagen;
    } 
  }

  prevImage(){
    if(this.position > 0){
      this.position--;
      this.actualImage = this.gallery[this.position].imagen;
    }
  }

   getSolicitudesSimilares(producto_id){
     this.marketService.getSolicitudesSimilares( producto_id, this.paginatorConf.pageIndex, this.paginatorConf.pageSize ).subscribe( data => {
       this.solicitudesSimilares = data.data;
       this.paginatorConf.length = data.total;
     });
   }

   detalleSolicitud(id:number){
    this.id = id;
    this.getSolicitud(); 
  }

  agregarCompra(){  
    this.spinnerIsVisible= true;
    this.marketService.compraSolicitud(this.solicitud.id, this.pedido.cantidad).subscribe(data => {
      this.spinnerIsVisible = false;
      sessionStorage.idSol = JSON.stringify(data.msg);
    }).add( () => this.openDialog() );
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(ModalComprobacionCompra, {
      width: '600px',
    });
  }
   
  ngOnInit() {
    this.next = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADxSURBVEhLtdQ7akJRFEbhq1ELo3E04lshRYo0goWtY7G2sLDRxsJKCwdg55tMJWkSX4ggmLVB4bCxEM52wYdg8W/wXgweKHr9NC+GMX5RkS+sG+FydUAZZoXxjdsBsUcJZmXxB/fIDkWYVYCMuke2MD0iv708A31Ejpv1Dn1kgzzM+sAR7hF5RjmY9YkT9JEMzKri3pE0zKrDPSB+EIF3LxhCH5jAOxkfQI9PEYdXIfShx2cwGe9Bj8/xCq9kvAM9vkAC3rWhx5dIwrsWnjbehB5f4Q3eyeso/5ju+BopmNXAGTL+BdPxWzV08ZRxg4LgH/UwaZ/ZnR8/AAAAAElFTkSuQmCC';
    this.prev = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADuSURBVEhLtdQ7akJRFIXha2ETBEkRSJfKGQiSGMXGwsJCJ+BkNEjwUaXMYATBQrAQUosz8Ikogvk3GDhsCAhn+cOHYLG2esEksvT1VV4KPezRsjeU2fgQl6szGpAVjv/pQ9IAfnyEB0Rnv7kfHyOD6D7hxyfIIrou/PgUkvEP+PEZHhFdB358jidE14Yf/4FkvA4/vsQzJOWwQnjghCZkFeCPHGHfTtZ/R2qQ9Qp/5IAqZL1hjfCI/U1XIKuIDfyREmS9Y4vwiB21byjLPvEO4RF7RnnIKsMf+YI0O2LPwMYXeIE8e/DfuMv4jSXJL02WaZRNaaEmAAAAAElFTkSuQmCC';

    this.position = 0;
  }

}


/* Modal comprobacion compra */


@Component({
  selector: 'modal-comprobacion-compra',
  templateUrl: 'modal-comprobacion-compra.html',
})
export class ModalComprobacionCompra{
  
  closeDialog(): void {
    let dialogRef = this.dialog.closeAll();
    let id = JSON.parse(sessionStorage.idSol)
    this.route.navigate([`detalles-compra-producto/${id}`]);
  }

  constructor(public dialog: MatDialog, private route:Router) {}

}