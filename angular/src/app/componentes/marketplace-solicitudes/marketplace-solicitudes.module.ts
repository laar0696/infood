import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VistaSolicitudesComponent } from './vista-solicitudes/vista-solicitudes.component';
import { TodoSolicitudesComponent } from './todo-solicitudes/todo-solicitudes.component';
import { MyDatePickerModule } from 'mydatepicker';
import { DetallesSolicitudComponent, ModalComprobacionCompra } from './detalles-solicitud/detalles-solicitud.component';
import { DashboardModule } from '../dashboard/dashboard.module';
import { FormsModule } from '@angular/forms';
import { SchemasModule } from '../../schemas/schemas.module';
import { MaterialModule } from '../../material/material.module';
import { DetallesCompraSolicitudComponent } from './detalles-compra-solicitud/detalles-compra-solicitud.component';
import {AngularOpenlayersModule} from 'ngx-openlayers';

@NgModule({
  imports: [
    CommonModule, MyDatePickerModule, DashboardModule, SchemasModule, FormsModule, MaterialModule, AngularOpenlayersModule
  ],
  entryComponents: [
    ModalComprobacionCompra
  ],
  exports: [ VistaSolicitudesComponent, FormsModule],
  declarations: [ VistaSolicitudesComponent, TodoSolicitudesComponent, DetallesSolicitudComponent, ModalComprobacionCompra, DetallesCompraSolicitudComponent]
})
export class MarketplaceSolicitudesModule { }
