import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesCompraSolicitudComponent } from './detalles-compra-solicitud.component';

describe('DetallesCompraSolicitudComponent', () => {
  let component: DetallesCompraSolicitudComponent;
  let fixture: ComponentFixture<DetallesCompraSolicitudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallesCompraSolicitudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesCompraSolicitudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
