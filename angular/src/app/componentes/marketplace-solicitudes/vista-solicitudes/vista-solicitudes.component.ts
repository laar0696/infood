import { Component, OnInit } from '@angular/core';
import { MarketService } from '../../../servicios/market.service';
import { SolicitudSchema } from '../../../schemas/solicitudSchema';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-vista-solicitudes',
  templateUrl: './vista-solicitudes.component.html',
  styleUrls: ['./vista-solicitudes.component.css']
})
export class VistaSolicitudesComponent implements OnInit {

  public solicitudes: SolicitudSchema[];

  public siteUrl: string = environment.siteUrl;

  constructor(private marketService: MarketService) { 
    this.marketService.getSolicitudesHome().subscribe( data => {
      this.solicitudes = data;
    });
  }


  ngOnInit() {
  }

}
