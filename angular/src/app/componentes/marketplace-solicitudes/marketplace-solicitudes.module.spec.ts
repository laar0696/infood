import { MarketplaceSolicitudesModule } from './marketplace-solicitudes.module';

describe('MarketplaceSolicitudesModule', () => {
  let marketplaceSolicitudesModule: MarketplaceSolicitudesModule;

  beforeEach(() => {
    marketplaceSolicitudesModule = new MarketplaceSolicitudesModule();
  });

  it('should create an instance', () => {
    expect(marketplaceSolicitudesModule).toBeTruthy();
  });
});
