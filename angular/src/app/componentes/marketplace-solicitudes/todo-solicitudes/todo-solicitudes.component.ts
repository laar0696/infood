import { Component, OnInit } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { MarketService } from '../../../servicios/market.service';
import { CertificacionSchema } from '../../../schemas/certificacionSchema';
import { EstadoSchema } from '../../../schemas/estadoSchema';
import { ProductoSchema } from '../../../schemas/productoSchema';
import { CategoriaSchema } from '../../../schemas/categoriaSchema';
import { OfertaSchema } from '../../../schemas/ofertaSchema';
import { Router, ActivatedRoute } from '@angular/router';
import { SolicitudSchema } from '../../../schemas/solicitudSchema';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-todo-solicitudes',
  templateUrl: './todo-solicitudes.component.html',
  styleUrls: ['./todo-solicitudes.component.css']
})
export class TodoSolicitudesComponent implements OnInit {


  public pspinner = {  mode:'indeterminate' };

  public cualquier : { 'id' : 0};

  public filtroModal = false;

  public paginatorConf = {
    length: 0,
    pageSize: 8,
    pageIndex: 0,
    pageSizeOptions: [ 2, 4, 8, 16 ]
  };

  public filtros = {
    categoria: this.cualquier,
    producto: this.cualquier,
    estado: this.cualquier,
    certificacion: this.cualquier,
    fecha: null,
    precio: 0,
    busqueda: null,
    paginador: {     
      length: 0,
      pageIndex: 0,
      pageSize:4
    }
  };

  public siteUrl: string = environment.siteUrl;
  
  public maxPrice;

  public solicitudes: SolicitudSchema[]
  public categorias: CategoriaSchema[]
  public productos: ProductoSchema[]
  public estados: EstadoSchema[]
  public certificaciones: CertificacionSchema[]


  constructor(private marketService: MarketService, private route: ActivatedRoute, private router: Router) {

    this.filtros.busqueda = this.route.snapshot.params['buscar'];
  
    
    this.getSolicitudes();

    this.getCategorias();
    
    this.getEstados();

    this.getCertificaciones();

    this.getProductos();

    let date = new Date();
    

  }


  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
  };

  alternaModal(){
    this.filtroModal = this.filtroModal?false:true;
  }

  closeModal() {
    this.filtroModal = false;
  }

  redirect(to:number) {
    switch (to) {
      case 0:
        this.router.navigate(['/']);
        break;
        case 1:
        window.location.reload();
        break;
    }
  }

  getCategorias(){
    this.marketService.getCategorias().subscribe( data => {
      this.categorias = data;
    });
  }

  getProductos(){
    this.marketService.getProductos( this.filtros.categoria ).subscribe( data => {
      this.productos = data;
    });
  }

  getEstados(){
    this.marketService.getEstados().subscribe( data => {
      this.estados = data;
    });
  }

  getCertificaciones(){
    this.marketService.getCertificaciones().subscribe( data => {
      this.certificaciones = data;
    });
  }


  getSolicitudes(){
    this.solicitudes = null;
    this.marketService.getSolicitudes(this.filtros).subscribe( data => {
      this.solicitudes = data.items;
      this.maxPrice = data.paginator.maxPrice?data.paginator.maxPrice:0;
      this.paginatorConf.length = data.paginator.length;
      this.paginatorConf.pageSize = data.paginator.pageSize;
    });
  }

  buscar() {
    this.filtros.paginador.pageIndex = 0;
    this.paginatorConf.pageIndex = 0;
    this.getSolicitudes();
  }

  detalleSolicitud(id:number){
    this.router.navigate([`marketplace-detalles-solicitud/${id}`]);
  }

  page(event){
    if (event.pageSize != this.paginatorConf.pageSize) {
      this.paginatorConf = event;
      this.paginatorConf.pageSizeOptions = [ 2, 4, 8, 16 ];
      this.paginatorConf.pageIndex = 0;
      this.filtros.paginador = this.paginatorConf;
    } else {
      this.paginatorConf = event;
      this.paginatorConf.pageSizeOptions = [ 2, 4, 8, 16 ];
      this.filtros.paginador = event;
    }
    this.getSolicitudes();
  }

  ngOnInit() {
      
  }

}
