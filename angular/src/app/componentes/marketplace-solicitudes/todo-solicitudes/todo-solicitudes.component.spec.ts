import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoSolicitudesComponent } from './todo-solicitudes.component';

describe('TodoSolicitudesComponent', () => {
  let component: TodoSolicitudesComponent;
  let fixture: ComponentFixture<TodoSolicitudesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoSolicitudesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoSolicitudesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
