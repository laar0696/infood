import { Component, OnInit } from '@angular/core';
import { ProductorsService } from '../../../servicios/productors.service';
import { Router } from '@angular/router';
// import {MatPaginatorModule} from '@angular/material/paginator';

@Component({
  selector: 'app-todo-productores',
  templateUrl: './todo-productores.component.html',
  styleUrls: ['./todo-productores.component.css']
})
export class TodoProductoresComponent implements OnInit {

  constructor(private productorsService: ProductorsService, private router: Router) { }
  pspinner = {  mode:'indeterminate' };
  spinerIsVisible= true;

  public productors = [];
  public categories = [];
  public states = [];
  public selectedCategory = 0;
  public selectedState = 0;
  public pageNumber = 1;
  public five = [1,2,3,4,5];
  public paginator;
  public paginatorConf = {
    length: 0,
    pageSize: 8,
    pageIndex: 0,
    pageSizeOptions: [ 2, 4, 8, 16 ]
  };

  nextPage(){
    if( this.paginator.next_page_url ){
    this.productors = [{productorName:'Cargando datos...'}];
    this.pageNumber = this.pageNumber+1;
      this.productorsService.getPaginator(this.pageNumber,this.selectedCategory,this.selectedState,this.paginatorConf.pageSize)
      .subscribe(data => {
        this.paginator = data.paginator;
        this.productors = data.productors;
        this.spinerIsVisible = false;
      });
    }
  }

  prevPage(){
    if( this.paginator.prev_page_url ){
      this.pageNumber = this.pageNumber-1;
      this.productors = [{productorName:'Cargando datos...'}];
      this.productorsService.getPaginator(this.pageNumber,this.selectedCategory,this.selectedState, this.paginatorConf.pageSize)
      .subscribe(data => {
        this.paginator = data.paginator;
        this.productors = data.productors;
        this.spinerIsVisible = false;
      });
    }
  }

  redirect(to:number) {
    switch (to) {
      case 0:
        this.router.navigate(['marketplace-busqueda-producto/']);
        break;
    }
  }

  buscar() {
    this.spinerIsVisible = true;
    this.pageNumber = 1;
    this.productors = [{productorName:'Cargando datos...'}];
    this.paginatorConf.pageIndex = 0;
    this.productorsService.getPaginator(this.pageNumber,this.selectedCategory,this.selectedState, this.paginatorConf.pageSize)
    .subscribe(data => {
      this.paginator = data.paginator;
      this.productors = data.productors;
      this.paginatorConf.length = data.paginator.total;
      this.paginatorConf.pageIndex = 0;
      this.spinerIsVisible = false;
    });
  }

  page($event) {
    this.spinerIsVisible = true;
    if ($event.pageSize != this.paginatorConf.pageSize) {
      this.paginatorConf = $event;
      this.paginatorConf.pageSizeOptions = [ 2, 4, 8, 16 ];
      this.buscar();
      return;
    }

    if ($event.pageIndex > this.paginatorConf.pageIndex) {
      this.nextPage();
    } else if ($event.pageIndex < this.paginatorConf.pageIndex) {
      this.prevPage();
    }

    this.paginatorConf.pageIndex = 0;
    this.paginatorConf = $event;
    this.paginatorConf.pageSizeOptions = [ 2, 4, 8, 16 ];
  }

  productorDetail(id){
    this.router.navigate(['/productor-detalle', id]);
  }

  ngOnInit() {
    this.productors = [{productorName:'Cargando datos...'}];
    this.paginator = {next_page_url:null};
    this.productorsService.getPaginator(this.pageNumber,this.selectedCategory,this.selectedState,this.paginatorConf.pageSize)
      .subscribe(data => {
        this.paginator = data.paginator;
        this.productors = data.productors;
        this.paginatorConf.length = data.paginator.total;
        this.paginatorConf.pageSize = data.paginator.per_page;
        this.spinerIsVisible = false;
      });

    this.productorsService.getCategories()
      .subscribe(data => {
        this.categories = data;
      });

    this.productorsService.getStates()
      .subscribe(data => {
        this.states = data;
      });
  }

}
