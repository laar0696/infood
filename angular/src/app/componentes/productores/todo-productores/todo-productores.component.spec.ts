import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoProductoresComponent } from './todo-productores.component';

describe('TodoProductoresComponent', () => {
  let component: TodoProductoresComponent;
  let fixture: ComponentFixture<TodoProductoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoProductoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoProductoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
