import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesProductorComponent } from './detalles-productor.component';

describe('DetallesProductorComponent', () => {
  let component: DetallesProductorComponent;
  let fixture: ComponentFixture<DetallesProductorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallesProductorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesProductorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
