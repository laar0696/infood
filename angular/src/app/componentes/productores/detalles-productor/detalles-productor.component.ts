import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// import { Location } from '@angular/common';
import { ProductorsService } from '../../../servicios/productors.service';

@Component({
  selector: 'app-detalles-productor',
  templateUrl: './detalles-productor.component.html',
  styleUrls: ['./detalles-productor.component.css']
})
export class DetallesProductorComponent implements OnInit {
  pspinner = {  mode:'indeterminate' };
  spinerIsVisible= true;

  public productor;
  public noProducts = false;
  public five = [1,2,3,4,5];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private productorsService: ProductorsService
  ) { }

  ngOnInit() {
    this.getProductorDetail();
  }

  getProductorDetail() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.productorsService.getProductor(id).subscribe( data => {
      this.productor = data;
      this.spinerIsVisible = false;
      if ( this.productor.products.length == 0 ) {
        this.noProducts = true;
      }
    });
  }

  productDetail(id:number) {
    this.router.navigate(['marketplace-detalles-producto/'+id]);
  }

  redirect(to:number) {
    switch (to) {
      case 0:
        this.router.navigate(['marketplace-busqueda-producto/']);
        break;
      case 1:
        this.router.navigate(['productores/']);
        break;
    }
  }
}