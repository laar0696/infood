import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoProductoresComponent } from './todo-productores/todo-productores.component';
import { DetallesProductorComponent } from './detalles-productor/detalles-productor.component';
import { MaterialModule } from '../../material/material.module';
import { DashboardModule } from '../dashboard/dashboard.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, DashboardModule, MaterialModule, FormsModule
  ],
  declarations: [TodoProductoresComponent, DetallesProductorComponent]
})
export class ProductoresModule { }
