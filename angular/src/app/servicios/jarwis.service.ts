import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { TokenService } from './token.service';
import { Observable } from 'rxjs';

@Injectable()
export class JarwisService {

  private apiUrl:string = environment.apiUrl;

  private Token: TokenService;

  constructor(private http: HttpClient) { }

  signup(data) {
    return this.http.post(`${this.apiUrl}/signup`, data)
  }
  
  //Login laravel
  login(data) {
    return this.http.post(`${this.apiUrl}/login`, data)
  }

  loginGoogle(data) {
    return this.http.post(`${this.apiUrl}/loginGoogle`, data)
  }

  loginFacebook(data) {
    return this.http.post(`${this.apiUrl}/loginFacebook`, data)
  }

  public getPersonalData():Observable<any>{
    return this.http.get(this.apiUrl+'/me');
  }

  sendPasswordResetLink(data) {
    return this.http.post(`${this.apiUrl}/sendPasswordResetLink`, data)
  }
  
  changePassword(data) {
    return this.http.post(`${this.apiUrl}/resetPassword`, data)
  }

  
}
