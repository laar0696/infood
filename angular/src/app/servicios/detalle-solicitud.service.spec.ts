import { TestBed, inject } from '@angular/core/testing';

import { DetalleSolicitudService } from './detalle-solicitud.service';

describe('DetalleSolicitudService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DetalleSolicitudService]
    });
  });

  it('should be created', inject([DetalleSolicitudService], (service: DetalleSolicitudService) => {
    expect(service).toBeTruthy();
  }));
});
