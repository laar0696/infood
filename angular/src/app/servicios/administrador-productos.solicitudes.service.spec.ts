import { TestBed, inject } from '@angular/core/testing';

import { AdministradorProductos.SolicitudesService } from './administrador-productos.solicitudes.service';

describe('AdministradorProductos.SolicitudesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdministradorProductos.SolicitudesService]
    });
  });

  it('should be created', inject([AdministradorProductos.SolicitudesService], (service: AdministradorProductos.SolicitudesService) => {
    expect(service).toBeTruthy();
  }));
});
