import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DetalleSolicitudService {
  apiUrl:String = environment.apiUrl;

  constructor(private http: HttpClient){ }

  public getDataSolicitude(id:number):Observable<any>{
    let body = JSON.stringify(id);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/getDataSolicitude?token=${localStorage.getItem('token')}`,'id='+body,{headers:headers});
  }

  public setComment(comentario:string, offertId:number):Observable<any>{
    let body = JSON.stringify({'comentario':comentario,'id_oferta':offertId});
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/newCommentSolicitude?token=${localStorage.getItem('token')}`,'comentario='+body,{headers:headers});
  }
}
