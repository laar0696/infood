import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { AllUsersSchema } from '../schemas/allUsersSchema';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private apiUrl:string = environment.apiUrl;

  handleError(error: HttpErrorResponse) {
    console.log('Error obteniendo los datos');
    return Observable.throw(error.message || 'Server Error');
  }

  constructor(private http: HttpClient, private token: TokenService) { }

  getAllUsers(): Observable< AllUsersSchema[] > {
    return this.http.get< AllUsersSchema[] >(`${this.apiUrl}/getAllUsers?token=${this.token.get()}`)
      .catch(this.handleError);
  }

  changeStatus(userId) {
    return this.http.post(`${this.apiUrl}/changeStatus?token=${localStorage.getItem('token')}`, {id:userId})
    .catch(this.handleError);
  }

  deleteUser(userId) {
    return this.http.post(`${this.apiUrl}/deleteUser?token=${localStorage.getItem('token')}`, {id:userId})
    .catch(this.handleError);
  }

  searchUser(query2search): Observable< AllUsersSchema[] > {
    return this.http.post< AllUsersSchema[] >(`${this.apiUrl}/searchUser?token=${this.token.get()}`, {q:query2search})
    .catch(this.handleError);
  }
}
