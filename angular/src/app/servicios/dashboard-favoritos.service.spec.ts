import { TestBed, inject } from '@angular/core/testing';

import { DashboardFavoritosService } from './dashboard-favoritos.service';

describe('DashboardFavoritosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DashboardFavoritosService]
    });
  });

  it('should be created', inject([DashboardFavoritosService], (service: DashboardFavoritosService) => {
    expect(service).toBeTruthy();
  }));
});
