import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import * as _ from 'lodash';
import { OfertaSchema } from '../schemas/ofertaSchema';
import { SolicitudSchema } from '../schemas/solicitudSchema';
import { ProductoSchema } from '../schemas/productoSchema';
import { EstadoSchema } from '../schemas/estadoSchema';
import { CertificacionSchema } from '../schemas/certificacionSchema';
import { CategoriaSchema } from '../schemas/categoriaSchema';
import { ResponseSchema } from '../schemas/responseSchema';
import { TokenService } from './token.service';
import { Router } from '@angular/router';
import { CompraSchema } from '../schemas/compraSchema';
import { ComentarioSchema } from '../schemas/comentarioSchema';
import { PaginatorSchema } from '../schemas/paginatorSchema';

@Injectable({
  providedIn: 'root'
})
export class MarketService {

  private apiUrl:string = environment.apiUrl;


  constructor( private http:HttpClient, private token:TokenService, public router:Router) { }

  public getOfertas(filtros) :Observable<ResponseSchema<OfertaSchema>>{
    return this.http.post<ResponseSchema<OfertaSchema>>(`${this.apiUrl}/marketPlace/getOfertas?token=${this.token.get()}`, filtros)
    .catch( (e:any) => Observable.throw(this.error(e) ) );
  }

  public getSolicitudes(filtros) :Observable<ResponseSchema<SolicitudSchema>>{
    return this.http.post<ResponseSchema<SolicitudSchema>>(`${this.apiUrl}/marketPlace/getSolicitudes?token=${this.token.get()}`, filtros)
    .catch( (e:any) => Observable.throw(this.error(e) ) );
  }


  public getProductos(categoria): Observable<ProductoSchema[]>{ 
    return this.http.post<ProductoSchema[]>(`${this.apiUrl}/marketPlace/getProductos`, categoria);
  }

  public getEstados(): Observable<EstadoSchema[]>{
    return this.http.get<EstadoSchema[]>(`${this.apiUrl}/marketPlace/getEstados`);
  }

  public getCertificaciones(): Observable<CertificacionSchema[]> {
    return this.http.get<CertificacionSchema[]>(`${this.apiUrl}/marketPlace/getCertificaciones`);
  }

  public getCategorias(): Observable<CategoriaSchema[]>{
    return this.http.get<CategoriaSchema[]>(`${this.apiUrl}/marketPlace/getCategorias`);
  }

  public getOfertasHome(): Observable<OfertaSchema[]>{
    return this.http.get<OfertaSchema[]>(`${this.apiUrl}/marketPlace/getOfertasHome`);
  }

  public getSolicitudesHome(): Observable<SolicitudSchema[]>{
    return this.http.get<SolicitudSchema[]>(`${this.apiUrl}/marketPlace/getSolicitudesHome?token=${this.token.get()}`)
    .catch( (e:any) => Observable.throw(this.error(e) ) );
  }

  public addProductoFavorito( favorito ){
    return this.http.post(`${this.apiUrl}/marketPlace/addProductoFavorito?token=${this.token.get()}`, favorito )
          .catch( (e:any) => Observable.throw( this.error(e)) );
  }
  
  public getOferta( oferta_id ): Observable<OfertaSchema>{
    let data = { id: oferta_id };
    return this.http.post<OfertaSchema>(`${this.apiUrl}/marketPlace/getOferta`, data);
  }

  public getOfertasSimilares( producto_id:number, page:number, size:number ) : Observable<PaginatorSchema<OfertaSchema>>{
    let data = { 'producto_id' : producto_id, 'size': size };
    return this.http.post<PaginatorSchema<OfertaSchema>>(`${this.apiUrl}/marketPlace/getOfertasSimilares?token=${this.token.get()}&page=${page}`, data);
  }


  public getSolicitud( solicitud_id ): Observable<SolicitudSchema>{
    let data = { id: solicitud_id };
    return this.http.post<SolicitudSchema>(`${this.apiUrl}/marketPlace/getSolicitud`, data);
  }

  public getSolicitudesSimilares( producto_id:number, page:number, size:number ) : Observable<PaginatorSchema<SolicitudSchema>>{
    let data = { 'producto_id' : producto_id, 'size':size };
    return this.http.post<PaginatorSchema<SolicitudSchema>>(`${this.apiUrl}/marketPlace/getSolicitudesSimilares?page=${page}`, data);
  }

  public compraOferta(oferta_id, unidades): Observable<ResponseSchema<any>>{
    let data = { 'oferta_id': oferta_id, 'unidades': unidades };
    return this.http.post<ResponseSchema<any>>(`${this.apiUrl}/marketPlace/compraOferta?token=${this.token.get()}`,data)
    .catch( (e:any) => Observable.throw(this.error(e) ) );
  }

  public compraSolicitud(solicitud_id, unidades): Observable<ResponseSchema<any>>{
    let data = { 'solicitud_id': solicitud_id, 'unidades': unidades };
    return this.http.post<ResponseSchema<any>>(`${this.apiUrl}/marketPlace/compraSolicitud?token=${this.token.get()}`, data)
    .catch( (e:any) => Observable.throw(this.error(e) ) );
  }

  public getCompras(index:number, page_size:number): Observable<ResponseSchema<OfertaSchema>>{
    let data = { 'size': page_size, 'page' : index };
    return this.http.post<ResponseSchema<OfertaSchema>>(`${this.apiUrl}/marketPlace/getCompras?token=${this.token.get()}`, data)
    .catch( (e:any) => Observable.throw(this.error(e) ) );
  }

  public getCompraOferta( id ): Observable<ResponseSchema<CompraSchema>>{
    let data = {'id': id};
    return this.http.post<ResponseSchema<CompraSchema>>(`${this.apiUrl}/marketPlace/getCompraOferta?token=${this.token.get()}`, data)
    .catch( (e:any) => Observable.throw(this.error(e) ) );
  }

  public getCompraSolicitud( id ): Observable<ResponseSchema<CompraSchema>>{
    let data = {'id': id};
    return this.http.post<ResponseSchema<CompraSchema>>(`${this.apiUrl}/marketPlace/getCompraSolicitud?token=${this.token.get()}`, data)
    .catch( (e:any) => Observable.throw(this.error(e) ) );
  }

  public addComentarioCompraOferta( id, comentario ): Observable<ResponseSchema<ComentarioSchema>>{
    let data = { 'id': id, 'comentario':comentario };
    return this.http.post<ResponseSchema<ComentarioSchema>>(`${this.apiUrl}/marketPlace/addComentarioCompraOferta?token=${this.token.get()}`, data)
    .catch( (e:any) => Observable.throw(this.error(e) ) );
  }
  
  public addComentarioCompraSolicitud( id, comentario ): Observable<ResponseSchema<ComentarioSchema>>{
    let data = { 'id': id, 'comentario':comentario };
    return this.http.post<ResponseSchema<ComentarioSchema>>(`${this.apiUrl}/marketPlace/addComentarioCompraSolicitud?token=${this.token.get()}`, data)
    .catch( (e:any) => Observable.throw(this.error(e) ) );
  }

  public addCalificacionCompra( compra_id ,puntos, comentario ) : Observable<ResponseSchema<any>>{
    let data = { 'compra_id': compra_id, 'puntos': puntos, 'comentario':comentario  };
    return this.http.post<ResponseSchema<any>>(`${this.apiUrl}/marketPlace/addCalificacionCompra?token=${this.token.get()}`,data)
      .catch( (e:any) => Observable.throw(this.error(e) ) );
  }

  public eliminaCompra( compra_id: number, tipo_compra: string ): Observable<ResponseSchema<any>>{
    let data = { 'compra_id': compra_id, 'tipo_compra': tipo_compra  };
    return this.http.post<ResponseSchema<any>>(`${this.apiUrl}/marketPlace/eliminaCompra?token=${this.token.get()}`, data);
  }

  public getCalificaciones( oferta_id ) : Observable<ResponseSchema<any>>{
    let data = { 'oferta_id': oferta_id };
    return this.http.post<ResponseSchema<any>>(`${this.apiUrl}/marketPlace/getCalificaciones?token=${this.token.get()}`,data)
      .catch( (e:any) => Observable.throw(this.error(e) ) );
  }
  
  public getOfertasAdmin(page:number, size:number): Observable<ResponseSchema<any>>{
    let data = { 'size': size };
    return this.http.post<ResponseSchema<any>>(`${this.apiUrl}/marketPlace/getOfertasAdmin?token=${this.token.get()}&page=${page}`,data)
      .catch( (e:any) => Observable.throw(this.error(e)) );
  }

  public getPedidosAdmin(page:number, size:number): Observable<ResponseSchema<any>>{
    let data = { 'size': size };
    return this.http.post<ResponseSchema<any>>(`${this.apiUrl}/marketPlace/getPedidosAdmin?token=${this.token.get()}&page=${page}`,data)
      .catch( (e:any) => Observable.throw(this.error(e)) );
  }


  public setEstatusCompraSolicitud( id:number, estatus:boolean ): Observable<ResponseSchema<any>>{
    let data = { 'id': id, 'completado': estatus };
    return this.http.post<ResponseSchema<any>>(`${this.apiUrl}/marketPlace/setEstatusCompraSolicitud?token=${this.token.get()}`,data)
      .catch( (e:any) => Observable.throw(this.error(e)) );
  }

  public setEstatusCompraOferta(id:number, estatus:boolean): Observable<ResponseSchema<any>>{
    let data = { 'id':id, 'completado': estatus };
    return this.http.post<ResponseSchema<any>>(`${this.apiUrl}/marketPlace/setEstatusCompraOferta?token=${this.token.get()}`,data)
      .catch( (e:any) => Observable.throw(this.error(e)) );
  }


  public error(error: Error | HttpErrorResponse){
    
    if( error instanceof HttpErrorResponse ){
      switch( error.status ){
        case 401 :  this.router.navigate(['login']);
                    return 'Unauthorized';
        case 403 :  this.router.navigate(['dashboard']);
                    return 'Unauthorized';
        default  :  return 'Error desconocido'
      }      
    }
    return error.message; 
  }

}
 