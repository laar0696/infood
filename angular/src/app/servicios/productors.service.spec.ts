import { TestBed, inject } from '@angular/core/testing';

import { ProductorsService } from './productors.service';

describe('ProductorsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductorsService]
    });
  });

  it('should be created', inject([ProductorsService], (service: ProductorsService) => {
    expect(service).toBeTruthy();
  }));
});
