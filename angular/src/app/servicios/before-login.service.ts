import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { TokenService } from './token.service';
import { AuthService } from './auth.service';

@Injectable()
export class BeforeLoginService implements CanActivate {

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {

    this.auth.check().subscribe(data => {
      this.router.navigate(['dashboard']);
    });

    return !this.token.loggedIn();
  }


  
  constructor( private router: Router, private auth:AuthService, private token: TokenService) { }

}
