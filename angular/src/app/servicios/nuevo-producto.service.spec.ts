import { TestBed, inject } from '@angular/core/testing';

import { NuevoProductoService } from './nuevo-producto.service';

describe('NuevoProductoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NuevoProductoService]
    });
  });

  it('should be created', inject([NuevoProductoService], (service: NuevoProductoService) => {
    expect(service).toBeTruthy();
  }));
});
