import { TestBed, inject } from '@angular/core/testing';

import { DashboardPersonalService } from './dashboard-personal.service';

describe('DashboardPersonalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DashboardPersonalService]
    });
  });

  it('should be created', inject([DashboardPersonalService], (service: DashboardPersonalService) => {
    expect(service).toBeTruthy();
  }));
});
