import { Injectable } from '@angular/core';
// import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { CategorySchema } from '../schemas/categoryschema';
import { ProductorSchema } from '../schemas/productorschema';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { StateSchema } from '../schemas/stateschema';
import { PaginatorSchema } from '../schemas/paginatorSchema';
import { ProductSchema } from '../schemas/productSchema';
import { ProductorDetailSchema } from '../schemas/productorDetailSchema';

@Injectable({
  providedIn: 'root'
})
export class ProductorsService {
  
  private apiUrl:string = environment.apiUrl;

  handleError(error: HttpErrorResponse) {
    return Observable.throw(error.message || 'Server Error');
  }

    constructor (private http: HttpClient) {}

    // public getProductors(){
    //   return this.http.get(this.apiUrl+'/getProductors/');
    // }
    
    getProductors(): Observable<ProductorSchema[]> {
      return this.http.get<ProductorSchema[]>(this.apiUrl+'/getProductors')
        .catch(this.handleError);
    }

    getProductor(id): Observable< ProductorDetailSchema<ProductSchema[]> > {
      return this.http.get< ProductorDetailSchema<ProductSchema[]> >(this.apiUrl+'/getProductor/'+id)
      .catch(this.handleError);
    }

    getPaginator(page, category, state, pageSize): Observable< PaginatorSchema<ProductorSchema> > {
      return this.http.post< PaginatorSchema<ProductorSchema> >(this.apiUrl+'/getPaginator', {page,category,state,pageSize})
        .catch(this.handleError);
    }

    getCategories(): Observable<CategorySchema[]> {
      return this.http.get<CategorySchema[]>(this.apiUrl+'/getCategories')
        .catch(this.handleError);
    }

    getStates(): Observable<StateSchema[]> {
      return this.http.get<StateSchema[]>(this.apiUrl+'/getStates')
        .catch(this.handleError);
    }

}
