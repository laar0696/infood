import { Injectable } from '@angular/core';
import { Userschema } from '../schemas/userschema';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardPersonalService {

  apiUrl:String = environment.apiUrl;

  handleError(error: HttpErrorResponse) {
    console.log('Error obteniendo los datos');
    return Observable.throw(error.message || 'Server Error');
  }

  constructor(private http: HttpClient) { }

  public updatePersonalData(user:Userschema): Observable<any>
  {
    let body = JSON.stringify(user);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/dataUpdate?token=${localStorage.getItem('token')}`,'user='+body,{headers:headers});
  }

  public updateUserData(user: Userschema, userId:number): Observable<any> {

    let body = {
      user: user,
      userId: userId
    };
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/userDataUpdate?token=${localStorage.getItem('token')}`,'user='+JSON.stringify(body)+'',{headers:headers});
  }

  public getPersonalData(user_id:number):Observable<any>{
    sessionStorage.removeItem('userData');
    sessionStorage.removeItem('userType');
    sessionStorage.removeItem('userId');
    sessionStorage.removeItem('countries');

    sessionStorage.setItem('ajeno', '0');

    return this.http.get(`${this.apiUrl}/getUserData?token=${localStorage.getItem('token')}`);
  }

  public getUserData(userId:number):Observable<any> {
    sessionStorage.removeItem('userData');
    // sessionStorage.removeItem('userType');
    // sessionStorage.removeItem('userId');
    sessionStorage.removeItem('countries');

    sessionStorage.setItem('ajeno', '1');

    return this.http.get(`${this.apiUrl}/getUserInfo/${userId}?token=${localStorage.getItem('token')}`);
  }

  public retrieveUserApplications():Observable<any>{
    return this.http.get(this.apiUrl+`/getUserApplications?token=${localStorage.getItem('token')}`);
  }

  public updatePassword(pass):Observable<any> {
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(this.apiUrl+`/updatePassword?token=${localStorage.getItem('token')}`,'newPass='+pass, {headers: headers});
  }

  public retrieveUserSolicitutes():Observable<any>{
    return this.http.get(this.apiUrl+`/getUserSolicitutes?token=${localStorage.getItem('token')}`);
  }

  public retrieveAllUserApplications():Observable<any>{
    return this.http.get(this.apiUrl+`/getAllAplications?token=${localStorage.getItem('token')}`);
  }

  public retrieveAllUsersSolicitude():Observable<any>{
    return this.http.get(this.apiUrl+`/getAllSolicitutes?token=${localStorage.getItem('token')}`);
  }

  changeStatus(userId) {
    return this.http.post(`${this.apiUrl}/changeStatus?token=${localStorage.getItem('token')}`, {id:userId})
    .catch(this.handleError);
  }
  
  changeStatusAdmin(userId) {
    return this.http.post(`${this.apiUrl}/changeStatusAdmin?token=${localStorage.getItem('token')}`, {id:userId})
    .catch(this.handleError);
  }
}
