import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { CertificacionSchema } from '../schemas/certificacionSchema';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CertificateService {

  constructor(private http: HttpClient) { }

  apiUrl = environment.apiUrl;

  public createCertificate(certificate:CertificacionSchema):Observable<any>{
    sessionStorage.removeItem('certificates');
    let body = JSON.stringify(certificate);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/createCertificate?token=${localStorage.getItem('token')}`,'certificate='+body,{headers:headers});
  }

}
