import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class AdminService implements CanActivate {

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean>{

    this.auth.admin().subscribe(
      data  => { 
        if( !data ){ 
          this.router.navigate(['dashboard']);
          localStorage.removeItem('administrador');
        } 
      }      
    );

    return this.auth.esAdmin();

  }

  constructor(private auth:AuthService, private router:Router, private token:TokenService) { }
}
