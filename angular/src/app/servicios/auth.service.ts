import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { ResponseSchema } from '../schemas/responseSchema';
import { TokenService } from './token.service';
@Injectable()
export class AuthService {
  private loggedIn = new BehaviorSubject<boolean>(this.Token.loggedIn());
  authStatus = this.loggedIn.asObservable();

  changeAuthStatus(value: boolean) {
    this.loggedIn.next(value);
  }

  private apiUrl = environment.apiUrl;

  constructor(private Token: TokenService, private http: HttpClient, private router: Router) {}

  check(): Observable<boolean>{
    return this.http.get<boolean>(`${this.apiUrl}/check?token=${localStorage.getItem('token')}`)
      .catch( (e:any) => Observable.throw(this.error(e)) );
  }

  checkNav(): Observable<boolean>{
    return this.http.get<boolean>(`${this.apiUrl}/check?token=${localStorage.getItem('token')}`);
  }

  admin(): Observable<boolean>{
    return this.http.get<boolean>(`${this.apiUrl}/admin?token=${localStorage.getItem('token')}`)
      .catch( (e:any) => Observable.throw(this.error(e) ) );
  }


  administrador(admin){
    localStorage.setItem('administrador', admin );
  }

  esAdmin():boolean{
    return localStorage.getItem('administrador') == '1';
  }

  refresh(){
    this.http.post<any>(`${this.apiUrl}/refresh?token=${localStorage.getItem('token')}`,null).subscribe(data => {
      this.Token.handle(data.access_token);
      this.administrador(data.admin);
      this.changeAuthStatus(true);
    });
  }

  logout(){
    this.http.post<ResponseSchema<any>>(`${this.apiUrl}/logout?token=${localStorage.getItem('token')}`, null)
      .catch( (e:any) => Observable.throw(this.error(e)))
      .subscribe( 
        () => {
          localStorage.clear();
          sessionStorage.clear();
          this.router.navigate(['login']);
        }
      );
  }

  public error(error: Error | HttpErrorResponse){
    if( error instanceof HttpErrorResponse ){
      switch( error.status ){
        case 401 :  this.router.navigate(['login']);
                    localStorage.clear();
                    sessionStorage.clear();
                    return 'Unauthorized';
        case 403 :  this.router.navigate(['dashboard']);
                    return 'Unauthorized';
        case   0 :  this.router.navigate(['login']);
        default  :  return error.status
      }      
    }
    return error.message; 
  }



}
