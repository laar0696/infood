import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VenderService {
  apiUrl:String = environment.apiUrl;

  constructor(private http: HttpClient){ }

  public deleteApplication(id:number):Observable<any> {
    let body = JSON.stringify(id);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/deleteOfert?token=${localStorage.getItem('token')}`,'id='+body,{headers:headers});
  }

  public copyApplication(id:number):Observable<any> {
    let body = JSON.stringify(id);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/copyOfert?token=${localStorage.getItem('token')}`,'id='+body,{headers:headers});
  }

  public copySolicitude(id:number):Observable<any>{
    let body = JSON.stringify(id);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/copyUserSolicitute?token=${localStorage.getItem('token')}`,'id='+body,{headers:headers});
  }

  public deleteSolicitude(id:number):Observable<any> {
    let body = JSON.stringify(id);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/deleteSolicitute?token=${localStorage.getItem('token')}`,'id='+body,{headers:headers});
  }

  public deleteApplicationAdministrative(id:number){
    let body = JSON.stringify(id);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/deleteAdminAplications?token=${localStorage.getItem('token')}`,'id='+body,{headers:headers});
  }
  
  public deleteSolicitudeAdministrative(id:number){
    let body = JSON.stringify(id);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/deleteAdminSolicitudes?token=${localStorage.getItem('token')}`,'id='+body,{headers:headers});
  }

}
