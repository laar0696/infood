import { TestBed, inject } from '@angular/core/testing';

import { DetalleProductoService } from './detalle-producto.service';

describe('DetalleProductoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DetalleProductoService]
    });
  });

  it('should be created', inject([DetalleProductoService], (service: DetalleProductoService) => {
    expect(service).toBeTruthy();
  }));
});
