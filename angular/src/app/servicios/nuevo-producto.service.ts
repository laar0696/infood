import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { VenderSchema } from '../schemas/vender-schema';

@Injectable({
  providedIn: 'root'
})
export class NuevoProductoService {

  constructor(private http: HttpClient) { }

  apiUrl = environment.apiUrl;

  public getBaseData(): Observable<any>{
    return this.http.get(`${this.apiUrl}/setDataInformation?token=${localStorage.getItem('token')}?`);
  }

  public setDataApplication(application:VenderSchema):Observable<any>{
    let body = JSON.stringify(application);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/saveAplication?token=${localStorage.getItem('token')}`,'offert='+body,{headers:headers});
  }

  public getEditApplicationData(id:number):Observable<any> {
    let body = JSON.stringify(id);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post<any>(`${this.apiUrl}/setDataEdit?token=${localStorage.getItem('token')}`,'id='+body,{headers:headers});
  }

  public setEditApplicationData(application:VenderSchema):Observable<any> {
    let body = JSON.stringify(application);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/saveChangesAplication?token=${localStorage.getItem('token')}`,'offert='+body,{headers:headers});
  }

  public updateApplicationData(application:VenderSchema):Observable<any>{
    let body = JSON.stringify(application);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/setDataUpdate?token=${localStorage.getItem('token')}`,'offert='+body,{headers:headers});
  }

  public eraseImageSolicitute(id:number){
    let body = JSON.stringify(id);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/eraseImageApplication?token=${localStorage.getItem('token')}`,'id='+body,{headers:headers});
  }

}
