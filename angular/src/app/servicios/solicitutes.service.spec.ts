import { TestBed, inject } from '@angular/core/testing';

import { SolicitutesService } from './solicitutes.service';

describe('SolicitutesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SolicitutesService]
    });
  });

  it('should be created', inject([SolicitutesService], (service: SolicitutesService) => {
    expect(service).toBeTruthy();
  }));
});
