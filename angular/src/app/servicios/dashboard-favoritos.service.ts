import { Injectable } from '@angular/core';
import { ProductSchema } from '../schemas/productSchema';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardFavoritosService {

  private apiUrl:string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    return Observable.throw(error.message || 'Server Error');
  }

  getFavoriteProducts(id, page_index, page_size): Observable<any> {
    let data = { 
      page_index: page_index,
      page_size: page_size
     };

    return this.http.post<ProductSchema[]>(`${this.apiUrl}/getFavoriteProducts?token=${localStorage.getItem('token')}&page=${page_size}`, data)
      .catch(this.handleError);
  }

  deleteFavoriteProduct(id): Observable<ProductSchema[]> {
    return this.http.get<ProductSchema[]>(`${this.apiUrl}/deleteFavoriteProduct/${id}?token=${localStorage.getItem('token')}`)
    .catch(this.handleError);
  }

}
