import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdministradorProductosSolicitudesService {

  constructor(private http:HttpClient) { }

  apiUrl = environment.apiUrl;

  public changeStatus(id:number):Observable <any>{
    let body = JSON.stringify(id);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/statusApplication?token=${localStorage.getItem('token')}`,'id='+body,{headers:headers});
  }

  public changeStatusSolicitude(id:number):Observable <any>{
    let body = JSON.stringify(id);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/statusSolicitude?token=${localStorage.getItem('token')}`,'id='+body,{headers:headers});
  }
}
