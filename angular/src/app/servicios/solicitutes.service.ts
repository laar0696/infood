import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SoliciteSchema } from '../schemas/solicitud-schema';

@Injectable({
  providedIn: 'root'
})
export class SolicitutesService {

  apiUrl = environment.apiUrl;

  constructor(private http:HttpClient) { }

  public getBaseData(): Observable<any>{
    return this.http.get(`${this.apiUrl}/setDataInformation?token=${localStorage.getItem('token')}?`);
  }

  /* Guarda */ 
  public setDataApplication(application:SoliciteSchema):Observable<any>{
    let body = JSON.stringify(application);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/createAplication?token=${localStorage.getItem('token')}`,'solicitude='+body,{headers:headers});
  }

  /* Trae informacion */ 
  public getEditApplicationData(id:number):Observable<any> {
    let body = JSON.stringify(id);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post<any>(`${this.apiUrl}/getSolicitudeInformation?token=${localStorage.getItem('token')}`,'id='+body,{headers:headers});
  }

  /* Guarda cambios al editar  */
  public updateApplicationData(application:SoliciteSchema):Observable<any>{
    let body = JSON.stringify(application);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/setSolicitudeInformation?token=${localStorage.getItem('token')}`,'solicitud='+body,{headers:headers});
  }

  public eraseImageSolicitute(id:number){
    let body = JSON.stringify(id);
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.apiUrl}/eraseImageSolicitude?token=${localStorage.getItem('token')}`,'id='+body,{headers:headers});
  }

}
