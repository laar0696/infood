-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 16-10-2018 a las 11:38:29
-- Versión del servidor: 5.6.39-cll-lve
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `INFOOD_market_place`
--
CREATE DATABASE IF NOT EXISTS `INFOOD_market_place` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `INFOOD_market_place`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_100000_create_password_resets_table', 1),
(9, '2018_07_03_182753_create_region', 2),
(234, '2014_10_12_000000_create_users_table', 3),
(235, '2018_07_03_180246_create_tipo_usuario', 3),
(236, '2018_07_03_180907_create_calificaciones', 3),
(237, '2018_07_03_181539_create_paises', 3),
(238, '2018_07_03_182436_create_estado', 3),
(239, '2018_07_03_184046_create_categoria', 3),
(240, '2018_07_06_161548_create_productos', 3),
(241, '2018_07_10_163351_create_solicitudes', 3),
(242, '2018_07_10_163418_create_solicitudes_venta', 3),
(243, '2018_07_10_163443_create_solicitudes_conpra', 3),
(244, '2018_07_10_170949_create_unidad', 3),
(245, '2018_07_10_171014_create_presentaciones', 3),
(246, '2018_07_10_171102_create_imagenes_solicitud', 3),
(247, '2018_07_10_171125_create_certificaciones_solicitud', 3),
(248, '2018_07_10_173012_create_favoritos', 3),
(249, '2018_07_11_154246_roll_back_user_solicitud', 4),
(250, '2018_07_11_154736_create_ventas', 4),
(251, '2018_07_11_164809_create_imagenes_venta', 5),
(252, '2018_07_11_180620_create_ofertas', 6),
(253, '2018_07_11_184013_create_certificaciones', 7),
(254, '2018_07_11_184514_create_certificaciones_oferta', 8),
(255, '2018_07_11_184527_create_certificaciones_solicitud', 8),
(256, '2018_08_02_193111_create_imagenes_certificacions_table', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `id` int(10) UNSIGNED NOT NULL,
  `wpuser_id` int(11) NOT NULL,
  `perfil_avatar` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_alt` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empresa_avatar` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `razon_social` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rfc` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_empresa` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado_id` int(11) NOT NULL,
  `nombre_comercial` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semblanza` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_usuario_id` int(11) NOT NULL,
  `fecha_baja` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `administrador` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_calificaciones`
--

DROP TABLE IF EXISTS `tbl_calificaciones`;
CREATE TABLE `tbl_calificaciones` (
  `id` int(10) UNSIGNED NOT NULL,
  `calificacion` int(11) NOT NULL,
  `usuario_califica_id` int(11) NOT NULL,
  `usuario_calificado_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `comentario` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_categorias`
--

DROP TABLE IF EXISTS `tbl_categorias`;
CREATE TABLE `tbl_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_categorias`
--

INSERT INTO `tbl_categorias` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Fresco', NULL, NULL),
(2, 'Procesado', NULL, NULL),
(3, 'Granos', '2018-07-17 22:06:37', '2018-07-17 22:06:37'),
(4, 'Pecuario', NULL, NULL),
(5, 'Otros', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_certificaciones`
--

DROP TABLE IF EXISTS `tbl_certificaciones`;
CREATE TABLE `tbl_certificaciones` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_certificaciones`
--

INSERT INTO `tbl_certificaciones` (`id`, `nombre`, `imagen`, `created_at`, `updated_at`) VALUES
(1, 'Orgánico Sagarpa México', 'http://infood.com.mx/wp-content/uploads/2018/09/organicosagarpa.png', NULL, NULL),
(2, 'Certificación Tipo Inspección Federal', 'http://infood.com.mx/wp-content/uploads/2018/09/default_certificacion.png', NULL, NULL),
(3, 'Certificación de buenas prácticas pecuarias', 'http://infood.com.mx/wp-content/uploads/2018/09/default_certificacion.png', NULL, NULL),
(15, 'México Calidad Suprema', 'http://infood.com.mx/wp-content/uploads/2018/09/default_certificacion.png', '2018-07-30 16:11:10', '2018-07-30 16:11:10'),
(16, 'Inocuidad Alimentaria FSSC 22000', 'http://infood.com.mx/wp-content/uploads/2018/09/default_certificacion.png', '2018-08-09 18:34:10', '2018-08-09 18:34:10'),
(18, 'Hazard Analysis Critical Crontol Point', 'http://infood.com.mx/wp-content/uploads/2018/09/default_certificacion.png', '2018-08-09 18:38:12', '2018-08-09 18:38:12'),
(19, 'ISO 22000:2005', 'http://infood.com.mx/wp-content/uploads/2018/09/default_certificacion.png', '2018-08-09 19:40:20', '2018-08-09 19:40:20'),
(20, 'NFS International Safety Certification for Food, Water and Consumer Goods', 'http://infood.com.mx/wp-content/uploads/2018/09/default_certificacion.png', '2018-08-14 19:31:19', '2018-08-14 19:31:19'),
(23, 'USDA', 'http://infood.com.mx/wp-content/uploads/2018/09/default_certificacion.png', '2018-08-15 21:35:24', '2018-08-15 21:35:24'),
(24, 'Bienestar Animal', 'http://infood.com.mx/wp-content/uploads/2018/09/default_certificacion.png', '2018-08-15 21:35:24', '2018-08-15 21:35:24'),
(25, 'Halal', 'http://infood.com.mx/wp-content/uploads/2018/09/default_certificacion.png', '2018-08-15 21:35:24', '2018-08-15 21:35:24'),
(26, 'Empresa Socialmente Responsable', 'http://infood.com.mx/wp-content/uploads/2018/09/default_certificacion.png', '2018-08-15 21:35:24', '2018-08-15 21:35:24'),
(27, 'Global GAP', 'http://infood.com.mx/wp-content/uploads/2018/09/default_certificacion.png', '2018-08-15 21:35:24', '2018-08-15 21:35:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_certificaciones_oferta`
--

DROP TABLE IF EXISTS `tbl_certificaciones_oferta`;
CREATE TABLE `tbl_certificaciones_oferta` (
  `id` int(10) UNSIGNED NOT NULL,
  `oferta_id` int(11) NOT NULL,
  `certificacion_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_certificaciones_oferta`
--

INSERT INTO `tbl_certificaciones_oferta` (`id`, `oferta_id`, `certificacion_id`, `created_at`, `updated_at`) VALUES
(143, 141, 1, '2018-10-03 18:14:18', '2018-10-03 18:14:18'),
(154, 142, 1, '2018-10-10 17:51:33', '2018-10-10 17:51:33'),
(159, 166, 1, '2018-10-10 17:55:33', '2018-10-10 17:55:33'),
(186, 168, 1, '2018-10-10 21:51:30', '2018-10-10 21:51:30'),
(187, 168, 3, '2018-10-10 21:51:30', '2018-10-10 21:51:30'),
(188, 168, 26, '2018-10-10 21:51:30', '2018-10-10 21:51:30'),
(189, 168, 24, '2018-10-10 21:51:30', '2018-10-10 21:51:30'),
(190, 168, 23, '2018-10-10 21:51:30', '2018-10-10 21:51:30'),
(197, 145, 1, '2018-10-12 21:58:50', '2018-10-12 21:58:50'),
(199, 170, 3, '2018-10-16 13:47:08', '2018-10-16 13:47:08'),
(200, 170, 1, '2018-10-16 13:47:08', '2018-10-16 13:47:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_certificaciones_solicitud`
--

DROP TABLE IF EXISTS `tbl_certificaciones_solicitud`;
CREATE TABLE `tbl_certificaciones_solicitud` (
  `id` int(10) UNSIGNED NOT NULL,
  `solicitud_id` int(11) NOT NULL,
  `certificacion_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_certificaciones_solicitud`
--

INSERT INTO `tbl_certificaciones_solicitud` (`id`, `solicitud_id`, `certificacion_id`, `created_at`, `updated_at`) VALUES
(58, 64, 1, '2018-10-03 21:16:53', '2018-10-03 21:16:53'),
(65, 73, 1, '2018-10-08 19:19:27', '2018-10-08 19:19:27'),
(90, 74, 1, '2018-10-10 21:01:21', '2018-10-10 21:01:21'),
(91, 74, 1, '2018-10-10 21:01:21', '2018-10-10 21:01:21'),
(94, 71, 1, '2018-10-10 21:16:32', '2018-10-10 21:16:32'),
(95, 71, 1, '2018-10-10 21:16:32', '2018-10-10 21:16:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_comentarios_compra_oferta`
--

DROP TABLE IF EXISTS `tbl_comentarios_compra_oferta`;
CREATE TABLE `tbl_comentarios_compra_oferta` (
  `id` int(11) NOT NULL,
  `compra_oferta_id` int(11) NOT NULL,
  `comentario` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_comentarios_compra_oferta`
--

INSERT INTO `tbl_comentarios_compra_oferta` (`id`, `compra_oferta_id`, `comentario`, `created_at`, `updated_at`, `usuario_id`) VALUES
(8, 30, 'Este es un comentario de prueba.', '2018-10-10 22:50:03', '2018-10-10 22:50:03', 38),
(9, 31, 'me interesa com nos ponemos de acuerdo', '2018-10-16 13:42:56', '2018-10-16 13:42:56', 32);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_comentarios_compra_solicitud`
--

DROP TABLE IF EXISTS `tbl_comentarios_compra_solicitud`;
CREATE TABLE `tbl_comentarios_compra_solicitud` (
  `id` int(11) NOT NULL,
  `compra_solicitud_id` int(11) NOT NULL,
  `comentario` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_comentarios_oferta`
--

DROP TABLE IF EXISTS `tbl_comentarios_oferta`;
CREATE TABLE `tbl_comentarios_oferta` (
  `id` int(11) NOT NULL,
  `oferta_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `comentario` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_comentarios_solicitud`
--

DROP TABLE IF EXISTS `tbl_comentarios_solicitud`;
CREATE TABLE `tbl_comentarios_solicitud` (
  `id` int(11) NOT NULL,
  `solicitud_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `comentario` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_comentarios_solicitud`
--

INSERT INTO `tbl_comentarios_solicitud` (`id`, `solicitud_id`, `usuario_id`, `comentario`, `created_at`, `updated_at`) VALUES
(7, 71, 1, 'Hola', '2018-10-08 19:44:32', '2018-10-08 19:44:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_compras_oferta`
--

DROP TABLE IF EXISTS `tbl_compras_oferta`;
CREATE TABLE `tbl_compras_oferta` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `oferta_id` int(11) NOT NULL,
  `unidades` int(11) NOT NULL,
  `completado` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_compras_oferta`
--

INSERT INTO `tbl_compras_oferta` (`id`, `usuario_id`, `oferta_id`, `unidades`, `completado`, `created_at`, `updated_at`) VALUES
(23, 32, 142, 1, 0, '2018-10-06 21:59:22', '2018-10-08 19:20:02'),
(29, 1, 142, 1, 0, '2018-10-08 19:46:27', '2018-10-08 19:46:27'),
(30, 38, 142, 1, 0, '2018-10-10 22:44:58', '2018-10-10 22:44:58'),
(31, 32, 142, 1, 0, '2018-10-16 13:42:07', '2018-10-16 13:42:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_compras_solicitud`
--

DROP TABLE IF EXISTS `tbl_compras_solicitud`;
CREATE TABLE `tbl_compras_solicitud` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `solicitud_id` int(11) NOT NULL,
  `unidades` int(11) NOT NULL,
  `completado` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_compras_solicitud`
--

INSERT INTO `tbl_compras_solicitud` (`id`, `usuario_id`, `solicitud_id`, `unidades`, `completado`, `created_at`, `updated_at`) VALUES
(7, 32, 70, 4, 0, '2018-10-07 22:25:46', '2018-10-07 22:25:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_estados`
--

DROP TABLE IF EXISTS `tbl_estados`;
CREATE TABLE `tbl_estados` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pais_id` int(11) NOT NULL,
  `latitud` double DEFAULT NULL,
  `longitud` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_estados`
--

INSERT INTO `tbl_estados` (`id`, `nombre`, `pais_id`, `latitud`, `longitud`, `created_at`, `updated_at`) VALUES
(1, 'Oaxaca', 1, 17.0542297, -96.71323039999999, NULL, NULL),
(2, 'Guanajuato', 1, 21.0190145, -101.25735859999998, '2018-07-12 00:00:00', NULL),
(3, 'Quiché', 2, 15.175358, -91.030835, NULL, NULL),
(4, 'Querétaro', 1, 20.5887932, -100.38988810000001, '2018-07-12 00:00:00', NULL),
(5, 'Aguascalientes', 1, 21.8852562, -102.29156769999997, '2018-07-12 00:00:00', NULL),
(6, 'Baja California', 1, 30.8406338, -115.28375849999998, '2018-07-12 00:00:00', NULL),
(7, 'Baja California Sur', 1, 26.0444446, -111.66607249999998, '2018-07-12 00:00:00', NULL),
(8, 'Campeche', 1, 18.931225, -90.26180679999999, '2018-07-12 00:00:00', NULL),
(9, 'Chiapas', 1, 16.7569318, -93.1292353, '2018-07-12 00:00:00', NULL),
(10, 'Chihuahua', 1, 28.6329957, -106.06910040000002, '2018-07-12 00:00:00', NULL),
(11, 'Ciudad de México (CDMX)', 1, 19.4326077, -99.13320799999997, '2018-07-12 00:00:00', NULL),
(12, 'Coahuila', 1, 27.058676, -101.7068294, '2018-07-12 00:00:00', NULL),
(13, 'Colima', 1, 19.1222634, -104.00723479999999, '2018-07-12 00:00:00', NULL),
(14, 'Durango', 1, 24.5592665, -104.6587821, '2018-07-12 00:00:00', NULL),
(15, 'Guerrero', 1, 17.4391926, -99.54509739999997, '2018-07-12 00:00:00', NULL),
(16, 'Hidalgo', 1, 20.0910963, -98.76238739999997, '2018-07-12 00:00:00', NULL),
(17, 'Jalisco', 1, 20.6595382, -103.34943759999999, '2018-07-12 00:00:00', NULL),
(18, 'Michoacán', 1, 19.5665192, -101.7068294, '2018-07-12 00:00:00', NULL),
(19, 'Distrito Federal', 1, 19.4326077, -99.13320799999997, '2018-07-12 00:00:00', NULL),
(20, 'Nayarit', 1, 21.7513844, -104.84546190000003, '2018-07-12 00:00:00', NULL),
(21, 'Morelos', 1, 18.6813049, -99.10134979999998, '2018-07-12 00:00:00', NULL),
(22, 'Nuevo León', 1, 25.592172, -99.99619469999999, '2018-07-12 00:00:00', NULL),
(23, 'Puebla', 1, 19.0414398, -98.2062727, '2018-07-12 00:00:00', NULL),
(24, 'Quintana Roo', 1, 19.1817393, -88.4791376, '2018-07-12 00:00:00', NULL),
(25, 'San Luis Potosí', 1, 22.1564699, -100.98554089999999, '2018-07-12 00:00:00', NULL),
(26, 'Sinaloa', 1, 25.1721091, -107.4795173, '2018-07-12 00:00:00', NULL),
(27, 'Sonora', 1, 29.2972247, -110.33088140000001, '2018-07-12 00:00:00', NULL),
(28, 'Tabasco', 1, 17.8409173, -92.6189273, '2018-07-12 00:00:00', NULL),
(29, 'Tamaulipas', 1, 24.26694, -98.8362755, '2018-07-12 00:00:00', NULL),
(30, 'Tlaxcala', 1, 19.318154, -98.2374954, '2018-07-12 00:00:00', NULL),
(31, 'Veracruz', 1, 19.173773, -96.13422409999998, '2018-07-12 00:00:00', NULL),
(32, 'Yucatán', 1, 20.7098786, -89.09433769999998, '2018-07-12 00:00:00', NULL),
(33, 'Zacatecas', 1, 22.7708555, -102.5832426, '2018-07-12 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_favoritos`
--

DROP TABLE IF EXISTS `tbl_favoritos`;
CREATE TABLE `tbl_favoritos` (
  `id` int(10) UNSIGNED NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `oferta_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_favoritos`
--

INSERT INTO `tbl_favoritos` (`id`, `usuario_id`, `oferta_id`, `created_at`, `updated_at`) VALUES
(43, 1, 142, '2018-10-04 00:02:00', '2018-10-04 00:02:00'),
(46, 32, 143, '2018-10-06 22:21:04', '2018-10-06 22:21:04'),
(49, 32, 142, '2018-10-16 13:41:21', '2018-10-16 13:41:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_imagenes_oferta`
--

DROP TABLE IF EXISTS `tbl_imagenes_oferta`;
CREATE TABLE `tbl_imagenes_oferta` (
  `id` int(10) UNSIGNED NOT NULL,
  `venta_id` int(11) NOT NULL,
  `imagen` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_imagenes_oferta`
--

INSERT INTO `tbl_imagenes_oferta` (`id`, `venta_id`, `imagen`, `created_at`, `updated_at`) VALUES
(168, 104, 'https://ecoosfera.com/wp-content/imagenes/cacahuates.jpg', '2018-08-30 11:35:29', '2018-08-30 11:35:29'),
(169, 105, 'https://static9.depositphotos.com/1001651/1115/i/950/depositphotos_11151777-stock-photo-glass-jar-with-canned-grapes.jpg', '2018-08-30 14:13:42', '2018-08-30 14:13:42'),
(170, 106, 'https://ecoosfera.com/wp-content/imagenes/cacahuates.jpg', '2018-08-30 14:20:58', '2018-08-30 14:20:58'),
(171, 107, 'https://static9.depositphotos.com/1001651/1115/i/950/depositphotos_11151777-stock-photo-glass-jar-with-canned-grapes.jpg', '2018-08-30 14:52:51', '2018-08-30 14:52:51'),
(172, 108, 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT_mvhX6JOMq9dyGjnP35K4y19wl20x3Wqinugp29_13Mj3C1TLiw', '2018-08-30 15:22:24', '2018-08-30 15:22:24'),
(174, 110, 'https://static9.depositphotos.com/1001651/1115/i/950/depositphotos_11151777-stock-photo-glass-jar-with-canned-grapes.jpg', '2018-08-30 15:51:15', '2018-08-30 15:51:15'),
(187, 4, 'http://www.telemama.es/wp-content/uploads/2015/12/IMG_7524.jpg', '2018-09-26 19:45:11', '2018-09-26 19:45:11'),
(202, 122, 'https://3.bp.blogspot.com/-8wgWc94T1hY/WbpEn3gBFrI/AAAAAAAAClg/JmuUYLzGTsIcADpKIs2xfRtKVrmBB5CngCLcBGAs/s1600/duraznos.jpg', '2018-10-02 18:36:51', '2018-10-02 18:36:51'),
(204, 131, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-02 18:39:01', '2018-10-02 18:39:01'),
(208, 135, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-03 17:51:13', '2018-10-03 17:51:13'),
(209, 136, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-03 17:51:44', '2018-10-03 17:51:44'),
(210, 137, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-03 17:51:48', '2018-10-03 17:51:48'),
(211, 138, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-03 17:51:49', '2018-10-03 17:51:49'),
(212, 139, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-03 17:51:49', '2018-10-03 17:51:49'),
(213, 140, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-03 17:51:49', '2018-10-03 17:51:49'),
(214, 141, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-03 18:14:18', '2018-10-03 18:14:18'),
(217, 142, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-03 21:13:31', '2018-10-03 21:13:31'),
(221, 146, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-06 21:05:28', '2018-10-06 21:05:28'),
(222, 161, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-06 21:25:18', '2018-10-06 21:25:18'),
(228, 166, 'http://infood.com.mx/marketPlace/public/storage/producto/venta/image_1539186730.jpg', '2018-10-10 17:55:33', '2018-10-10 17:55:33'),
(229, 166, 'http://infood.com.mx/marketPlace/public/storage/producto/venta/image_1539186776.jpg', '2018-10-10 17:55:33', '2018-10-10 17:55:33'),
(246, 168, 'http://infood.com.mx/marketPlace/public/storage/producto/venta/image_1539200989.jpg', '2018-10-10 21:49:49', '2018-10-10 21:49:49'),
(247, 168, 'http://infood.com.mx/marketPlace/public/storage/producto/venta/image_1539201090.jpg', '2018-10-10 21:51:30', '2018-10-10 21:51:30'),
(248, 168, 'http://infood.com.mx/marketPlace/public/storage/producto/venta/image_1539201090.jpg', '2018-10-10 21:51:30', '2018-10-10 21:51:30'),
(249, 168, 'http://infood.com.mx/marketPlace/public/storage/producto/venta/image_1539201090.jpg', '2018-10-10 21:51:30', '2018-10-10 21:51:30'),
(260, 145, 'http://infood.com.mx/marketPlace/public/storage/producto/venta/image_1539373593-3-1.jpg', '2018-10-12 21:46:33', '2018-10-12 21:46:33'),
(264, 167, 'http://infood.com.mx/marketPlace/public/storage/producto/venta/image_1539373931-0-1.jpg', '2018-10-12 21:52:11', '2018-10-12 21:52:11'),
(270, 170, 'http://infood.com.mx/wp-content/uploads/2018/09/mandarina.jpg', '2018-10-16 13:47:08', '2018-10-16 13:47:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_imagenes_solicitud`
--

DROP TABLE IF EXISTS `tbl_imagenes_solicitud`;
CREATE TABLE `tbl_imagenes_solicitud` (
  `id` int(10) UNSIGNED NOT NULL,
  `solicitud_id` int(11) NOT NULL,
  `imagen` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_imagenes_solicitud`
--

INSERT INTO `tbl_imagenes_solicitud` (`id`, `solicitud_id`, `imagen`, `created_at`, `updated_at`) VALUES
(71, 64, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-03 21:16:53', '2018-10-03 21:16:53'),
(72, 65, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-03 21:40:42', '2018-10-03 21:40:42'),
(74, 67, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-03 21:41:00', '2018-10-03 21:41:00'),
(75, 68, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-03 21:41:00', '2018-10-03 21:41:00'),
(79, 72, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-06 21:49:22', '2018-10-06 21:49:22'),
(80, 73, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', '2018-10-06 21:57:30', '2018-10-06 21:57:30'),
(92, 74, 'http://infood.com.mx/marketPlace/public/storage/producto/solicitud/image_1539198041.jpg', '2018-10-10 21:00:41', '2018-10-10 21:00:41'),
(96, 71, 'http://infood.com.mx/marketPlace/public/storage/producto/solicitud/image_1539198992.jpg', '2018-10-10 21:16:32', '2018-10-10 21:16:32'),
(111, 75, 'http://infood.com.mx/marketPlace/public/storage/producto/solicitud/image_1539211964-4-1.jpg', '2018-10-11 00:52:44', '2018-10-11 00:52:44'),
(112, 75, 'http://infood.com.mx/marketPlace/public/storage/producto/solicitud/image_1539374259-1-1.jpg', '2018-10-12 21:57:39', '2018-10-12 21:57:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_ofertas`
--

DROP TABLE IF EXISTS `tbl_ofertas`;
CREATE TABLE `tbl_ofertas` (
  `id` int(10) UNSIGNED NOT NULL,
  `variedad` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otras_caracteristicas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `volumen_total` double(10,2) DEFAULT NULL,
  `unidad_id` int(11) DEFAULT NULL,
  `precio_unidad` double(10,2) DEFAULT NULL,
  `presentacion_id` int(11) DEFAULT NULL,
  `tamano` double(10,2) DEFAULT NULL,
  `unidad_presentacion_id` int(11) DEFAULT NULL,
  `pedido_minimo` double(10,2) DEFAULT NULL,
  `unidad_pedido_minimo_id` int(11) DEFAULT NULL,
  `precio` enum('f','v') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disponible_desde` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `disponible_hasta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `estado_id` int(11) DEFAULT NULL,
  `ciudad` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `codigo_postal` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resena` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aprobado` tinyint(1) NOT NULL,
  `usuario_id` int(11) NOT NULL DEFAULT '0',
  `producto_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_ofertas`
--

INSERT INTO `tbl_ofertas` (`id`, `variedad`, `otras_caracteristicas`, `volumen_total`, `unidad_id`, `precio_unidad`, `presentacion_id`, `tamano`, `unidad_presentacion_id`, `pedido_minimo`, `unidad_pedido_minimo_id`, `precio`, `disponible_desde`, `disponible_hasta`, `estado_id`, `ciudad`, `codigo_postal`, `resena`, `aprobado`, `usuario_id`, `producto_id`, `created_at`, `updated_at`) VALUES
(141, 'Variedad prueba num 1', 'Otras caracteristicas num 1', 111.00, 5, 11.00, 16, 12.00, 3, 2.00, 4, 'f', '2018-10-03 07:00:00', '2018-10-27 07:00:00', 0, 'Leon', '37999', 'Reseña de producto.', 0, 1, 1, '2018-10-03 18:14:18', '2018-10-03 18:14:18'),
(142, 'Variedad prueba num 1', 'Otras caracteristicas num 1', 111.00, 5, 11.00, 16, 12.00, 3, 2.00, 4, 'f', '2018-10-03 07:00:00', '2018-10-27 07:00:00', 3, 'Leon', '37999', 'Reseña de producto.', 1, 1, 1, '2018-10-03 18:14:50', '2018-10-10 17:51:33'),
(145, 'Variedad prueba num 1', 'Otras caracteristicas num 1', 111.00, 5, 11.00, 16, 12.00, 3, 2.00, 4, 'f', '2018-10-08 07:00:00', '2018-10-27 07:00:00', 1, 'Leon', '37999', 'Reseña de producto.', 0, 1, 1, '2018-10-05 15:46:15', '2018-10-12 21:58:50'),
(146, 'jkljlk', 'hjih', 10.00, 5, 17.00, 14, NULL, 0, 10.00, 0, 'f', '2018-10-06 07:00:00', '2018-10-13 07:00:00', 2, 'irapuato', '36660', NULL, 0, 41, 1, '2018-10-06 21:05:28', '2018-10-06 21:05:28'),
(161, 'vayo', 'dfdf', 10.00, 11, 15.00, 14, NULL, 0, NULL, 0, NULL, '2018-10-06 07:00:00', '2018-10-11 07:00:00', 0, NULL, NULL, NULL, 0, 41, 246, '2018-10-06 21:25:18', '2018-10-06 21:25:18'),
(166, 'verde', NULL, 1333.00, 5, 13.00, 5, 12.00, 7, 200.00, 8, 'f', '2018-10-10 15:54:46', '2018-10-19 07:00:00', 9, 'queretaro', '23456', 'sdfghj', 0, 1, 1, '2018-10-10 17:55:33', '2018-10-10 17:55:33'),
(167, 'non', NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, '2018-10-12 07:00:00', '2018-10-23 07:00:00', 3, 'quiche', '37219', NULL, 0, 1, 1, '2018-10-10 21:18:16', '2018-10-12 21:50:06'),
(168, 'SDSA', 'adsasd', 666.00, 5, 616.00, 11, 23423.00, 5, 23.00, 2, 'v', '2018-10-11 07:00:00', '2018-10-10 07:00:00', 14, 'Duranguito', '23322', 'Prueba', 0, 51, 6, '2018-10-10 21:46:15', '2018-10-10 21:46:15'),
(170, 'gdfs', NULL, 15.00, 11, 15.00, 14, NULL, 0, 10.00, 11, 'v', '2018-10-16 07:00:00', '2018-10-31 07:00:00', 31, 'Jalapa', '366660', NULL, 0, 41, 100, '2018-10-16 13:47:08', '2018-10-16 13:47:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_paises`
--

DROP TABLE IF EXISTS `tbl_paises`;
CREATE TABLE `tbl_paises` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_paises`
--

INSERT INTO `tbl_paises` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'México', NULL, NULL),
(2, 'Guatemala', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_presentaciones`
--

DROP TABLE IF EXISTS `tbl_presentaciones`;
CREATE TABLE `tbl_presentaciones` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_presentaciones`
--

INSERT INTO `tbl_presentaciones` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Bolsa', '2018-07-29 06:49:10', '2018-07-29 06:49:10'),
(2, 'Bonche', '2018-09-26 19:27:31', '2018-09-26 19:27:31'),
(3, 'Bote', '2018-09-26 19:27:31', '2018-09-26 19:27:31'),
(4, 'Botella', '2018-09-26 19:27:31', '2018-09-26 19:27:31'),
(5, 'Caja', '2018-09-26 19:27:31', '2018-09-26 19:27:31'),
(6, 'Canal', '2018-09-26 19:27:31', '2018-09-26 19:27:31'),
(7, 'Cesta', '2018-09-26 19:27:31', '2018-09-26 19:27:31'),
(8, 'Corte', '2018-09-26 19:27:31', '2018-09-26 19:27:31'),
(9, 'Costal', '2018-09-26 19:27:31', '2018-09-26 19:27:31'),
(10, 'Cubeta', '2018-09-26 19:27:31', '2018-09-26 19:27:31'),
(11, 'Entero', '2018-09-26 19:27:31', '2018-09-26 19:27:31'),
(12, 'Envase', '2018-09-26 19:29:33', '2018-09-26 19:29:33'),
(13, 'Frasco', '2018-09-26 19:29:33', '2018-09-26 19:29:33'),
(14, 'Granel', '2018-09-26 19:29:33', '2018-09-26 19:29:33'),
(15, 'Jaula', '2018-09-26 19:29:33', '2018-09-26 19:29:33'),
(16, 'Juego', '2018-09-26 19:29:33', '2018-09-26 19:29:33'),
(17, 'Lata', '2018-09-26 19:29:33', '2018-09-26 19:29:33'),
(18, 'Maleta', '2018-09-26 19:29:33', '2018-09-26 19:29:33'),
(19, 'Manojo', '2018-09-26 19:29:33', '2018-09-26 19:29:33'),
(20, 'Paca', '2018-09-26 19:29:33', '2018-09-26 19:29:33'),
(21, 'Paquete', '2018-09-26 19:29:33', '2018-09-26 19:29:33'),
(22, 'Racimo', '2018-09-26 19:30:38', '2018-09-26 19:30:38'),
(23, 'Ramillete', '2018-09-26 19:30:38', '2018-09-26 19:30:38'),
(24, 'Reja', '2018-09-26 19:30:38', '2018-09-26 19:30:38'),
(25, 'Rollo', '2018-09-26 19:30:38', '2018-09-26 19:30:38'),
(26, 'Sacos', '2018-09-26 19:30:38', '2018-09-26 19:30:38'),
(27, 'Tambores', '2018-09-26 19:30:38', '2018-09-26 19:30:38'),
(28, 'Tazón', '2018-09-26 19:30:38', '2018-09-26 19:30:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_productos`
--

DROP TABLE IF EXISTS `tbl_productos`;
CREATE TABLE `tbl_productos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `imagen_default` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_productos`
--

INSERT INTO `tbl_productos` (`id`, `nombre`, `categoria_id`, `imagen_default`, `created_at`, `updated_at`) VALUES
(1, 'Acelga', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/acelga.jpg', NULL, NULL),
(2, 'Aguacate Criollo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/aguacate.jpg', NULL, NULL),
(3, 'Aguacate Fuerte', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/aguacate.jpg', NULL, NULL),
(4, 'Aguacate Hass', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/aguacate.jpg', NULL, NULL),
(5, 'Aguacate Hass Flor Vieja', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/aguacate.jpg', NULL, NULL),
(6, 'Aguacate Pagua', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/aguacate.jpg', NULL, NULL),
(7, 'Ajo Blanco', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/ajo.jpg', NULL, NULL),
(8, 'Ajo Morado', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/ajo.jpg', NULL, NULL),
(9, 'Apio', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/apio.jpg', NULL, NULL),
(10, 'Berenjena', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/berengena.jpg', NULL, NULL),
(11, 'Betabel', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/betabel.jpg', NULL, NULL),
(12, 'Brócoli', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/brocoli.jpg', NULL, NULL),
(13, 'Cacahuate', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/cacahuate.jpg', NULL, NULL),
(14, 'Calabacita Criolla', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/calabacita.jpg', NULL, NULL),
(15, 'Calabacita Italiana', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/calabacita.jpg', NULL, NULL),
(16, 'Calabacita Regional', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/calabacita.jpg', NULL, NULL),
(17, 'Calabaza de Castilla', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/calabaza.jpg', NULL, NULL),
(18, 'Camote', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(19, 'Caña', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(20, 'Cebolla Bola', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/cebolla.jpg', NULL, NULL),
(21, 'Cebolla Bola Grande', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/cebolla.jpg', NULL, NULL),
(22, 'Cebolla de Rabo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/cebolla.jpg', NULL, NULL),
(23, 'Cebolla Morada', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/cebolla_morada.jpg', NULL, NULL),
(24, 'Cilantro', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/cilantro.jpg', NULL, NULL),
(25, 'Ciruela Amarilla', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/ciruela.jpg', NULL, NULL),
(26, 'Ciruela Claudia', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/ciruela.jpg', NULL, NULL),
(27, 'Ciruela Huesuda Amarilla', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/ciruela.jpg', NULL, NULL),
(28, 'Ciruela Huesuda Roja', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/ciruela.jpg', NULL, NULL),
(29, 'Ciruela Moscatel', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/ciruela.jpg', NULL, NULL),
(30, 'Ciruela Negra', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/ciruela.jpg', NULL, NULL),
(31, 'Ciruela Pasa', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/ciruela.jpg', NULL, NULL),
(32, 'Ciruela Roja', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/ciruela.jpg', NULL, NULL),
(33, 'Coco', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(34, 'Col grande', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(35, 'Col mediana', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(36, 'Col sin clasificación', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(37, 'Coliflor grande', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/coliflor.jpg', NULL, NULL),
(38, 'Coliflor mediana', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/coliflor.jpg', NULL, NULL),
(39, 'Coliflor sin clasificación', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/coliflor.jpg', NULL, NULL),
(40, 'Champiñón', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/champinnon.jpg', NULL, NULL),
(41, 'Chayote', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chayote.jpg', NULL, NULL),
(42, 'Chayote sin espinas', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chayote.jpg', NULL, NULL),
(43, 'Chícharo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chicharo.jpg', NULL, NULL),
(44, 'Chícharo arrugado', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chicharo.jpg', NULL, NULL),
(45, 'Chile Anaheim', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(46, 'Chile Ancho', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(47, 'Chile California', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(48, 'Chile Caloro', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(49, 'Chile Caribe', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(50, 'Chile Cat', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(51, 'Chile Chilaca', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(52, 'Chile de Árbol Fresco', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(53, 'Chile de Árbol Seco', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(54, 'Chile Dulce', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(55, 'Chile Guajillo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(56, 'Chile Habanero', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(57, 'Chile Húngaro', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(58, 'Chile Jalapeño', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/jalapenno.jpg', NULL, NULL),
(59, 'Chile Marisol', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(60, 'Chile Pasilla', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(61, 'Chile Pimiento Morrón', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(62, 'Chile Poblano', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chile poblano.jpg', NULL, NULL),
(63, 'Chile Puya Fresco', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(64, 'Chile Puya Seco', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(65, 'Chile Serrano', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/chiles.jpg', NULL, NULL),
(66, 'Durazno Amarillo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/durazno.jpg', NULL, NULL),
(67, 'Durazno Melocotón', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/durazno.jpg', NULL, NULL),
(68, 'Durazno Pisco', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/durazno.jpg', NULL, NULL),
(69, 'Ejote', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/ejotes.jpg', NULL, NULL),
(70, 'Ejote Largo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/ejotes.jpg', NULL, NULL),
(71, 'Ejote Redondo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/ejotes.jpg', NULL, NULL),
(72, 'Elote', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(73, 'Elote Grande', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(74, 'Elote Mediano', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(75, 'Epazote', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(76, 'Espárrago', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/esparrago.jpg', NULL, NULL),
(77, 'Espinaca', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/espinaca.jpg', NULL, NULL),
(78, 'Fresa', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/fresas.jpg', NULL, NULL),
(79, 'Fresa Chandler', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/fresas.jpg', NULL, NULL),
(80, 'Granada China', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/granada.jpg', NULL, NULL),
(81, 'Granada Roja', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/granada.jpg', NULL, NULL),
(82, 'Guanábana', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/guanabana.jpg', NULL, NULL),
(83, 'Guayaba', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/guayaba.jpg', NULL, NULL),
(84, 'Haba Verde', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(85, 'Jamaica', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(86, 'Jícama', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(87, 'Jícama Mediana', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(88, 'Jícama Piñatera', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(89, 'Kiwi', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/kiwi.jpg', NULL, NULL),
(90, 'Lechuga Orejona Grande', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/lechuga.jpg', NULL, NULL),
(91, 'Lechuga Orejona Sin Clasificación', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/lechuga.jpg', NULL, NULL),
(92, 'Lechuga Romanita Grande', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/lechuga.jpg', NULL, NULL),
(93, 'Lechuga Romanita Mediana', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/lechuga.jpg', NULL, NULL),
(94, 'Lechuga Romanita Sin Clasificación', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/lechuga.jpg', NULL, NULL),
(95, 'Lima', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/lima.jpg', NULL, NULL),
(96, 'Limón c/semilla', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/limon_verde.jpg', NULL, NULL),
(97, 'Limón c/semilla Sin Clasificación', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/limon_verde.jpg', NULL, NULL),
(98, 'Limón s/semilla', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/limon_verde.jpg', NULL, NULL),
(99, 'Mamey', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(100, 'Mandarina', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/mandarina.jpg', NULL, NULL),
(101, 'Mandarina Mónica', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/mandarina.jpg', NULL, NULL),
(102, 'Mandarina Reyna', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/mandarina.jpg', NULL, NULL),
(103, 'Mandarina Tangerina', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/mandarina.jpg', NULL, NULL),
(104, 'Mango Ataulfo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/mango_petacon.jpg', NULL, NULL),
(105, 'Mango Criollo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/mango_petacon.jpg', NULL, NULL),
(106, 'Mango Haden', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/mango_petacon.jpg', NULL, NULL),
(107, 'Mango Kent', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/mango_petacon.jpg', NULL, NULL),
(108, 'Mango Manila', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/mango_petacon.jpg', NULL, NULL),
(109, 'Mango Manililla', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/mango_petacon.jpg', NULL, NULL),
(110, 'Mango Oro', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/mango_petacon.jpg', NULL, NULL),
(111, 'Mango Tommy', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/mango_petacon.jpg', NULL, NULL),
(112, 'Manzana Golden Delicious', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/manzana.jpg', NULL, NULL),
(113, 'Manzana Red Delicious', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/manzana_roja.jpg', NULL, NULL),
(114, 'Manzana Starking', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/manzana.jpg', NULL, NULL),
(115, 'Melón Cantaloupe', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/melon.jpg', NULL, NULL),
(116, 'Melón Cantaloupe Sin Clasificación', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/melon.jpg', NULL, NULL),
(117, 'Membrillo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(118, 'Nanche', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(119, 'Naranja Agria', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/naranja.jpg', NULL, NULL),
(120, 'Naranja March chica', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/naranja.jpg', NULL, NULL),
(121, 'Naranja March grande', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/naranja.jpg', NULL, NULL),
(122, 'Naranja March mediana', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/naranja.jpg', NULL, NULL),
(123, 'Naranja Valencia chica', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/naranja.jpg', NULL, NULL),
(124, 'Naranja Valencia grande', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/naranja.jpg', NULL, NULL),
(125, 'Naranja Valencia mediana', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/naranja.jpg', NULL, NULL),
(126, 'Nectarina', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/naftarina.jpg', NULL, NULL),
(127, 'Nopal', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/nopal.jpg', NULL, NULL),
(128, 'Nopal grande', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/nopal.jpg', NULL, NULL),
(129, 'Nuez', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/nueces.jpg', NULL, NULL),
(130, 'Nuez Cáscara de Papel', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/nueces.jpg', NULL, NULL),
(131, 'Nuez Wichita', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/nueces.jpg', NULL, NULL),
(132, 'Orégano', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(133, 'Papa Alpha', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/papas.jpg', NULL, NULL),
(134, 'Papa Galeana', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/papas.jpg', NULL, NULL),
(135, 'Papa Gema', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/papas.jpg', NULL, NULL),
(136, 'Papa Marciana', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/papas.jpg', NULL, NULL),
(137, 'Papa Rouset', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/papas.jpg', NULL, NULL),
(138, 'Papa San José', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/papas.jpg', NULL, NULL),
(139, 'Papaya Maradol', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/papaya.jpg', NULL, NULL),
(140, 'Papaya Roja', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/papaya.jpg', NULL, NULL),
(141, 'Pepino', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/pepino.jpg', NULL, NULL),
(142, 'Pera Ángel', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/pera.jpg', NULL, NULL),
(143, 'Pera Asiática', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/pera.jpg', NULL, NULL),
(144, 'Pera Bartlett #135', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/pera.jpg', NULL, NULL),
(145, 'Pera Bosco', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/pera.jpg', NULL, NULL),
(146, 'Pera D\'anjou #100', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/pera.jpg', NULL, NULL),
(147, 'Pera Lechera', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/pera.jpg', NULL, NULL),
(148, 'Pera Mantequilla', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/pera.jpg', NULL, NULL),
(149, 'Pera Paraíso', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/pera.jpg', NULL, NULL),
(150, 'Perejil', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/perejil.png', NULL, NULL),
(151, 'Piña Grande', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/pinna.jpg', NULL, NULL),
(152, 'Piña Mediana', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/pinna.jpg', NULL, NULL),
(153, 'Pistache', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/pistache.jpg', NULL, NULL),
(154, 'Pitaya', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/pitahaya.jpg', NULL, NULL),
(155, 'Plátano Chiapas', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/platano.jpg', NULL, NULL),
(156, 'Plátano Chiapas calidad exportación', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/platano.jpg', NULL, NULL),
(157, 'Plátano Dominico', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/platano.jpg', NULL, NULL),
(158, 'Plátano Enano-Gigante', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/platano.jpg', NULL, NULL),
(159, 'Plátano Macho', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/platano.jpg', NULL, NULL),
(160, 'Plátano Manzano', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/platano.jpg', NULL, NULL),
(161, 'Plátano Morado', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/platano.jpg', NULL, NULL),
(162, 'Plátano Pera', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/platano.jpg', NULL, NULL),
(163, 'Plátano Tabasco', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/platano.jpg', NULL, NULL),
(164, 'Plátano Veracruz', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/platano.jpg', NULL, NULL),
(165, 'Rábano', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/rabanos.jpg', NULL, NULL),
(166, 'Sandía Blanca', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/sandia.jpg', NULL, NULL),
(167, 'Sandía Charleston', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/sandia.jpg', NULL, NULL),
(168, 'Sandía Jubilee', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/sandia.jpg', NULL, NULL),
(169, 'Sandía Negra', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/sandia.jpg', NULL, NULL),
(170, 'Sandía Peacock', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/sandia.jpg', NULL, NULL),
(171, 'Sandía Rayada', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/sandia.jpg', NULL, NULL),
(172, 'Sandía Sangría', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/sandia.jpg', NULL, NULL),
(173, 'Tamarindo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(174, 'Tejocote', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(175, 'Tomate Bola', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/tomate_cherry.jpg', NULL, NULL),
(176, 'Tomate Saladette', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/tomate_cherry.jpg', NULL, NULL),
(177, 'Tomate Verde', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/jitomate.jpg', NULL, NULL),
(178, 'Toronja Blanca', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/toronja.jpg', NULL, NULL),
(179, 'Toronja Roja', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/toronja.jpg', NULL, NULL),
(180, 'Toronja Roja Mediana', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/toronja.jpg', NULL, NULL),
(181, 'Toronja Rosada', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/toronja.jpg', NULL, NULL),
(182, 'Tuna', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(183, 'Tuna Blanca', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(184, 'Uva Blanca', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/uva_verde.jpg', NULL, NULL),
(185, 'Uva Calmeria', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/uva_verde.jpg', NULL, NULL),
(186, 'Uva Cardenal', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/uva_verde.jpg', NULL, NULL),
(187, 'Uva Flame', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/uva_verde.jpg', NULL, NULL),
(188, 'Uva Globo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/uva_verde.jpg', NULL, NULL),
(189, 'Uva Moscatel', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/uva_verde.jpg', NULL, NULL),
(190, 'Uva Pasa', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/uva_verde.jpg', NULL, NULL),
(191, 'Uva Perlette', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/uva_verde.jpg', NULL, NULL),
(192, 'Uva Queen', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/uva_verde.jpg', NULL, NULL),
(193, 'Uva s/semilla', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/uva_verde.jpg', NULL, NULL),
(194, 'Uva Superior', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/uva_verde.jpg', NULL, NULL),
(195, 'Uva Thompson', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/uva_verde.jpg', NULL, NULL),
(196, 'Yerbabuena', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(197, 'Zanahoria Leña', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/zanahoria.jpg', NULL, NULL),
(198, 'Zanahoria Mediana', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/zanahoria.jpg', NULL, NULL),
(199, 'Zanahoria Polvo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/zanahoria.jpg', NULL, NULL),
(200, 'Zapote', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(201, 'Zarzamora', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/zarzamora.jpg', NULL, NULL),
(202, 'Yute', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(203, 'Yuca', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(204, 'Vainilla', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/vainilla.jpg', NULL, NULL),
(205, 'Trigo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/trigo.jpg', NULL, NULL),
(208, 'Soya', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(209, 'Sorgo', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(211, 'Perón', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(212, 'Piña', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/pinna.jpg', NULL, NULL),
(213, 'Piñon', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(214, 'Poro', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(215, 'Sábila', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(216, 'Salvado', 1, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(217, 'Concentrados', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(218, 'Condimentos', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(219, 'Salsas', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(220, 'Mermeladas', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(221, 'Postres', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(222, 'Barras energéticas', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(223, 'Conservas', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(224, 'Consome', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(225, 'Frituras', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(226, 'Galletas', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(227, 'Harinas', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(228, 'Miel', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/miel.jpg', NULL, NULL),
(229, 'Huevo', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/huevo.jpg', NULL, NULL),
(230, 'Jaleas', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(231, 'Embutidos', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(232, 'Quesos', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(233, 'Leche', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(234, 'Pastas', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(235, 'Sazonadores', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(236, 'Sopas', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(237, 'Fermentados', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(238, 'Bebidas', 2, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(239, 'Alubia chica', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(240, 'Alubia grande', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(241, 'Arroz pulido Morelos', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(242, 'Arroz pulido sin especificar', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(243, 'Arroz pulido Sinaloa', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(245, 'Frijol Azufrado', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(246, 'Frijol Bayo', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(247, 'Frijol Bayo berrendo', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(248, 'Frijol Cacahuate bola', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(249, 'Frijol Cacahuate largo', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(250, 'Frijol Canario', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(251, 'Frijol Colorado', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(252, 'Frijol Flór de Junio', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(253, 'Frijol Flór de Mayo', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(254, 'Frijol Garbancillo', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(255, 'Frijol Garbancillo zarco', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(256, 'Frijol Mayocoba', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(257, 'Frijol Negro', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(258, 'Frijol Negro Bola', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(259, 'Frijol Negro importado', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(260, 'Frijol Negro Nayarit', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(261, 'Frijol Negro Veracruz', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(262, 'Frijol Ojo de Cabra', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(263, 'Frijol Peruano', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(264, 'Frijol Pinto', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(265, 'Frijol Pinto importado', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(266, 'Frijol Serahui', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(267, 'Frijol Tepari', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(268, 'Frijol Yurimun', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/frijoles.jpg', NULL, NULL),
(269, 'Garbanzo chico', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(270, 'Garbanzo grande', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(271, 'Haba', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(272, 'Lenteja chica', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/lenteja.jpg', NULL, NULL),
(273, 'Lenteja grande', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/lenteja.jpg', NULL, NULL),
(274, 'Maíz blanco', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(275, 'Maíz blanco pozolero', 3, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(276, 'Abejas', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(277, 'Alimento para animales', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(278, 'Avestruz', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(279, 'Becerros', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/becerro.jpg', NULL, NULL),
(280, 'Borregos', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(281, 'Burros', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(282, 'Caballos', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(283, 'Cabras', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(284, 'Cerdos', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(285, 'Chamorro', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(286, 'Codorniz', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(287, 'Conejos', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(288, 'Faisán', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(289, 'Gallinas', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(290, 'Gallos', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(291, 'Lechón', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(292, 'Novillos', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(293, 'Patos', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(294, 'Pavos', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(295, 'Pollos', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(296, 'Torete', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(297, 'Toros', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(298, 'Vacas', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(299, 'Vaquillas', 4, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL),
(300, 'NA', 5, 'http://infood.com.mx/wp-content/uploads/2018/09/default_producto.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_solicitudes`
--

DROP TABLE IF EXISTS `tbl_solicitudes`;
CREATE TABLE `tbl_solicitudes` (
  `id` int(10) UNSIGNED NOT NULL,
  `variedad` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otras_caracteristicas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `volumen_total` double(10,2) DEFAULT NULL,
  `unidad_id` int(11) DEFAULT NULL,
  `precio_unidad` double(10,2) DEFAULT NULL,
  `presentacion_id` int(11) DEFAULT NULL,
  `tamano` double(10,2) DEFAULT NULL,
  `unidad_presentacion_id` int(11) DEFAULT NULL,
  `precio` enum('f','v') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requerido_desde` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `requerido_hasta` timestamp NULL DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `ciudad` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `codigo_postal` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resena` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aprobado` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `usuario_id` int(11) NOT NULL DEFAULT '0',
  `producto_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_solicitudes`
--

INSERT INTO `tbl_solicitudes` (`id`, `variedad`, `otras_caracteristicas`, `volumen_total`, `unidad_id`, `precio_unidad`, `presentacion_id`, `tamano`, `unidad_presentacion_id`, `precio`, `requerido_desde`, `requerido_hasta`, `estado_id`, `ciudad`, `codigo_postal`, `resena`, `aprobado`, `created_at`, `updated_at`, `usuario_id`, `producto_id`) VALUES
(64, 'Variedad', 'Otras caracteristicas', 10.00, 4, 10.00, 16, 10.00, 3, NULL, '2018-10-12 07:00:00', '2018-10-28 07:00:00', 0, 'Leon', '39909', 'asdfasdf', 0, '2018-10-03 21:16:53', '2018-10-03 21:16:53', 1, 3),
(65, 'Negro', NULL, NULL, 0, NULL, 0, NULL, 0, NULL, '2018-10-20 07:00:00', '2018-10-06 07:00:00', 0, NULL, NULL, NULL, 0, '2018-10-03 21:40:42', '2018-10-03 21:40:42', 1, 214),
(67, 'Negro', NULL, NULL, 0, NULL, 0, NULL, 0, NULL, '2018-10-10 15:49:25', '2018-10-27 07:00:00', 1, NULL, NULL, NULL, 0, '2018-10-03 21:41:00', '2018-10-10 17:49:25', 1, 214),
(68, 'Negro', NULL, NULL, 0, NULL, 0, NULL, 0, NULL, '2018-10-20 07:00:00', '2018-10-27 07:00:00', 0, NULL, NULL, NULL, 0, '2018-10-03 21:41:00', '2018-10-03 21:41:00', 1, 214),
(71, 'Variedad', 'Otras caracteristicas', 10.00, 4, 10.00, 16, 10.00, 3, NULL, '2018-10-10 16:04:48', '2018-10-28 07:00:00', 8, 'Leon', '39909', 'asdfasdf', 0, '2018-10-05 21:33:03', '2018-10-10 18:04:48', 1, 246),
(72, 'hass', 'exportacion', 100.00, 11, 17.00, 14, NULL, 0, NULL, '2018-10-07 20:13:17', '2018-10-21 07:00:00', 0, 'uurapan', '555', NULL, 1, '2018-10-06 21:49:22', '2018-10-07 22:13:17', 41, 4),
(73, 'exportacion', 'tamaño criba 8mm', 1000.00, 11, 40.00, 14, NULL, 0, NULL, '2018-10-08 17:19:27', '2018-10-27 07:00:00', 13, 'uruapan', '123', NULL, 0, '2018-10-06 21:57:30', '2018-10-08 19:19:27', 41, 273),
(74, 'Variedad', 'Otras caracteristicas', 10.00, 4, 10.00, 16, 10.00, 3, NULL, '2018-10-10 22:50:40', '2018-10-28 07:00:00', 8, 'Leon', '39909', 'asdfasdf', 1, '2018-10-10 18:05:05', '2018-10-11 00:50:40', 1, 246),
(75, 'no', NULL, NULL, 0, NULL, 0, NULL, 0, NULL, '2018-10-10 07:00:00', '2018-10-28 07:00:00', 0, NULL, NULL, NULL, 1, '2018-10-10 21:17:07', '2018-10-11 01:09:23', 1, 278),
(77, 'loca', 'calibre 250g', 30.00, 11, 17.00, 5, 20.00, 5, NULL, '2018-10-17 07:00:00', '2018-11-11 07:00:00', 2, 'Irapuato', '36660', NULL, 0, '2018-10-16 13:51:37', '2018-10-16 13:51:37', 41, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_tipo_usuario`
--

DROP TABLE IF EXISTS `tbl_tipo_usuario`;
CREATE TABLE `tbl_tipo_usuario` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productor` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_tipo_usuario`
--

INSERT INTO `tbl_tipo_usuario` (`id`, `nombre`, `productor`, `created_at`, `updated_at`) VALUES
(1, 'Pequeño productor', 1, NULL, NULL),
(2, 'Consumidor\r\n', 0, NULL, NULL),
(3, 'Mujer productora\r\n', 1, NULL, NULL),
(4, 'Empresario agricola.\r\n', 1, NULL, NULL),
(5, 'Artesano gourmet.\r\n', 1, NULL, NULL),
(6, 'Comercializador\r\n', 1, NULL, NULL),
(7, 'Procesador artesanal.\r\n', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_unidades`
--

DROP TABLE IF EXISTS `tbl_unidades`;
CREATE TABLE `tbl_unidades` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_unidades`
--

INSERT INTO `tbl_unidades` (`id`, `nombre`) VALUES
(1, 'Brushel'),
(2, 'Galón'),
(3, 'Gramos'),
(4, 'Gruesa'),
(5, 'Kilogramos'),
(6, 'Libras'),
(7, 'Litros'),
(8, 'Metros'),
(9, 'Onzas'),
(10, 'Piezas'),
(11, 'Toneladas'),
(12, 'Mililitros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ap_materno` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ap_paterno` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perfil_avatar` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_alt` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `celular` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empresa_avatar` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rfc` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono_empresa` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado_id` int(11) NOT NULL DEFAULT '0',
  `nombre_comercial` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `semblanza` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ciudad` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_usuario_id` int(11) NOT NULL DEFAULT '0',
  `categoria_id` int(11) NOT NULL DEFAULT '0',
  `estatus` tinyint(1) NOT NULL DEFAULT '0',
  `administrador` tinyint(1) NOT NULL DEFAULT '0',
  `wp_user_id` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `nickname`, `name`, `ap_materno`, `ap_paterno`, `perfil_avatar`, `email_alt`, `celular`, `empresa_avatar`, `rfc`, `telefono_empresa`, `estado_id`, `nombre_comercial`, `semblanza`, `ciudad`, `tipo_usuario_id`, `categoria_id`, `estatus`, `administrador`, `wp_user_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'infood', 'Nameyca 100', 'Prueba', 'Prueba 2', 'http://infood.com.mx/marketPlace/public/storage/avatar/perfil/image_1.jpg', 'laaralterno@gmail.com.mx', '3333333356', 'http://infood.com.mx/marketPlace/public/storage/avatar/empresa/image_1.jpg', '343434', '34343434', 8, 'afsdfasdf', 'asdfasdfasdfasdfasdfasdfasdf', 'Ciudad', 4, 1, 1, 1, 1, NULL, '2018-01-22 00:00:00', '2018-10-10 21:03:44'),
(28, NULL, '', '', '', NULL, 'infoodalterno@infood.com.mx', '', NULL, '', '', 0, '', '', '', 0, 0, 1, 1, 37, NULL, '2018-10-04 08:24:31', '2018-10-08 20:49:06'),
(29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 1, 0, 38, NULL, '2018-10-05 02:14:31', '2018-10-05 02:14:31'),
(30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 1, 0, 39, NULL, '2018-10-05 02:20:52', '2018-10-05 02:20:52'),
(31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 1, 0, 40, NULL, '2018-10-05 02:25:01', '2018-10-05 02:25:01'),
(32, NULL, 'Obed', 'Sánchez', 'Ramírez', 'http://infood.com.mx/marketPlace/public/storage/avatar/perfil/image_32.jpg', 'macross.eo@gmail.com', '5951020073', NULL, '', '', 0, '', '', '', 6, 0, 1, 0, 41, NULL, '2018-10-06 20:32:25', '2018-10-16 13:27:03'),
(33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 1, 0, 42, NULL, '2018-10-06 23:10:11', '2018-10-06 23:10:11'),
(34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 1, 0, 43, NULL, '2018-10-07 01:16:28', '2018-10-07 01:16:28'),
(35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 1, 0, 44, NULL, '2018-10-07 04:41:42', '2018-10-07 04:41:42'),
(36, NULL, '', '', '', 'http://infood.com.mx/marketPlace/public/storage/avatar/perfil/image_36.jpg', '', '', NULL, '', '', 0, '', '', '', 0, 0, 1, 1, 45, NULL, '2018-10-08 19:33:06', '2018-10-10 21:08:23'),
(37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 1, 0, 46, NULL, '2018-10-08 19:34:36', '2018-10-08 19:34:36'),
(38, NULL, 'Luis Alberto ', 'Ramirez', 'Arvizu', 'http://infood.com.mx/marketPlace/public/storage/avatar/perfil/image_38.jpg', 'alterno@gmail.com', '4772191472', NULL, '', '', 2, '', '', 'Leon', 6, 0, 0, 0, 48, NULL, '2018-10-08 22:19:28', '2018-10-09 23:05:47'),
(39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 49, NULL, '2018-10-10 00:53:45', '2018-10-10 00:53:45'),
(40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 50, NULL, '2018-10-10 02:51:00', '2018-10-10 02:51:00'),
(41, NULL, 'G', 'D', 'A', 'http://infood.com.mx/marketPlace/public/storage/avatar/perfil/image_41.jpg', '', '', NULL, '', '', 0, '', '', '', 0, 0, 0, 0, 51, NULL, '2018-10-11 02:31:08', '2018-10-10 21:41:36'),
(42, NULL, 'JUAN JOSE', 'LUNA', 'CAMACHO', 'http://infood.com.mx/marketPlace/public/storage/avatar/perfil/image_42.jpg', '', '', NULL, '', '', 0, '', '', '', 2, 0, 0, 0, 52, NULL, '2018-10-11 02:40:28', '2018-10-10 21:42:28'),
(43, NULL, 'Elia ', 'Corona', 'Cortes', 'http://infood.com.mx/marketPlace/public/storage/avatar/perfil/image_43.jpg', 'vitamaiz@hotmail.com', '4621095240', NULL, '', '', 0, '', '', '', 6, 0, 0, 0, 53, NULL, '2018-10-16 05:23:53', '2018-10-16 00:27:01'),
(44, NULL, '', '', '', 'https://lh3.googleusercontent.com/-qGAuLf02b94/AAAAAAAAAAI/AAAAAAAAC4M/9tlc2dzqiNQ/s96-c/photo.jpg', '', '', NULL, '', '', 0, '', '', '', 0, 0, 0, 0, 54, NULL, '2018-10-16 19:00:58', '2018-10-16 14:01:14');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_calificaciones`
--
ALTER TABLE `tbl_calificaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_usuario_calificado_id_` (`usuario_calificado_id`);

--
-- Indices de la tabla `tbl_categorias`
--
ALTER TABLE `tbl_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_certificaciones`
--
ALTER TABLE `tbl_certificaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_certificaciones_oferta`
--
ALTER TABLE `tbl_certificaciones_oferta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_certificaciones_solicitud`
--
ALTER TABLE `tbl_certificaciones_solicitud`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_comentarios_compra_oferta`
--
ALTER TABLE `tbl_comentarios_compra_oferta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_comentarios_compra_solicitud`
--
ALTER TABLE `tbl_comentarios_compra_solicitud`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_comentarios_oferta`
--
ALTER TABLE `tbl_comentarios_oferta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_comentarios_solicitud`
--
ALTER TABLE `tbl_comentarios_solicitud`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_compras_oferta`
--
ALTER TABLE `tbl_compras_oferta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_compras_solicitud`
--
ALTER TABLE `tbl_compras_solicitud`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_estados`
--
ALTER TABLE `tbl_estados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_favoritos`
--
ALTER TABLE `tbl_favoritos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_imagenes_oferta`
--
ALTER TABLE `tbl_imagenes_oferta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_imagenes_solicitud`
--
ALTER TABLE `tbl_imagenes_solicitud`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_ofertas`
--
ALTER TABLE `tbl_ofertas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_paises`
--
ALTER TABLE `tbl_paises`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_presentaciones`
--
ALTER TABLE `tbl_presentaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_productos`
--
ALTER TABLE `tbl_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_solicitudes`
--
ALTER TABLE `tbl_solicitudes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_tipo_usuario`
--
ALTER TABLE `tbl_tipo_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_unidades`
--
ALTER TABLE `tbl_unidades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `idx_userId` (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;

--
-- AUTO_INCREMENT de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_calificaciones`
--
ALTER TABLE `tbl_calificaciones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tbl_categorias`
--
ALTER TABLE `tbl_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tbl_certificaciones`
--
ALTER TABLE `tbl_certificaciones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `tbl_certificaciones_oferta`
--
ALTER TABLE `tbl_certificaciones_oferta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT de la tabla `tbl_certificaciones_solicitud`
--
ALTER TABLE `tbl_certificaciones_solicitud`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT de la tabla `tbl_comentarios_compra_oferta`
--
ALTER TABLE `tbl_comentarios_compra_oferta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `tbl_comentarios_compra_solicitud`
--
ALTER TABLE `tbl_comentarios_compra_solicitud`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_comentarios_oferta`
--
ALTER TABLE `tbl_comentarios_oferta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `tbl_comentarios_solicitud`
--
ALTER TABLE `tbl_comentarios_solicitud`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tbl_compras_oferta`
--
ALTER TABLE `tbl_compras_oferta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `tbl_compras_solicitud`
--
ALTER TABLE `tbl_compras_solicitud`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tbl_estados`
--
ALTER TABLE `tbl_estados`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `tbl_favoritos`
--
ALTER TABLE `tbl_favoritos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `tbl_imagenes_oferta`
--
ALTER TABLE `tbl_imagenes_oferta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=271;

--
-- AUTO_INCREMENT de la tabla `tbl_imagenes_solicitud`
--
ALTER TABLE `tbl_imagenes_solicitud`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT de la tabla `tbl_ofertas`
--
ALTER TABLE `tbl_ofertas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT de la tabla `tbl_paises`
--
ALTER TABLE `tbl_paises`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_presentaciones`
--
ALTER TABLE `tbl_presentaciones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `tbl_productos`
--
ALTER TABLE `tbl_productos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=301;

--
-- AUTO_INCREMENT de la tabla `tbl_solicitudes`
--
ALTER TABLE `tbl_solicitudes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT de la tabla `tbl_tipo_usuario`
--
ALTER TABLE `tbl_tipo_usuario`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tbl_unidades`
--
ALTER TABLE `tbl_unidades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
