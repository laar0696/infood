<div marginwidth="0" marginheight="0">
    <div dir="ltr" style="background-color:#f7f7f7;margin:0;padding:70px 0 70px 0;width:100%">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color:#ffffff;border:1px solid #dedede;border-radius:3px!important">
                            <tbody>
                                <tr>
                                    <td align="center" valign="top">                            
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color:#80b500;border-radius:3px 3px 0 0!important;color:#ffffff;border-bottom:0;font-weight:bold;line-height:100%;vertical-align:middle;font-family: Roboto,Arial,sans-serif">
                                            <tbody>
                                                <tr>
                                                    <td style="padding:36px 48px;display:block">
                                                        <h1 style="color:#ffffff;font-family: Roboto,Arial,sans-serif;font-size:30px;font-weight:300;line-height:150%;margin:0;text-align:left">Agradecimientos</h1>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color:#ffffff;border:1px solid #dedede;border-radius:3px!important">
                            <tbody>
                                <tr>
                                    <td align="center" valign="top">                            
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color:#fff;border-radius:3px 3px 0 0!important;color:#ffffff;border-bottom:0;line-height:100%;vertical-align:middle;font-family: Roboto,Arial,sans-serif;font-size: 16px;">
                                            <tbody style="color:#3c3c3c">
                                                <tr>
                                                    <td style="padding:36px 48px;display:block;text-align: left">
                                                       Gracias por usar INFOOD
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:36px 48px;display:block">
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:36px 48px;display:block;text-align: center">
                                                        INFOOD
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>