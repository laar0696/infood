<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

use Illuminate\Support\Facades\Auth;

class DashboardForm extends Controller
{
    //

    public function personalDataUpdate(Request $request){

        $user = User::where('ID', '1')->first();
        Auth::login($user);
        $user = Auth::user();

        
        $wp_usermeta = DB::table('infood.wp_usermeta')
            ->where('user_id', $user->ID)
            ->where('')
            ->get();
        
        
        dd($wp_usermeta);

    }

    public function personalDataGet(){
        return json_encode(DB::connection('mysql2')->select('select * from wp_usermeta'));
    }
}
