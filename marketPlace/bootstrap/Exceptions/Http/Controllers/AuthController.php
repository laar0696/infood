<?php

namespace App\Http\Controllers;

use MikeMcLin\WpPassword\Facades\WpPassword;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','signup']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['user_email', 'user_pass']);

        $user = User::where('user_email',$credentials['user_email'])->first();

        if( $user == null ){
            return response()->json(['error' => 'El correo no existe'], 401);
        }else{
            if( WpPassword::check($credentials['user_pass'], $user->user_pass) ){
                return $this->respondWithToken(Auth::login($user));
            }else{
              return response()->json(['error' => 'Contraseña incorrecta'], 401);
            }
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }



    public function signup(Request $request)
    {
        /*
        email: null,
    name: null,
    password: null,
    password_confirmation: null
    */
        //dd(request(['email', 'name', 'password', 'password'])['name'] );

        //comprobar email, no sea repetido
        //comprobar contraseñas



        if( User::where('user_email', $request->input('email') )->first() == null  ){

            if( strcmp( $request->input('password') , $request->input('password_confirmation') == 0 )  ){
                //Las contraseñas son iguales
                $user = new User();
                $user->user_login = $request->input('email');
                $user->user_pass = WpPassword::make( $request->input('password') );
                $user->user_nicename = $request->input('name');
                $user->user_email = $request->input('email');
                $user->user_status = '1';
                $user->display_name = $request->input('name');
                //$user->save();
                return $this->respondWithToken(Auth::login($user));
            }else{
                return response()->json(['error' => 'Las contraseñas no son iguales'], 401);
            }

        }else{
            return response()->json(['error' => 'El correo ya esta registrado'], 401);
        }


        dd($user);

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }


}
