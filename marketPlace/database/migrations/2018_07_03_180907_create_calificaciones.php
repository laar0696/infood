<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalificaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_calificaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calificacion');
            $table->integer('usuario_califica_id');
            $table->integer('usuario_calificado_id');
            $table->string('comentario', 200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_calificaciones');
    }
}
