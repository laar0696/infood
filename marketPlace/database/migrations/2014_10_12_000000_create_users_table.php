<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nickname', 50)->nullable();
            $table->string('name', 50)->nullable();
            $table->string('ap_materno', 50)->nullable();
            $table->string('ap_paterno', 50)->nullable();
            $table->string('perfil_avatar', 200)->nullable();
            $table->string('email_alt', 50)->nullable();
            $table->string('celular', 20)->nullable();
            $table->string('empresa_avatar', 200)->nullable();
            $table->string('rfc', 50)->nullable();
            $table->string('telefono_empresa', 20)->nullable();
            $table->integer('estado_id')->default(0);
            $table->string('nombre_comercial', 100)->nullable();
            $table->string('semblanza', 500)->nullable();
            $table->integer('tipo_usuario_id')->default(0);
            $table->integer('categoria_id')->default(0);
            $table->boolean('estatus')->default(true);
            $table->boolean('administrador')->default(false);
            $table->integer('wp_user_id')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('tbl_users');
    }
}
