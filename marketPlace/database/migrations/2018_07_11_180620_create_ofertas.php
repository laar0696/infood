<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfertas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_ofertas', function(Blueprint $table){
            $table->increments('id');
            $table->string('variedad');
            $table->string('otras_caracteristicas');
            $table->double('volumen_total', 10, 3);
            $table->integer('unidad_id');
            $table->double('precio_unidad', 10, 2);
            $table->integer('presentacion_id');
            $table->double('tamano_presentacion', 10, 3);
            $table->integer('unidad_presentacion_id');
            $table->double('pedido_minimo', 10, 3);
            $table->integer('unidad_pedido_minimo_id');
            $table->enum('precio', ['f', 'v']);
            $table->timestamp('disponible_desde')->nullable();
            $table->timestamp('disponible_hasta')->nullable();
            $table->integer('estado_id');
            $table->string('ciudad');
            $table->string('codigo_postal', 5);
            $table->string('resena', 1000);
            $table->boolean('aprobado');
            $table->integer('usuario_id')->default(0);
            $table->integer('producto_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_ofertas');
    }
}
