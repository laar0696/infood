<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RollBackUserSolicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_solicitudes_compra');
        Schema::dropIfExists('tbl_solicitudes_venta');
        Schema::table('tbl_solicitudes', function(Blueprint $table){
            $table->integer('usuario_id')->default(0);
            $table->dropColumn('pedido_minimo');
            $table->dropColumn('unidad_pedido_minimo_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
