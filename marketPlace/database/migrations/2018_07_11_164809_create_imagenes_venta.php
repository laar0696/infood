<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagenesVenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_imagenes_oferta', function(Blueprint $table){
            $table->increments('id');
            $table->integer('venta_id');
            $table->string('imagen', 200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_imagenes_oferta');
        Schema::dropIfExists('tbl_imagenes_venta');
    }
}
