<?php

use Faker\Generator as Faker;

$factory->define(App\UserMarket::class, function (Faker $faker) {
    return [
         "nickname" => $faker->userName,
         "name" => $faker->firstName($gender = null|'male'|'female'),
         "ap_paterno" => $faker->lastName,
         "ap_materno" => $faker->lastName,
         "perfil_avatar" => $faker->imageUrl($width = 640, $height = 480),
         "email_alt" => $faker->email,
         "celular" => $faker->tollFreePhoneNumber,
         "empresa_avatar" => $faker->imageUrl($width = 640, $height = 480),
         "rfc" => "SAOK-790530-QZ2",
         "telefono_empresa" => $faker->tollFreePhoneNumber,
         "estado_id" => $faker->numberBetween($min = 1, $max = 30),
         "nombre_comercial" => $faker->sentence($nbWords = 6, $variableNbWords = true),
         "semblanza" => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
         "tipo_usuario_id" => $faker->numberBetween($min = 1, $max = 5),
         "categoria_id" => $faker->numberBetween($min = 1, $max = 5),
         "estatus" => '1',
         "administrador" => '0',
         "wp_user_id" => "1"
    ];     
});
