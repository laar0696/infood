<?php

Route::group([

   'middleware' => 'auth'

], function ($router) {

    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');

    Route::post('dataUpdate','DashboardForm@personalDataUpdate');
    Route::post('userDataUpdate','DashboardForm@personalDataUpdate');
    Route::post('updatePassword','DashboardForm@updatePassword');

    Route::get('getUserData','DashboardForm@personalDataGet');
    Route::get('getUserInfo/{id}','DashboardForm@getUserData');

    Route::get('getBillingSelectInfo','DashboardForm@billingSelectInfo');

    //route::get('prueba','DashboardForm@billingSelectInfo'); //ruta para pruebas

    Route::post('getFavoriteProducts', 'DashboardController@getFavoriteProducts');
    Route::get('deleteFavoriteProduct/{id}', 'DashboardController@deleteFavoriteProduct');

    Route::get('getAllUsers','DashboardController@getAllUsers');
    Route::post('changeStatus', 'DashboardController@changeStatus');
    Route::post('changeStatusAdmin', 'DashboardController@changeStatusAdmin');
    Route::post('deleteUser', 'DashboardController@deleteUser');
    Route::post('searchUser', 'DashboardController@searchUser');

    Route::get('setDataInformation','ApplicationController@getInformationForAplication');
    Route::post('saveAplication','ApplicationController@saveApplication');

    Route::post('createCertificate','ApplicationController@addNewCertificate');

    Route::get('getUserApplications','ApplicationController@retrievesUserApplications');
    Route::post('deleteOfert','ApplicationController@deleteApplication');
    Route::post('copyOfert','ApplicationController@copyDataApplication');

    Route::post('setDataEdit','ApplicationController@editApplicationSetData');
    Route::post('setDataUpdate','ApplicationController@editApplication');
    Route::post('getDataApplication','ApplicationController@getDataApplicationResume');
    Route::post('newComment','ApplicationController@addComentario');
    Route::post('eraseImageApplication','ApplicationController@eraseImage');
    // Solicitudes de usuarios

    Route::get('getUserSolicitutes','SolicitudeController@retrievesUserSolicitudes');
    Route::post('copyUserSolicitute','SolicitudeController@copyDataSolicitute');
    Route::post('deleteSolicitute','SolicitudeController@deleteSolicitudes');
    Route::post('getDataSolicitude','SolicitudeController@getDataSolicituteResume');
    Route::post('eraseImageSolicitude','SolicitudeController@eraseImage');

    Route::post('createAplication','SolicitudeController@saveSolicitude');
    Route::post('getSolicitudeInformation', 'SolicitudeController@editSolicitudeSetData');
    Route::post('setSolicitudeInformation', 'SolicitudeController@editSolicitude');
    Route::post('newCommentSolicitude', 'SolicitudeController@addCommentSolicitude');

    //Comentario de prueba arvizu

    // Administrador

    Route::get('getAllAplications', 'ApplicationController@obtainAllProducts');
    Route::post('deleteAdminAplications', 'ApplicationController@deleteApplicationAdministrative');
    Route::post('statusApplication', 'ApplicationController@approved');
    Route::post('eraseImageApplication','ApplicationController@eraseImage');

    Route::get('getAllSolicitutes', 'SolicitudeController@retrievesallUserSolicitudes');
    Route::post('deleteAdminSolicitudes','SolicitudeController@deleteAdministrativeSolicitudes');
    Route::post('statusSolicitude', 'SolicitudeController@approved');

});

Route::get('/getProductors', 'ProductorsController@getProductors');
Route::get('/getStates', 'ProductorsController@getStates');
Route::get('/getCategories', 'ProductorsController@getCategories');
Route::any('getPaginator', 'ProductorsController@getPaginator');
Route::get('getProductor/{id}', 'ProductorsController@getProductor');

Route::group([
    'prefix' => 'marketPlace'
], function ($router){

    //HOME
    Route::get('getOfertasHome', 'MarketPlace@getOfertasHome');
    Route::get('getSolicitudesHome', 'MarketPlace@getSolicitudesHome');



    Route::post('getOfertas', 'MarketPlace@getOfertas');
    Route::post('getSolicitudes','MarketPlace@getSolicitudes');



    Route::get('getCategorias', 'MarketPlace@getCategorias');
    Route::post('getProductos', 'MarketPlace@getProductos');
    Route::get('getEstados', 'MarketPlace@getEstados');
    Route::get('getCertificaciones', 'MarketPlace@getCertificaciones');


    Route::post('getOferta', 'MarketPlace@getOferta');
    Route::post('getOfertasSimilares','MarketPlace@getOfertasSimilares');

    Route::post('getSolicitud', 'MarketPlace@getSolicitud');
    Route::post('getSolicitudesSimilares','MarketPlace@getSolicitudesSimilares');


    Route::post('getCalificaciones','MarketPlace@getCalificaciones');





    Route::group([
      'middleware' => 'auth'
    ], function($router){
      Route::post('addCalificacionCompra','MarketPlace@addCalificacionCompra');

      Route::post('getCompraOferta', 'MarketPlace@getCompraOferta');
      Route::post('getCompraSolicitud', 'MarketPlace@getCompraSolicitud');

      Route::post('addComentarioCompraOferta','MarketPlace@addComentarioCompraOferta');
      Route::post('addComentarioCompraSolicitud','MarketPlace@addComentarioCompraSolicitud');

      Route::post('getCompras', 'DashboardController@getCompras');

      Route::post('compraOferta', 'MarketPlace@compraOferta');
      Route::post('compraSolicitud', 'MarketPlace@compraSolicitud');

      Route::post('addProductoFavorito', 'MarketPlace@addProductoFavorito');

      Route::post('eliminaCompra', 'MarketPlace@eliminaCompra');

      Route::post('getOfertasAdmin', 'DashboardController@getOfertasAdmin');
      Route::post('getPedidosAdmin', 'DashboardController@getPedidosAdmin');

      Route::post('setEstatusCompraOferta', 'MarketPlace@setEstatusCompraOferta');
      Route::post('setEstatusCompraSolicitud', 'MarketPlace@setEstatusCompraSolicitud');

    });

});


Route::get('check', 'AuthController@check');
Route::get('admin', 'AuthController@admin');
Route::post('logout', 'AuthController@logout');
Route::post('login', 'AuthController@login');
Route::post('loginGoogle', 'AuthController@loginGoogle');
Route::post('loginFacebook', 'AuthController@loginFacebook');
Route::post('registroGoogle', 'AuthController@registroGoogle');
Route::post('registroFacebook', 'AuthController@registroFacebook');
Route::post('signup', 'AuthController@signup');
