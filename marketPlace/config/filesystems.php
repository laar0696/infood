<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage/c',
            'visibility' => 'public',
        ],

        'avatar_perfil' => [
            'driver' => 'local',
            'root' => storage_path('app/public/avatar/perfil'),
            'url' => env('APP_URL').'/storage/image/user',
            'visibility' => 'public',
        ],

        'avatar_empresa' => [
            'driver' => 'local',
            'root' => storage_path('app/public/avatar/empresa'),
            'url' => env('APP_URL').'/storage/image/empresa',
            'visibility' => 'public',
        ],

        'certificados' => [
            'driver' => 'local',
            'root' => storage_path('app/public/'),
            'url' => env('APP_URL').'/storage/image/certificates',
            'visibility' => 'public',
        ],

        'imagen_producto_venta' => [
            'driver' => 'local',
            'root' => storage_path('app/public/producto/venta'),
            'url' => env('APP_URL').'/storage/image/productos',
            'visibility' => 'public',
        ],

        'imagen_producto_solicitud' => [
            'driver' => 'local',
            'root' => storage_path('app/public/producto/solicitud'),
            'url' => env('APP_URL').'/storage/image/productos',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
        ],

    ],

];
