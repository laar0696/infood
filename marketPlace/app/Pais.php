<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $guard = 'api';
    protected $table = 'tbl_paises';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
        protected $fillable = [
            'id','nombre'
        ];

        public function states(){
            return $this->belongsToMany('App\Estado','pais_id');
        }
}
