<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
    protected $table = "tbl_calificaciones";
    protected $primaryKey = "id";
    protected $guard = "api";

    protected $fillable = [
        "nombre", "calificacion", "usuario_califica_id", "usuario_calificado_id", "comentario"
    ];


    public function usuarioCalificado(){
      return $this->belongsTo('App\UserMarket', 'usuario_calificado_id');
    }


    public function usuarioCalifica(){
      return $this->belongsTo('App\UserMarket', 'usuario_califica_id');
    }


}
