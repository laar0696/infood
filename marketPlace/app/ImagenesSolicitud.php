<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagenesSolicitud extends Model
{
    protected $table = "tbl_imagenes_solicitud";
    protected $guard = "api";
    protected $primaryKey = "id";

    protected $fillable = [
        "solicitud_id",
        'imagen'
    ];

    public function solicitud(){
        $this->belongsTo('App\Solicitud', 'solicitud_id');
    }

}
