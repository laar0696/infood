<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presentacion extends Model
{
    protected $table = "tbl_presentaciones";
    protected $guard = "api";
    protected $primaryKey = "id";

    protected $fillable = [
        "nombre"
    ];

}
