<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagenesCertificacion extends Model
{
    //`market`.`tbl_certificaciones_oferta`

    protected $table = 'tbl_certificaciones_oferta';
    protected $guard = "api";
    protected $primaryKey = "id";

    protected $fillable = [
        "certificacion_id",
        "oferta_id"
    ];
}
