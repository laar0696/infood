<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CertificacionSolicitud extends Model
{
    //
    protected $table = 'tbl_certificaciones_solicitud';
    protected $guard = "api";
    protected $primaryKey = "id";

    protected $fillable = [
        "certificacion_id",
        "solicitud_id"
    ];
}
