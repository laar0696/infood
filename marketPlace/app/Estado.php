<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    //
    protected $guard = 'api';
    protected $table = 'tbl_estados';
    protected $primaryKey = 'id';



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'id','nombre','pais_id', 'longitud', 'latitud'
      ];



    public function pais(){
      return $this->belongsTo('App\Pais', 'pais_id');
    }
}
