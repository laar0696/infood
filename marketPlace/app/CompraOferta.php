<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompraOferta extends Model
{
    protected $primaryKey = "id";
    protected $table = "tbl_compras_oferta";

    protected $fillable = [
      'unidades',
      'completado'
    ];

    public function usuario(){
      return $this->belongsTo('App\UserMarket', 'usuario_id');
    }

    public function oferta(){
      return $this->belongsTo('App\Oferta', 'oferta_id');
    }

    public function comentarios(){
      return $this->hasMany('App\ComentarioCompraOferta', 'compra_oferta_id');
    }

}
