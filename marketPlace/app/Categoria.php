<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = "tbl_categorias";
    protected $primaryKey = "id";
    protected $guard = "api";

    protected $fillable = [
        "nombre"
    ];

    public function productos(){
        return $this->hasOne('App\Producto', 'categoria_id');
    }
}
