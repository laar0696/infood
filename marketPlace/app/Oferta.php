<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oferta extends Model
{
    protected $table = "tbl_ofertas";
    protected $guard = "api";
    protected $primaryKey = "id";

    protected $fillable = [
        "aprobado",
        "ciudad",
        "codigo_postal",
        "disponible_desde",
        "disponible_hasta",
        "estado_id",
        "otras_caracteristicas",
        "precio",
        "precio_unidad",
        "resena",
        "tamano",
        "unidad_id",
        "pedido_minimo",
        "unidad_pedido_minimo_id",
        "presentacion_id",
        "unidad_presentacion_id",
        "variedad",
        "volumen_total",
        "usuario_id",
        "producto_id"
    ];

    public function estado(){
        return $this->belongsTo('App\Estado', 'estado_id');
    }

    public function presentacion(){
        return $this->belongsTo('App\Presentacion', 'presentacion_id');
    }

    public function unidad(){
        return $this->belongsTo('App\Unidad', 'unidad_id');
    }

    public function unidadPedidoMinimo(){
        return $this->belongsTo('App\Unidad', 'unidad_pedido_minimo_id');
    }

    public function unidadPresentacion(){
        return $this->belongsTo('App\Unidad', 'unidad_presentacion_id');
    }

    public function usuario(){
        return $this->belongsTo('App\UserMarket', 'usuario_id');
    }

    public function imagenes(){
        return $this->hasMany('App\ImagenesOferta', 'venta_id');
    }

    public function producto(){
        return $this->belongsTo('App\Producto', 'producto_id');
    }

    public function certificaciones(){
        return $this->belongsToMany('App\Certificacion', 'tbl_certificaciones_oferta', 'oferta_id', 'certificacion_id');
    }

    public function getDisponibleDesde(){
        $fecha = explode(" ","{$this->disponible_desde}")[0];
        $fecha = explode("-", $fecha);

        $dia = $fecha[2];
        $mes = $fecha[1];
        $anio = $fecha[0];

        return $dia.'/'.$mes.'/'.$anio;
    }

    public function getDisponibleHasta(){
      $fecha = explode(" ","{$this->disponible_hasta}")[0];
      $fecha = explode("-", $fecha);

      $dia = $fecha[2];
      $mes = $fecha[1];
      $anio = $fecha[0];

      return $dia.'/'.$mes.'/'.$anio;
    }
}
