<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    protected $guard = 'api';
    protected $table = 'infood.wp_usermeta';
    protected $primaryKey = 'umeta_id';
    public $timestamps = false;

    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'umeta_id', 'user_id', 'meta_key', 'meta_value'
      ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    
    
}
