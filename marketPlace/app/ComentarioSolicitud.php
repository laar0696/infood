<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentarioSolicitud extends Model
{
    protected $table = "tbl_comentarios_solicitud";
    protected $primaryKey = "id";

    protected $fillable = [
        "solicitud_id","usuario_id","comentario"
    ];
}
