<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentarioCompraOferta extends Model
{
    protected $table = "tbl_comentarios_compra_oferta";
    protected $primaryKey = "id";

    protected $fillable = [
      "comentario",
      "compra_oferta_id",
      "usuario_id"
    ];

    public function compraOferta(){
      return $this->belongsTo('App\CompraOferta','compra_oferta_id');
    }

    public function usuario(){
      return $this->belongsTo('App\UserMarket', 'usuario_id');
    }

}
