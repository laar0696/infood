<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoUsuario extends Model
{
    protected $table = "tbl_tipo_usuario";
    protected $primaryKey = "id";
    protected $guard = "api";

    protected $fillable = [
        "nombre",
        "productor"
    ];
}
