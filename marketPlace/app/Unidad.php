<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidad extends Model
{
    protected $table = "tbl_unidades";
    protected $guard = "api";
    protected $primaryKey = "id";

    protected $fillable = [
        "nombre"
    ];
}
