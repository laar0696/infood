<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Certificacion;
use App\Categoria;
use App\Producto;
use App\Pais;
use App\Estado;
use App\Unidad;
use App\Presentacion;
use App\Oferta;
use App\ImagenesOferta;
use App\ImagenesCertificacion;
use App\ComentarioOferta;
use App\UserMarket;
use Validator;
use DB;
use File;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class ApplicationController extends Controller
{

    public function sendEmail(String $type, $id){

        $offert = Oferta::find($id);

        $mail = new PHPMailer;
        $mail->isSMTP();

        $mail->Host = "smtp.gmail.com";
        $mail->SMTPAuth = true;

        $mail->Username = "cdtst3@gmail.com";
        $mail->Password = "ljrhitovxyxunzhw";

        $mail->SMTPSecure = "tls";
        $mail->Port = 587;

        $mail->From = "name@gmail.com";
        $mail->FromName = "INFOOD";

        switch($type){

            case 'new':
                $mail->Subject = 'Nuevo producto solicitado';
                $mail->Body = File::get(storage_path('emails/newProduct.txt'));

                $direction_emails = DB::table('infood.wp_users')
                                            ->join('tbl_users','infood.wp_users.id','=','tbl_users.wp_user_id')
                                            ->where('.tbl_users.administrador','=','1')
                                            ->select('infood.wp_users.user_email')->get();

                foreach($direction_emails as $dir){
                    $mail->addBCC($dir->user_email);
                }
                $mail->addAddress('contacto@infood.com.mx');
            break;

            case 'approved':
                $mail->Subject = 'Estatus producto';
                $mail->Body = File::get(storage_path('emails/enableProduct.txt'));

                $mail->addAddress(DB::table('infood.wp_users')
                                            ->select('infood.wp_users.user_email')
                                            ->where('infood.wp_users.id','=',$id)
                                            ->first()->user_email);
                $mail->addAddress('contacto@infood.com.mx');
            break;
        }

        $mail->isHTML(true);
        $mail->send();
    }

    public function getInformationForAplication(){
        $countries = Pais::all();
        $states = Estado::all();
        $countriesAux = [];
        $statesAux = [];

        $categories = Categoria::all();
        $products = Producto::all();
        $categoriesAux = [];
        $productsAux = [];

        foreach( $countries as $country ){
            foreach( $states as $state ) {
                if( $state->pais_id == $country->id ) {
                    $statesAux [] = [ 'id'=>$state->id, 'pais_id'=>$state->pais_id, 'name'=>$state->nombre];
                }
            }
            $countriesAux[] = [ 'id'=>$country->id, 'name'=>$country->nombre, 'states'=>$statesAux ];
            $statesAux = [];
        }

        foreach( $categories as $category ){
            foreach( $products as $product ) {
                if( $product->categoria_id == $category->id ) {
                    $productsAux [] = [ 'id'=>$product->id, 'categoria_id'=>$category->id, 'nombre'=>$product->nombre, 'imagen_default'=>$product->imagen_default];
                }
            }
            $categoriesAux[] = [ 'id'=>$category->id, 'nombre'=>$category->nombre, 'products'=>$productsAux ];
            $productsAux = [];
        }

        $info = [
                    'certificates' => Certificacion::all(),
                    'categories' => $categoriesAux,
                    'countries' => $countriesAux,
                    'unities' => Unidad::all(),
                    'presentation' => Presentacion::all()
                ];

        return json_encode($info);
    }

    public function saveApplication(Request $request){

        $data=json_decode($request->offert,true);

        $validator = Validator::make($data, $this->rulesAddAndEdit(), $this->mesaggesAddAndEdit());

        if($validator->passes()) {
            $data = json_decode($request->offert);

            $desde = $data->disponible_desde->jsdate;
            $desde = str_replace('00:00.000Z', '00:00:00', $desde);

            $hasta = $data->disponible_hasta == null ? null : $data->disponible_hasta->jsdate;
            $hasta = str_replace('00:00.000Z', '00:00:00', $hasta);

            $desde = substr($desde, 0, 10) . ' ' . substr($desde, 14, strlen($desde) );
            $hasta = substr($hasta, 0, 10) . ' ' . substr($hasta, 14, strlen($hasta) );


            $offert = new Oferta;

            $offert->aprobado = 0;
            $offert->ciudad = $data->ciudad;
            $offert->codigo_postal = $data->codigo_postal;
            $offert->disponible_desde = $desde; //'2018-07-11 00:00:00'
            $offert->disponible_hasta = $hasta;
            $offert->estado_id = $data->estado_id;
            $offert->otras_caracteristicas = $data->otras_caracteristicas;
            $offert->precio = $data->precio;
            $offert->precio_unidad = $data->precio_unidad;
            $offert->resena = $data->resena;
            $offert->tamano = $data->tamano;
            $offert->unidad_id = $data->unidad_id;//bien
            $offert->pedido_minimo = $data->pedido_minimo;
            $offert->unidad_pedido_minimo_id = $data->unidad_pedido_minimo_id;
            $offert->presentacion_id = $data->presentacion_id;
            $offert->unidad_presentacion_id = $data->unidad_presentacion_id;
            $offert->variedad = $data->variedad;
            $offert->volumen_total = $data->volumen_total;
            $offert->usuario_id = Auth::user()->ID;
            $offert->producto_id = $data->producto_id;

            $offert->save();
            $idSale = $offert->id;

            foreach( $data->photo as $index=>$image ){
                if ( $image->type == 'default' ){
                    ImagenesOferta::create([ 'venta_id'=>$idSale,'imagen'=>$image->imagen ]);
                }else {
                    ImagenesOferta::create([ 'venta_id'=>$idSale,'imagen'=>$this->getDataImage( $image->imagen, 'producto', $index ) ]);
                }
            }

            foreach( $data->certificates as $certificates ){
                ImagenesCertificacion::create([ 'oferta_id'=>$idSale, 'certificacion_id'=>$certificates->id]);
            }

            $this->sendEmail('new',null);

        }else return response()->json(['error' => $validator->errors()->all()]);

    }

    public function editApplication(Request $request){
        $data=json_decode($request->offert,true);

        $direction_emails = DB::table('infood.wp_users')
                                    ->join(env('DB_DATABASE').'.tbl_users','infood.wp_users.id','=',env('DB_DATABASE').'.tbl_users.wp_user_id')
                                    ->where(env('DB_DATABASE').'.tbl_users.administrador','=','1')
                                    ->select('infood.wp_users.user_email');
                                    //->get();
        //dd($direction_emails);

        $validator = Validator::make($data, $this->rulesAddAndEdit(), $this->mesaggesAddAndEdit());

        if($validator->passes()) {
            $data = json_decode($request->offert);
            
            $desde = $data->disponible_desde->date;
            $desde = $desde->year.'-'.$desde->month.'-'.$desde->day.' 00:00:00';

            $hasta = $data->disponible_hasta->date;
            $hasta = $hasta->year.'-'.$hasta->month.'-'.$hasta->day.' 00:00:00';

            $idSale = $data->venta_id;
            //dd($idSale);
            $offert = Oferta::find($idSale);

            $offert->ciudad = $data->ciudad;
            $offert->codigo_postal = $data->codigo_postal;
            $offert->disponible_desde = $desde; //'2018-07-11 00:00:00'
            $offert->disponible_hasta = $hasta;
            $offert->estado_id = $data->estado_id;
            $offert->otras_caracteristicas = $data->otras_caracteristicas;
            $offert->precio = $data->precio;
            $offert->precio_unidad = $data->precio_unidad;
            $offert->resena = $data->resena;
            $offert->tamano = $data->tamano;
            $offert->unidad_id = $data->unidad_id;
            $offert->pedido_minimo = $data->pedido_minimo;
            $offert->unidad_pedido_minimo_id = $data->unidad_pedido_minimo_id;
            $offert->presentacion_id = $data->presentacion_id;
            $offert->unidad_presentacion_id = $data->unidad_presentacion_id;
            $offert->variedad = $data->variedad;
            $offert->volumen_total = $data->volumen_total;
            $offert->producto_id = $data->producto_id;

            $offert->save();
            //$offert->save();


            $images = ImagenesOferta::where('venta_id','=',$idSale)->get();
            $certificados  = ImagenesCertificacion::where('oferta_id',"=",$idSale)->get();


            foreach($certificados as $cert){
                $cert->delete();
            }

            foreach( $data->photo as $index=>$image ){
                if ( $image->type == 'default' ){
                  //  dd($image);
//                    ImagenesOferta::create([ 'venta_id'=>$idSale,'imagen'=>$image->imagen ]);
                }else {
                    ImagenesOferta::create([ 'venta_id'=>$idSale,'imagen'=>$this->getDataImage( $image->imagen, 'producto', $index ) ]);
                }
            }

            foreach( $data->certificates as $certificates ){
                ImagenesCertificacion::create([ 'oferta_id'=>$idSale, 'certificacion_id'=>$certificates->id ]);
            }

        }else return response()->json(['error' => $validator->errors()->all()]);;
    }


    public function addNewCertificate(Request $request){
        $data = json_decode($request->certificate, true);
        $rules = ['nombre'=>'required','imagen'=>'required'];

        $messages = [
                        'nombre.required'=>'El campo nombre debe contener un valor',
                        'imagen.required'=>'Debe contener una imagen'
                    ];

        $validator = Validator::make($data, $rules, $messages);


        if($validator->passes()){
            $data = json_decode($request->certificate);

            $certificate = new Certificacion;
            $certificate->nombre = $data->nombre;
            $certificate->imagen = $this->getDataImage($data->imagen,"certificado",0);
            $certificate->save();
            return json_encode(Certificacion::all());
        }else{
            //dd($validator->errors()->all());
            return response()->json(['error' => $validator->errors()->all()]);
        }
    }

    public function retrievesUserApplications(){
        $producto_clase = [];

        $id = Auth::user()->ID;

        $oferts=DB::table('tbl_ofertas')
            ->where('usuario_id', $id)
            ->get();

        if($oferts != null){
            foreach($oferts as $ofert){

                $image = ImagenesOferta::where('venta_id',$ofert->id)->get();

                $unidad = Unidad::find($ofert->unidad_id);
                $producto = Producto::find($ofert->producto_id);
                $img = "";

                foreach($image as $index=>$i){
                    $index == '0' ? $img = $i->imagen : '' ;
                }

                $producto_clase [] = [
                    "id" => $ofert->id,
                    "imagen" => $img,
                    "precio" => $ofert->precio_unidad,
                    "unidad" => $unidad != null ? $unidad->nombre : 'No especificado',
                    "nombre" => $producto->nombre,
                    "variedad" => $ofert->variedad,
                    "volumen" => $ofert->volumen_total,
                    "disponibleDesde" => date('d/m/Y', strtotime($ofert->disponible_desde)),
                    "disponibleHasta" => date('d/m/Y', strtotime($ofert->disponible_hasta)),
                    "aprobado" => $ofert->aprobado
                ];
                //$product->proveedor = ;
            }
        }

        return json_encode($producto_clase);
    }

    public function deleteApplication(Request $request){
        $id = json_decode($request->id);

        $offert = Oferta::where('id','=',$id)->first();
        $images = ImagenesOferta::where('venta_id','=',$offert->id)->get();
        $certificates = ImagenesCertificacion::where('oferta_id',"=",$offert->id)->get();

        foreach( $images as $img ){
            $image = explode( env('APP_URL').'storage/producto/venta/' , $img->imagen );
            count($image) > 1 ? Storage::disk('imagen_producto_solicitud')->delete($image[1]) : '' ;
            $img->delete();
        }

        foreach( $certificates as $cert ){
            $cert->delete();
        }

        $offert->delete();

        return $this->retrievesUserApplications();
    }

    public function copyDataApplication(Request $request){
        $id = json_decode($request->id);

        $offert_base = Oferta::find($id);
        //dd($offert);
        $offert = $offert_base->replicate();

        $offert->aprobado = 0;

        $offert->save();

        $idSale = $offert->id;

        $imagenes = DB::table('tbl_imagenes_oferta')->where('venta_id',$offert_base->id)->get();
        $certificados = ImagenesCertificacion::where('oferta_id',"=",$offert_base->id)->get();

        foreach( $imagenes as $image ){
            ImagenesOferta::create([ 'venta_id'=>$idSale,'imagen'=>$image->imagen ]);
        }

        foreach( $certificados as $certificates ){
            ImagenesCertificacion::create([ 'oferta_id'=>$idSale, 'certificacion_id'=>$certificates->certificacion_id]);
        }

        $this->sendEmail('new',null);
        return $this->retrievesUserApplications();
    }

    public function editApplicationSetData(Request $request){
        $id = json_decode($request->id);
        $offert = Oferta::find($id);
        $imagenes = DB::table('tbl_imagenes_oferta')->where('venta_id',$id)->get();

        $administrador = UserMarket::where('wp_user_id',Auth::user()->ID)->first()->administrador;

        $certificates_sale = DB::table('tbl_certificaciones_oferta')->where('oferta_id',$id)->get();

        return json_encode(['ofert'=>$offert, 'images'=>$imagenes,'certificatesOffert'=>$certificates_sale,'permisionOptions'=>$administrador ]);
    }

    public function getDataApplicationResume(Request $request){

        $app = app();
        $information = $app->make('stdClass');
        $certificates_sale = [];

        $id = json_decode($request->id);
        $userId = Auth::user()->ID;
        $offert = Oferta::find($id);

        $nombreComercial = UserMarket::where('wp_user_id',$userId)->first()->nombre_comercial;

        $producto_base = Producto::find($offert->producto_id);

        $producto = $producto_base != null ? $producto_base->nombre : 'No especificado';

        $origen_detalle = Estado::find($offert->estado_id);
        $origen = $origen_detalle != null ? $origen_detalle->nombre : 'No especificado';
        $latitud = $origen_detalle != null ? $origen_detalle->latitud : 19.405591;
        $longitud= $origen_detalle != null ? $origen_detalle->longitud : 19.405591;

        $presentacion_detalle = Presentacion::find($offert->presentacion_id);
        $presentacion = $presentacion_detalle != null ? $presentacion_detalle->nombre : 'No especificado';

        $unidad_detalle = Unidad::find($offert->unidad_id);
        $unidad = $unidad_detalle != null ? $unidad_detalle->nombre : 'No especificado' ;

        $tipoPrecio = $offert->precio === 'f' ? 'Fijo' : 'Variable';

        $imagenes = DB::table('tbl_imagenes_oferta')->where('venta_id',$id)->get();
        $certificates = DB::table('tbl_certificaciones_oferta')->where('oferta_id',$id)->get();

        $comentarios = [];
        $comentarios_base = ComentarioOferta::where('oferta_id',"=",$id)->get();

        foreach($comentarios_base as $cbase){
            $user =UserMarket::where('wp_user_id',$cbase->usuario_id)->first()->name;


            $comentarios[] = [
                                'user'=>$user,
                                'fecha'=>date('d/m/Y', strtotime($cbase->created_at)),
                                'comentario'=>$cbase->comentario
                            ];
        }

        $desde = date('d/m/Y',strtotime($offert->disponible_desde));
        $hasta = date('d/m/Y',strtotime($offert->disponible_hasta));

        foreach($certificates as $cert){
           $certificates_sale[] = [Certificacion::find($cert->certificacion_id)->imagen];
        }

        $information->producto = $producto.' '.$offert->variedad;
        $information->precio_unidad = $offert->precio_unidad;
        $information->caracteristicas = $offert->otras_caracteristicas;
        $information->productor = $nombreComercial != '' ? $nombreComercial : 'No especificado';
        $information->produccion_total = $offert->volumen_total != null ? $offert->volumen_total : 'No especificado' ;
        $information->presentacion = $presentacion != '' ? $presentacion : 'No especificado';
        $information->tamano = $offert->tamano != '' ? $offert->tamano : 'No especificado' ;
        $information->desde = $desde;
        $information->hasta = $hasta;
        $information->certificados = $certificates_sale;
        $information->resena = $offert->resena;
        $information->comentarios = $comentarios;
        $information->imagenes = $imagenes;
        $information->unidad = $unidad;
        $information->tipo_precio = $tipoPrecio;
        $information->origen = $origen;
        $information->ciudad = $offert->ciudad;
        $information->lat = $latitud;
        $information->lon = $longitud;

        return json_encode($information);
    }

    public function addComentario(Request $request){
        $comentario = json_decode($request->comentario);
        $userId = UserMarket::where('wp_user_id',Auth::user()->ID)->first()->wp_user_id;

        if($comentario->comentario !=''){
            $transaccion = ComentarioOferta::create([
                                        'oferta_id'=>$comentario->id_oferta,
                                        'usuario_id'=>$userId,
                                        'comentario'=>$comentario->comentario
                                    ]);

            if($transaccion){
                $comentarios = [];
                $comentarios_base = ComentarioOferta::where('oferta_id',"=",$comentario->id_oferta)->get();

                foreach($comentarios_base as $cbase){
                    $user = UserMarket::find($cbase->usuario_id)->nickname;
                    $comentarios[] = [
                                        'user'=>$user,
                                        'fecha'=>date('d/m/Y', strtotime($cbase->created_at)),
                                        'comentario'=>$cbase->comentario
                                    ];
                }
                return json_encode($comentarios);
            }
        }else{
            return 'false';
        }
    }

    public function getDataImage(String $base64Image,$tipo,$number){

        $id = Auth::user()->ID;
        $file_data = $base64Image;
        $file_name = 'image_'.time().'-'.$number.'-'.$id.'.jpg';

        @list($type, $file_data) = explode(';', $file_data);
        @list(, $file_data) = explode(',', $file_data);
        $file_data = str_replace(' ', '+', $file_data);

        if($file_data!=""){

            if($tipo == "certificado"){
                Storage::disk('certificados')->put($file_name,base64_decode($file_data));
                return env('APP_URL').'/storage/certificados/'.$file_name;
                //return 'http://127.0.0.1:8000/storage/certificados/'.$file_name;
            }else{
                Storage::disk('imagen_producto_venta')->put($file_name,base64_decode($file_data));
                return env('APP_URL').'/storage/producto/venta/'.$file_name;
               // return 'http://127.0.0.1:8000/storage/producto/venta/'.$file_name;
            }

        }

    }


    public function rulesAddAndEdit(){
        $rules = [
                    'producto_id'=>'required|regex:/^[1-9][0-9]*/',
                    'variedad'=>'required',
                    'volumen_total'=>'numeric|nullable',
                    'precio_unidad'=>'numeric|nullable',
                    'tamano'=>'numeric|nullable',
                    'pedido_minimo'=>'numeric|nullable',
                    'codigo_postal'=>'nullable',
                    'disponible_desde'=>'required',
                    'disponible_hasta'=>'required'
                ];
            return $rules;
    }

    public function mesaggesAddAndEdit(){
        $mesagges=[
                    'producto_id.required'=>'El campo Producto es obligatorio',
                    'variedad.required'=>'El campo Variedad es obligatorio',
                    'otras_caracteristicas.required'=>'El campo Otras Caracteristicas es obligatorio',
                    'volumen_total.numeric'=>'El campo Volumen Total es numerico',
                    'precio_unidad.numeric'=>'El campo Precio por Unidad es numerico',
                    'tamano.numeric'=>'EL campo Tamaño es numerico',
                    'pedido_minimo.numeric'=>'El campo Pedido minimo es numerico',
                    'disponible_desde.required'=>'El campo Disponibilidad de tu Producto Desde es obligatorio ',
                    'disponible_hasta.required'=>'El campo Disponibilidad de tu Producto Hasta es obligatorio ',
        ];

        return $mesagges;
    }

    public function obtainAllProducts(){
        $producto_clase = [];
	
        $oferts=Oferta::all();
	
        if($oferts != null){
            foreach($oferts as $ofert){

                $image = DB::table('tbl_imagenes_oferta')
                    ->where('venta_id',$ofert->id)
                    ->limit(1)
                    ->get();
                $unidad = Unidad::find($ofert->unidad_id);
                $producto = Producto::find($ofert->producto_id);
                //dd($unidad);
                
                $img = '';
                
                foreach($image as $i){
                    $img = $i->imagen;
                }

                $producto_clase [] = [
                    "id" => $ofert->id,
                    "imagen" => $img,
                    "precio" => $ofert->precio_unidad,
                    "unidad" => $unidad != null ? $unidad->nombre : 'No especificado',
                    "nombre" => $producto->nombre,
                    "variedad" => $ofert->variedad,
                    "volumen" => $ofert->volumen_total,
                    "disponibleDesde" => date('d/m/Y', strtotime($ofert->disponible_desde)),
                    "disponibleHasta" => date('d/m/Y', strtotime($ofert->disponible_hasta)),
                    "aprobado" => $ofert->aprobado
                ];
            }
        }

        return json_encode($producto_clase);
    }

    public function deleteApplicationAdministrative(Request $request){
        $id = json_decode($request->id);

        $offert = Oferta::where('id','=',$id)->first();
        $images = ImagenesOferta::where('venta_id','=',$offert->id);
        $certificates = ImagenesCertificacion::where('oferta_id',"=",$offert->id)->get();

        foreach( $images as $img ){
            $image = explode( env('APP_URL').'storage/producto/venta/' , $img->imagen );
            count($image) > 1 ? Storage::disk('imagen_producto_solicitud')->delete($image[1]) : '' ;
            $img->delete();
        }

        foreach( $certificates as $cert ){
            $cert->delete();
        }

        $offert->delete();

        return $this->obtainAllProducts();
    }

    public function approved(Request $request){
        $id = json_decode($request->id);

        $offert = Oferta::find($id);
        $offert->aprobado == 0 ?  $offert->aprobado = 1 : $offert->aprobado = 0;
        $offert->save();

        $offert->aprobado == 1 ? $this->sendEmail('approved',$offert->usuario_id) : '';

    }

    public function eraseImage(Request $request){
    	$imagen =  ImagenesOferta::find(json_decode($request->id));
    //    $imagen =  ImagenesOferta::find($request->id);

        $image = explode( env('APP_URL').'storage/producto/venta/' , $imagen->imagen );
        count($image) > 1 ? Storage::disk('imagen_producto_solicitud')->delete($image[1]) : '' ;
        $imagen->delete();
     }
}
