<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserMarket;
use App\Estado;
use App\Categoria;
use App\Oferta;

class ProductorsController extends Controller
{

    public function getPaginator(Request $request){
        $pageSize = $request->pageSize;
        $productores = [];
        // dd($request->all());
        if ($request->category != 0){
            if ($request->state != 0) {
                $productoresBD = $posts = UserMarket::whereHas('tipoUsuario', function ($query) {
                    $query->where('productor',1);
                })->where('categoria_id', $request->category)->where('estado_id', $request->state)->paginate($pageSize);
                // $productoresBD = UserMarket::where('categoria_id', $request->category)->where('estado_id', $request->state)->paginate(3);                
            } else {
                $productoresBD = UserMarket::whereHas('tipoUsuario', function ($query) {
                    $query->where('productor',1);
                })->where('categoria_id', $request->category)->paginate($pageSize);
            }
        } else if ($request->state != 0) {
            $productoresBD = UserMarket::whereHas('tipoUsuario', function ($query) {
                $query->where('productor',1);
            })->where('estado_id', $request->state)->paginate($pageSize);
        } else {
            $productoresBD = UserMarket::whereHas('tipoUsuario', function ($query) {
                $query->where('productor',1);
            })->paginate($pageSize);
        }

        foreach ($productoresBD as $productor) {
            $sumaCalificacionesRecibidas =  0;
            
            foreach ($productor->calificacionesRecibidas as $calificacion) {
                $sumaCalificacionesRecibidas += $calificacion->calificacion;
            }
            $calificacion = (count($productor->calificacionesRecibidas)!=0)?$sumaCalificacionesRecibidas/count($productor->calificacionesRecibidas):0;
            $califArray = [];
            for ($i = 0; $i < round($calificacion); $i++) {
                array_push($califArray, ($i+1));
            }
    
            array_push($productores, [
                'productorId' => $productor->id,
                'productorName' => $productor->nombre_comercial,
                'productorImage' => $productor->empresa_avatar,
                'productorCalif' => round($calificacion),
                'productorCategoryId' => isset($productor->categoria) ? $productor->categoria->id : 0,
                'productorCategoryName' => isset($productor->categoria) ? $productor->categoria->nombre : 0,
                'productorStateId' => isset($productor->estado) ? $productor->estado->id : 0,
                'productorStateName' => isset($productor->estado) ? $productor->estado->nombre : 0,
                'productorSemblance' => $productor->semblanza
            ]);
        }

        // dd($request->all());
        return ['paginator' => $productoresBD, 'productors' => $productores];
    }

    public function getProductor($id){
        $productorBD = UserMarket::find($id);
        $productosBD = Oferta::where('usuario_id', $id)->where('aprobado', '1')->get();
        $productos = [];

        //Obtener datos de los productos del productor
        foreach($productosBD as $producto) {
            array_push($productos, [
                'id' => $producto->id,
                'imagen' => $producto->imagenes->first()->imagen,
                'precio' => $producto->precio_unidad,
                'unidad' => $producto->unidad->nombre,
                'nombre' => $producto->producto->nombre,
                'variedad' => $producto->variedad,
                'volumen' => $producto->volumen_total,
                'disponibleDesde' => isset($productorBD->created_at)?\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $producto->disponible_desde)->format('d/m/Y'):'Indefinido',
                'disponibleHasta' => isset($productorBD->created_at)?\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $producto->disponible_hasta)->format('d/m/Y'):'Indefinido',
                'proveedor' => $productorBD->nombre_comercial
            ]);
        }

        //Obtener promedio de calificaciones recibidas
        $sumaCalificacionesRecibidas =  0;
        foreach ($productorBD->calificacionesRecibidas as $calificacion) {
            $sumaCalificacionesRecibidas += $calificacion->calificacion;
        }
        $calificacion = (count($productorBD->calificacionesRecibidas)!=0)?$sumaCalificacionesRecibidas/count($productorBD->calificacionesRecibidas):0;

        $productor = [
            'id' => $productorBD->id,
            'name' => $productorBD->nombre_comercial,
            'image' => $productorBD->empresa_avatar,
            'calif' => round($calificacion),
            'categoryId' => $productorBD->categoria->id,
            'categoryName' => $productorBD->categoria->nombre,
            'stateId' => $productorBD->estado->id,
            'stateName' => $productorBD->estado->nombre,
            'birthday' => isset($productorBD->created_at)?\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $productorBD->created_at)->format('d/m/Y'):'Indefinido',
            'semblance' => $productorBD->semblanza,
            'products' => $productos
        ];

        return response()->json($productor);
    }

    public function getProductors(){
        $productores = [];
        $productoresBD = UserMarket::where('tipo_usuario_id', '1')->paginate(3);
        
        foreach ($productoresBD as $productor) {
            $sumaCalificacionesRecibidas =  0;
            
            foreach ($productor->calificacionesRecibidas as $calificacion) {
                $sumaCalificacionesRecibidas += $calificacion->calificacion;
            }
            $calificacion = (count($productor->calificacionesRecibidas)!=0)?$sumaCalificacionesRecibidas/count($productor->calificacionesRecibidas):0;
            $califArray = [];
            for ($i = 0; $i < round($calificacion); $i++) {
                array_push($califArray, ($i+1));
            }
    
            array_push($productores, [
                'productorId' => $productor->id,
                'productorName' => $productor->nombre_comercial,
                'productorImage' => $productor->empresa_avatar,
                'productorCalif' => round($calificacion),
                'productorCategoryId' => $productor->categoria->id,
                'productorCategoryName' => $productor->categoria->nombre,
                'productorStateId' => $productor->estado->id,
                'productorStateName' => $productor->estado->nombre,
                'productorSemblance' => $productor->semblanza
            ]);
        }

        return response()->json($productores);
    }

    public function getStates(){
        $statesBD = Estado::all();
        $states = [];

        foreach ($statesBD as $state) {
            array_push($states, [
                'stateId' => $state->id,
                'stateName' => $state->nombre,
                'stateCountryId' => $state->pais_id
            ]);
        }

        return response()->json($states);
    }

    public function getCategories(){
        $categoriesBD = Categoria::all();
        $categories = [];

        foreach ($categoriesBD as $category) {
            array_push($categories, [
                'categoryId' => $category->id,
                'categoryName' => $category->nombre,
            ]);
        }

        return response()->json($categories);
    }
}
