<?php

namespace App\Http\Controllers;

use MikeMcLin\WpPassword\Facades\WpPassword;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UserMarket;
use Carbon\Carbon;
use DB;
use File;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
//include '../../wp-load.php';

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','signup','loginGoogle', 'loginFacebook']]);
    }


    public function wplogin(){

      if( Auth::check() ){
        //return loginFunction( Auth::user()->user_login );
      }

    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['user_email', 'user_pass']);

        $user = User::where('user_email',$credentials['user_email'])->first();

        if( $user == null ){
            return response()->json(['error' => 'El correo no existe'], 401);
        }else{
            if( WpPassword::check($credentials['user_pass'], $user->user_pass) ){
                Auth::login($user);
                //$this->wplogin();
                return $this->respondWithToken(Auth::login($user, ['exp' => Carbon::now()->addDays(7)->timestamp] ));
            }else{
              return response()->json(['error' => 'Contraseña incorrecta'], 401);
            }
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginGoogle(Request $request)
    {

        $user = User::where('user_email',$request['email'])->first();
        if( $user == null ){
          $user = new User();
          $user->user_login = $request['email'];
          $user->user_pass = WpPassword::make( rand() . rand() );
          $user->user_nicename = $request['firstName'].' '.$request['lastName'];
          $user->user_email = $request['email'];
          $user->user_status = '1';
          $user->display_name = $request['firstName'].' '.$request['lastName'];
          $user->save();

          $userMarket = new UserMarket();
          $userMarket->wp_user_id = $user->ID;
          $userMarket->perfil_avatar = $request['photoUrl'];

          if($userMarket->save()){
              $this->sendEmail('user',$request['email']);
              $this->sendEmail('admin',null);
          }

        }

        return $this->respondWithToken(Auth::login($user, ['exp' => Carbon::now()->addDays(7)->timestamp]));
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginFacebook(Request $request)
    {
        $user = User::where('user_email',$request['user'])->first();
        if( $user == null ){
            return response()->json(['error' => 'El correo no existe'], 401);
        }else{
            return $this->respondWithToken(Auth::login($user, ['exp' => Carbon::now()->addDays(7)->timestamp]));
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }


    public function signup(Request $request)
    {

        if( User::where('user_email', $request->input('email') )->first() == null  ){

            if( strcmp( $request->input('password') , $request->input('password_confirmation') == 0 )  ){
                //Las contrase���as son iguales
                $user = new User();
                $user->user_login = $request->input('email');
                $user->user_pass = WpPassword::make( $request->input('password') );
                $user->user_nicename = $request->input('name');
                $user->user_email = $request->input('email');
                $user->user_status = '1';
                $user->display_name = $request->input('name');
                $user->save();

                $userMarket = new UserMarket();
                $userMarket->wp_user_id = $user->ID;

                if($userMarket->save()){
                    $this->sendEmail('user',$request->input('email'));
                    $this->sendEmail('admin',null);
                    return $this->respondWithToken(Auth::login($user, ['exp' => Carbon::now()->addDays(7)->timestamp]));
                }else
                    return response()->json(['error' => 'El usuario no pudo ser creado'], 401);

            }else{
                return response()->json(['error' => 'Las contraseñas no son iguales'], 401);
            }JWTAuth::fromUser($user, $customClaims);

        }else{
            return response()->json(['error' => 'El correo ya esta registrado'], 401);
        }


    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
 	      //wp_logout();
        return response()->json(['msg' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {

        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'admin' => Auth::user()->userMarket->administrador,
            'estatus' => Auth::user()->user_status
        ]);



    }


    public function check(){
        return Auth::check().'';
    }

    public function admin(){
        $administrador = Auth::user()->userMarket->administrador == '1';

        return  $administrador.'';
    }

    public function sendEmail(String $type, $email){
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->Host = "smtp.gmail.com";
        $mail->SMTPAuth = true;
        $mail->Username = "cdtst3@gmail.com";
        $mail->Password = "ljrhitovxyxunzhw";
        $mail->SMTPSecure = "tls";
        $mail->Port = 587;
        $mail->From = "name@gmail.com";
        $mail->FromName = "INFOOD";

        switch($type){
            case 'user':
                $mail->Subject = 'Bienvenido';
                $mail->Body = File::get(storage_path('emails/newUser.txt'));
                $mail->addAddress( $email );
                $mail->addAddress( 'contacto@infood.com.mx' );
            break;
            case 'admin':
                $mail->Subject = 'Nuevo usuario';
                $mail->Body = File::get(storage_path('emails/newUserCreated.txt'));
                $direction_emails = DB::table('infood.wp_users')
                                            ->join('tbl_users','infood.wp_users.id','=','tbl_users.wp_user_id')
                                            ->where('tbl_users.administrador','=','1')
                                            ->select('infood.wp_users.user_email')->get();

                foreach($direction_emails as $dir){
                    $mail->addBCC($dir->user_email);
                }
                $mail->addAddress( 'contacto@infood.com.mx' );
            break;
        }

        $mail->isHTML(true);
        $mail->send();
    }


}
