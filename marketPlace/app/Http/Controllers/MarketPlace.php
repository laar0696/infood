<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Oferta;
use App\Categoria;
use App\Producto;
use App\Solicitud;
use App\Estado;
use App\Certificacion;
use App\Favorito;
use App\CompraSolicitud;
use App\CompraOferta;
use Illuminate\Support\Facades\Auth;
use App\ComentarioCompraOferta;
use App\ComentarioCompraSolicitud;
use App\Calificacion;
use DB;

use File;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

use App\CertificacionSolicitud;

class MarketPlace extends Controller
{

    public function getOfertas(Request $rq){

        $ofertas = Oferta::where(function($query) use ($rq) {
            if( $rq->input('categoria') ){
              $query->whereHas('producto', function($q) use ($rq){
                $q->where('categoria_id', $rq->input('categoria')['id']);
              });
            }

            if( $rq->input('estado') ){
              $query->whereHas('estado', function($q) use ($rq){
                $q->where('id', $rq->input('estado')['id']);
              });
            }

            if( $rq->input('producto') ){
              $query->whereHas('producto', function($q) use ($rq){
                $q->where('id', $rq->input('producto')['id']);
              });
            }

            if( $rq->input('fecha') ){
              $fecha = date(
                            'Y-m-d',
                            mktime(
                              0,
                              0,
                              0,
                              $rq->input('fecha')['date']['month'],
                              $rq->input('fecha')['date']['day'],
                              $rq->input('fecha')['date']['year']
                            )
                          );
              $query->whereDate('disponible_desde', '<=', $fecha)
                    ->whereDate('disponible_hasta', '>=', $fecha);
            }

            if( $rq->input('precio') != 0 ){
              $query->where('precio_unidad', '>=', $rq->input('precio') );
            }

            if( $rq->input('certificacion') ){
              $query->whereHas('certificaciones', function($q) use ($rq){
                $q->where('tbl_certificaciones.id', $rq->input('certificacion')['id']);
              });
            }

            if( $rq->input('busqueda') ){

              $palabras = explode( ' ', $rq->input('busqueda') );
              $query->where(function($q) use ($palabras){

                  $q->where('variedad', 'like', '%'.$palabras[0].'%');
                  for($i=1, $l = count($palabras); $i<$l; $i++ ){
                    $q->orWhere('variedad', 'like', '%'.$palabras[$i].'%');
                  }

                  $q->orWhereHas('producto', function($qa) use ($palabras){
                    $qa->where('nombre', 'like', '%'.$palabras[0].'%');
                    for($i=1, $l = count($palabras); $i<$l; $i++ ){
                      $qa->orWhere('nombre', 'like', '%'.$palabras[$i].'%');
                    }
                  });

              });
            }

        })
        ->whereHas('usuario', function($q){
          $q->where('estatus','1');
        })
        ->where('aprobado', 1);

        if( Auth::check() ){
          $auth = Auth::user()->userMarket;
          $ofertas->leftJoin('tbl_favoritos', function($q) use ($auth){
              $q->on('tbl_favoritos.oferta_id', '=', 'tbl_ofertas.id')
                ->where('tbl_favoritos.usuario_id', $auth->id);
          });
          $ofertas->select('tbl_ofertas.*', 'tbl_favoritos.id as fav');
        }

        $ofertas = $ofertas->get();




        $data['paginator']['length'] = count($ofertas);
        $data['paginator']['maxPrice'] = $ofertas->max('precio_unidad');

        $a = $rq->input('paginador')['pageIndex'] * $rq->input('paginador')['pageSize'];
        $b = $rq->input('paginador')['pageSize'];

        $items = array();
        for( $i=0; $i < $b && ($a+$i) < $data['paginator']['length']; $i++ ){
            $oferta = $ofertas[$a+$i];
            $oferta->imagenes;
            $oferta->producto->categoria;
            $oferta->unidad;
            $oferta->usuario;
            $oferta->estado;
            $oferta->fav = $oferta->fav ? true : false;
            $oferta->disponible_desde = $oferta->getDisponibleDesde();
            $oferta->disponible_hasta = $oferta->getDisponibleHasta();
            array_push( $items, $oferta );
        }

        $data['items'] = $items;
        $data['paginator']['pageSize'] = 4;
        $data['paginator']['pageIndex'] = 0;

        return response()->json($data);
    }

    public function getSolicitudes(Request $rq){

        $solicitudes = Solicitud::where(function($query) use ($rq) {

            if( $rq->input('categoria') ){
              $query->whereHas('producto', function($q) use ($rq){
                $q->where('categoria_id', $rq->input('categoria')['id']);
              });
            }

            if( $rq->input('estado') ){
              $query->whereHas('estado', function($q) use ($rq){
                $q->where('id', $rq->input('estado')['id']);
              });
            }

            if( $rq->input('producto') ){
              $query->whereHas('producto', function($q) use ($rq){
                $q->where('id', $rq->input('producto')['id']);
              });
            }

            if( $rq->input('fecha') ){
              $fecha = date(
                            'Y-m-d',
                            mktime(
                              0,
                              0,
                              0,
                              $rq->input('fecha')['date']['month'],
                              $rq->input('fecha')['date']['day'],
                              $rq->input('fecha')['date']['year']
                            )
                          );
              $query->whereDate('requerido_desde', '<=', $fecha)
                    ->whereDate('requerido_hasta', '>=', $fecha);
            }

            if( $rq->input('precio') != 0 ){
              $query->where('precio_unidad', '>=', $rq->input('precio') );
            }

            if( $rq->input('certificacion') ){
              $query->whereHas('certificaciones', function($q) use ($rq){
                $q->where('tbl_certificaciones.id', $rq->input('certificacion')['id']);
              });
            }

            if( $rq->input('busqueda') ){

              $palabras = explode( ' ', $rq->input('busqueda') );
              $query->where(function($q) use ($palabras){

                  $q->where('variedad', 'like', '%'.$palabras[0].'%');
                  for($i=1, $l = count($palabras); $i<$l; $i++ ){
                    $q->orWhere('variedad', 'like', '%'.$palabras[$i].'%');
                  }

                  $q->orWhereHas('producto', function($qa) use ($palabras){
                    $qa->where('nombre', 'like', '%'.$palabras[0].'%');
                    for($i=1, $l = count($palabras); $i<$l; $i++ ){
                      $qa->orWhere('nombre', 'like', '%'.$palabras[$i].'%');
                    }
                  });

              });
            }

        })->where('aprobado', 1)
          ->whereHas('usuario', function($q){
              $q->where('estatus','1');
          })
          ->get();


        $data['paginator']['length'] = count($solicitudes);
        $data['paginator']['maxPrice'] = $solicitudes->max('precio_unidad');

        $a = $rq->input('paginador')['pageIndex'] * $rq->input('paginador')['pageSize'];
        $b = $rq->input('paginador')['pageSize'];

        $items = array();
        for( $i=0; $i < $b && ($a+$i) < $data['paginator']['length']; $i++ ){
          $solicitud = $solicitudes[$a+$i];
          $solicitud->imagenes;
          $solicitud->producto->categoria;
          $solicitud->unidad;
          $solicitud->usuario;
          $solicitud->estado;
          $solicitud->requerido_desde = $solicitud->getRequeridoDesde();
          $solicitud->requerido_hasta = $solicitud->getRequeridoHasta();
          array_push( $items, $solicitud );
        }

        $data['items'] = $items;
        $data['paginator']['pageSize'] = 4;
        $data['paginator']['pageIndex'] = 0;

        return response()->json($data);
    }

    public function getCategorias(){

        $categorias = Categoria::all();
        return response()->json($categorias);
    }

    public function getProductos(Request $request){

      if( count( $request->all() ) == 0 ){
        return response()->json(Producto::all());
      }

      $productos = Producto::whereHas('categoria', function($q) use ($request){
        $q->where('id', $request->input('id'));
      })->get();

      return response()->json($productos);
    }

    public function getEstados(){

        $estados = Estado::all();
        return response()->json($estados);
    }

    public function getCertificaciones(){

        $certificaciones = Certificacion::all();
        return response()->json($certificaciones);
    }

    public function getSolicitudesHome(){
        $solicitudes = Solicitud::take(4)
                        ->where('aprobado', true)
                        ->whereHas('usuario', function($q){
                          $q->where('estatus','1');
                        })
                        ->get();
        foreach( $solicitudes as $solicitud){

            $solicitud->imagenes;
            $solicitud->producto->categoria;
            $solicitud->unidad;
            $solicitud->usuario;
            $solicitud->estado;
            $solicitud->requerido_desde = $solicitud->getRequeridoDesde();
            $solicitud->requerido_hasta = $solicitud->getRequeridoHasta();

        }

         return response()->json( $solicitudes );
    }

    public function getOfertasHome(){
        $ofertas = Oferta::take(4)
                        ->whereHas('usuario', function($a){
                          $a->where('estatus', '1');
                        })
                        ->where('aprobado', true)
                        ->get();

        foreach( $ofertas as $oferta){

            $oferta->imagenes;
            $oferta->producto->categoria;
            $oferta->unidad;
            $oferta->usuario;
            $oferta->estado;
            $oferta->disponible_desde = $oferta->getDisponibleDesde();
            $oferta->disponible_hasta = $oferta->getDisponibleHasta();

        }

        return response()->json( $ofertas );
    }

    public function addProductoFavorito( Request $rq ){

        $auth = Auth::user()->userMarket;
        if( $rq->input('estado') ){
          $favorito = new Favorito();
          $favorito->usuario_id = $auth->id;
          $favorito->oferta_id = $rq->input('id');
          $favorito->save();
        }else{
          Favorito::where('usuario_id', '=', $auth->id)
                  ->where('oferta_id', '=', $rq->input('id') )
                  ->delete();
        }

    }

    public function getOferta( Request $rq ){
      $oferta = Oferta::find( $rq->input('id') );
      $oferta->imagenes;
      $oferta->producto->categoria;
      $oferta->unidad;
      $oferta->unidadPresentacion;
      $oferta->usuario;
      $oferta->estado;
      $oferta->disponible_desde = $oferta->getDisponibleDesde();
      $oferta->disponible_hasta = $oferta->getDisponibleHasta();
      $oferta->certificaciones;
      $oferta->presentacion;


      $calificacion = 0;
      $calificaciones = $oferta->usuario->calificacionesRecibidas;
      $calificaciones_l = count( $calificaciones );
      for( $i=0; $i<$calificaciones_l; $i++) {
        $calificacion += $calificaciones[$i]->calificacion;
      }

      if( $calificacion != 0  && $calificaciones_l > 0 ){
        $calificacion /= count( $calificaciones );
      }

      $oferta->calificacion = (int)($calificacion);


      return response()->json($oferta);
    }

    public function getOfertasSimilares(Request $rq){

      $producto = Producto::find( $rq->producto_id );

      if( Auth::user() ){
      	      $auth = Auth::user()->userMarket;
	      $ofertas = Oferta::whereHas('producto', function($q) use($producto){
	                    $q->where('id', $producto->id );
	                    $q->orWhereHas('categoria', function($qp) use($producto){
	                      $qp->where('id', $producto->categoria->id);
	                   });
	                })
	                ->where('aprobado','1')
	                ->whereHas('usuario',function($q){
	                  $q->where('estatus', '1');
	                })
	                ->leftJoin('tbl_favoritos', function($q) use ($auth){
	              		$q->on('tbl_favoritos.oferta_id', '=', 'tbl_ofertas.id')
	                	->where('tbl_favoritos.usuario_id', $auth->id);
	          	})
	                ->select('tbl_ofertas.*', 'tbl_favoritos.id as fav')
	                ->paginate( $rq->size );

      }else{
      		$ofertas = Oferta::whereHas('producto', function($q) use($producto){
	                    $q->where('id', $producto->id );
	                    $q->orWhereHas('categoria', function($qp) use($producto){
	                      $qp->where('id', $producto->categoria->id);
	                   });
	                })
	                ->where('aprobado','1')
	                ->whereHas('usuario',function($q){
	                  $q->where('estatus', '1');
	                })
	                ->paginate( $rq->size );

      }



      foreach($ofertas as $oferta){
          $oferta->imagenes;
          $oferta->producto->categoria;
          $oferta->unidad;
          $oferta->usuario;
          $oferta->estado;
          $oferta->fav = $oferta->fav ? true : false;
          $oferta->disponible_desde = $oferta->getDisponibleDesde();
          $oferta->disponible_hasta = $oferta->getDisponibleHasta();
      }


      return response()->json($ofertas);

    }

    public function getSolicitud( Request $rq ){
      $solicitud = Solicitud::find( $rq->input('id') );
      $solicitud->imagenes;
      $solicitud->producto->categoria;
      $solicitud->unidad;
      $solicitud->unidadPresentacion;
      $solicitud->usuario;
      $solicitud->estado;
      $solicitud->requerido_desde = $solicitud->getRequeridoDesde();
      $solicitud->requerido_hasta = $solicitud->getRequeridoHasta();
      $solicitud->certificaciones;
      $solicitud->presentacion;


      $calificacion = 0;
      $calificaciones = $solicitud->usuario->calificacionesRecibidas;
      $calificaciones_l = count( $calificaciones );
      for( $i=0; $i<$calificaciones_l; $i++) {
        $calificacion += $calificaciones[$i]->calificacion;
      }

      if( $calificacion != 0  && $calificaciones_l > 0 ){
        $calificacion /= count( $calificaciones );
      }

      $solicitud->calificacion = round($calificacion);


      return response()->json($solicitud);
    }

    public function getSolicitudesSimilares(Request $rq){

      $producto = Producto::find( $rq->producto_id );

      $solicitudes = Solicitud::whereHas('producto', function($q) use($producto){
                    $q->where('id', $producto->id );
                    $q->orWhereHas('categoria', function($qp) use($producto){
                      $qp->where('id', $producto->categoria->id);
                   });
                })
                ->whereHas('usuario', function($a){
                  $a->where('estatus','1');
                })
                ->where('aprobado', '1')
                ->paginate($rq->size);

      $data = array();

      foreach($solicitudes as $solicitud){
          $solicitud->imagenes;
          $solicitud->producto->categoria;
          $solicitud->unidad;
          $solicitud->usuario;
          $solicitud->estado;
          $solicitud->requerido_desde = $solicitud->getRequeridoDesde();
          $solicitud->requerido_hasta = $solicitud->getRequeridoHasta();
          array_push( $data, $solicitud);
      }

      return response()->json($solicitudes);
    }








    public function compraOferta(Request $rq){
        $usuario = Auth::user();
        $usuario = $usuario->userMarket;
        $compraOferta = new CompraOferta();
        $compraOferta->usuario_id = $usuario->id;
        $compraOferta->unidades = $rq->input('unidades');
        $compraOferta->oferta_id = $rq->input('oferta_id');
        $compraOferta->save();

        $this->sendEmail('ofert', $usuario->id);

        return response()->json(['msg' => $compraOferta->id]);
    }





    public function compraSolicitud(Request $rq){
        $usuario = Auth::user();
        $usuario = $usuario->userMarket;
        $compraSolicitud = new CompraSolicitud();
        $compraSolicitud->usuario_id = $usuario->id;
        $compraSolicitud->unidades = $rq->input('unidades');
        $compraSolicitud->solicitud_id = $rq->input('solicitud_id');
        $compraSolicitud->save();

        $this->sendEmail('solicitude', $usuario->id);
        return response()->json(['msg' => $compraSolicitud->id]);
    }


    public function getCompraOferta(Request $rq){

      $compra = CompraOferta::find($rq->input('id'));
      $oferta = $compra->oferta;
      $oferta->imagenes;
      $oferta->producto->categoria;
      $oferta->unidad;
      $oferta->usuario;
      $oferta->unidadPresentacion;
      $oferta->estado;
      $oferta->presentacion;
      $oferta->disponible_desde = $oferta->getDisponibleDesde();
      $oferta->disponible_hasta = $oferta->getDisponibleHasta();

      $comentarios = $compra->comentarios;
      if( $comentarios != null ){
        $hoy = date("Y-m-d");
        foreach ($comentarios as $comentario) {


          $dia = explode(' ', $comentario->created_at)[0];
          $hora = explode(' ', $comentario->created_at)[1];


          if( $hoy == $dia ){
            $comentario->fecha = "Hoy ".$hora;
          }else{
            $mes = explode('-',$dia)[2];
            $anio = explode('-',$dia)[0];
            $dia = explode('-',$dia)[1];
            $comentario->fecha = $dia.'/'.$mes.'/'.$anio;
          }

          $comentario->usuario;
        }
      }
      $datos = array();
      $datos[0]['unidades'] = $compra->unidades;
      $datos[0]['oferta'] = $oferta;
      $datos[0]['comentarios'] = $comentarios;
      $datos[0]['compra'] = $compra;
      $datos[0]['completado'] = $compra->completado;

      return response()->json(['items' => $datos],200);
    }

    public function getCompraSolicitud(Request $rq){

      $hoy = date('Y-m-d');

      $compra = CompraSolicitud::find($rq->input('id'));

      $solicitud = $compra->solicitud;
      $solicitud->imagenes;
      $solicitud->producto->categoria;
      $solicitud->unidad;
      $solicitud->usuario;
      $solicitud->unidadPresentacion;
      $solicitud->estado;
      $solicitud->presentacion;
      $solicitud->requerido_desde = $solicitud->getRequeridoDesde();
      $solicitud->requerido_hasta = $solicitud->getRequeridoHasta();

      $comentarios = $compra->comentarios;
      if( $comentarios != null ){
        $hoy = date("Y-m-d");
        foreach ($comentarios as $comentario) {


          $dia = explode(' ', $comentario->created_at)[0];
          $hora = explode(' ', $comentario->created_at)[1];


          if( $hoy == $dia ){
            $comentario->fecha = "Hoy ".$hora;
          }else{
            $mes = explode('-',$dia)[2];
            $anio = explode('-',$dia)[0];
            $dia = explode('-',$dia)[1];
            $comentario->fecha = $dia.'/'.$mes.'/'.$anio;
          }

          $comentario->usuario;
        }
      }

      $certificacion = CertificacionSolicitud::where('solicitud_id',$compra->solicitud_id)->get();

      $certificaciones = [];

      foreach( $certificacion as $cer ){
        $aux = Certificacion::find($cer->certificacion_id);
        $certificaciones [] = [ 'imagen'=>$aux->imagen ];
      }

      $datos = array();
      $datos[0]['unidades'] = $compra->unidades;
      $datos[0]['solicitud'] = $solicitud;
      $datos[0]['comentarios'] = $comentarios;
      $datos[0]['compra'] = $compra;
      $datos[0]['completado'] = $compra->completado;
      $datos[0]['certificaciones'] = $certificaciones;

      return response()->json(['items' => $datos],200);
    }


    public function addComentarioCompraOferta(Request $rq){

      $comentario = new ComentarioCompraOferta();
      $comentario->comentario = $rq->input('comentario');
      $comentario->usuario_id = Auth::user()->userMarket->id;
      $comentario->compra_oferta_id = $rq->input('id');
      $comentario->save();

      $hoy = date("Y-m-d");

      $dia = explode(' ', $comentario->created_at)[0];
      $hora = explode(' ', $comentario->created_at)[1];


      if( $hoy == $dia ){
        $comentario->fecha = "Hoy ".$hora;
      }else{
        $mes = explode('-',$dia)[2];
        $anio = explode('-',$dia)[0];
        $dia = explode('-',$dia)[1];
        $comentario->fecha = $dia.'/'.$mes.'/'.$anio;
      }

      $comentario->usuario;

      $items = array();
      $items[0] = $comentario;
      return response()->json(['items' => $items], 200);
    }



    public function addComentarioCompraSolicitud(Request $rq){

      $comentario = new ComentarioCompraSolicitud();
      $comentario->comentario = $rq->input('comentario');
      $comentario->usuario_id = Auth::user()->userMarket->id;
      $comentario->compra_solicitud_id = $rq->input('id');
      $comentario->save();

      $hoy = date("Y-m-d");

      $dia = explode(' ', $comentario->created_at)[0];
      $hora = explode(' ', $comentario->created_at)[1];


      if( $hoy == $dia ){
        $comentario->fecha = "Hoy ".$hora;
      }else{
        $mes = explode('-',$dia)[2];
        $anio = explode('-',$dia)[0];
        $dia = explode('-',$dia)[1];
        $comentario->fecha = $dia.'/'.$mes.'/'.$anio;
      }

      $comentario->usuario;

      $items = array();
      $items[0] = $comentario;
      return response()->json(['items' => $items], 200);
    }

    public function addCalificacionCompra(Request $rq){

      $compra = CompraOferta::find( $rq->input('compra_id'));

      $calificacion = new Calificacion();
      $calificacion->calificacion = $rq->input('puntos');
      $calificacion->comentario = $rq->input('comentario');
      $calificacion->usuario_califica_id = Auth::user()->userMarket->id;
      $calificacion->usuario_calificado_id = $compra->oferta->usuario->id;
      $calificacion->save();

      return response()->json(['msg'=>'Calificacion exitosa'], 200);

    }

    public function eliminaCompra(Request $rq){
        $compra_id = $rq->compra_id;

        if( $rq->tipo_compra == 'oferta' ){
          $compra = CompraOferta::find($compra_id);
        }else if( $rq->tipo_compra == 'solicitud' ){
          $compra = CompraSolicitud::find($compra_id);
        }

        isset($compra) ? $compra->delete() : '';

        return response()->json([ 'msg' => 'Compra eliminada', 'items' => array() ],200);

    }

    public function getCalificaciones(Request $rq){
        $oferta_id = $rq->oferta_id;
        $usuario_calificado = Oferta::find( $oferta_id )->usuario;
        $calificaciones = Calificacion::whereHas('usuarioCalificado', function($query) use ($usuario_calificado) {
          $query->where('id', $usuario_calificado->id);
          $query->where('estatus', '1');
        })->get();

        $items = array();
        for( $i=0, $l=count( $calificaciones ); $i<$l; $i++){
          $calificacion = $calificaciones[$i];
          $calificacion->usuarioCalifica;
          $hoy = date('Y').'-'.date('m').'-'.date('d');
          $fecha = explode(' ', $calificacion->created_at )[0];
          $hora = explode(' ', $calificacion->created_at )[1];

          $created_at = '';
          if( $hoy == $fecha ){
            $created_at = 'Hoy - ';
          }else{
            $created_at = explode('-', $fecha)[2].'/'.explode('-', $fecha)[1].'/'.explode('-', $fecha)[0].' - ';
          }

          $created_at .= explode(':', $hora)[0].':'.explode(':', $hora)[1];

          $calificacion->fecha = $created_at;
          array_push( $items, $calificacion );
        }

        return response()->json(['items' => $items], 200);
    }


    public function setEstatusCompraOferta(Request $rq){

      if( Auth::user()->userMarket->administrador != '1' ){
        return response()->json(['msg'=>'No esta autorizado'], 403);
      }

      $compra_oferta = CompraOferta::find( $rq->id );
      $compra_oferta->completado = $rq->completado;
      $compra_oferta->save();

      return response()->json(['msg'=> 'El cambio se realizo correctamente'], 200);

    }


    public function setEstatusCompraSolicitud(Request $rq){

      if( Auth::user()->userMarket->administrador != '1' ){
        return response()->json(['msg'=>'No esta autorizado'], 403);
      }

      $compra_solicitud = CompraSolicitud::find( $rq->id );
      $compra_solicitud->completado = $rq->completado;
      $compra_solicitud->save();

      return response()->json(['msg' => 'El cambio se realizo correctamente'], 200);

    }


    public function sendEmail(String $type, $id){

      $mail = new PHPMailer;
      $mail->isSMTP();

      $mail->Host = "smtp.gmail.com";
      $mail->SMTPAuth = true;

      $mail->Username = "cdtst3@gmail.com";
      $mail->Password = "ljrhitovxyxunzhw";

      $mail->SMTPSecure = "tls";
      $mail->Port = 587;

      $mail->From = "name@gmail.com";
      $mail->FromName = "INFOOD";

      switch($type){

          case 'ofert':
              $mail->Subject = 'Nuevo pedido';
              $mail->Body = File::get(storage_path('emails/newOrder.txt'));
              $mail->addAddress(DB::table('infood.wp_users')
                                  ->join('tbl_users','infood.wp_users.id','=','tbl_users.wp_user_id')
                                  ->where('tbl_users.id','=',$id)
                                  ->select('infood.wp_users.user_email')->first()->user_email);
              $mail->addAddress('contacto@infood.com.mx');
          break;

          case 'solicitude':
              $mail->Subject = 'Nueva solicitud';
              $mail->Body = File::get(storage_path('emails/newSnugPrice.txt'));
              $mail->addAddress(DB::table('infood.wp_users')
                                  ->join('tbl_users','infood.wp_users.id','=','tbl_users.wp_user_id')
                                  ->where('tbl_users.id','=',$id)
                                  ->select('infood.wp_users.user_email')->first()->user_email);
              $mail->addAddress('contacto@infood.com.mx');
          break;
      }

      $mail->isHTML(true);
      $mail->send();
  }

}
