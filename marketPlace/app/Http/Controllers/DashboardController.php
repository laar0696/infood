<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Solicitud;
use App\Favorito;
use App\UserMarket;
use App\User;
use App\CompraSolicitud;
use App\CompraOferta;
use App\UserMeta;
use DB;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function getFavoriteProducts(Request $rq){

        if(!Auth::check()){
          return response()->json(['No esta autorizado'], 403);
        }


        $productosBD = Auth::user()->userMarket->favoritos;
        $productos = [];

        $page_index = $rq->page_index == 0 ? 0 : $rq->page_index - 1;
        $page_size = $rq->page_size;

        $favs_a = $page_index * $page_size;
        $favs_b = $favs_a + $page_size;

        for(; $favs_a <= $favs_b && isset($productosBD[$favs_a]) ; $favs_a++ ) {
            $favorito = $productosBD[$favs_a];
            array_push($productos, [
                'id' => $favorito->oferta->id,
                'imagen' => $favorito->oferta->imagenes->first()->imagen,
                'precio' => isset($favorito->oferta->precio_unidad) ? $favorito->oferta->precio_unidad : 'No especificado',
                'unidad' => isset($favorito->oferta->unidad->nombre) ? $favorito->oferta->unidad->nombre : '',
                'nombre' => $favorito->oferta->producto->nombre,
                'variedad' => isset($favorito->oferta->variedad) ? $favorito->oferta->variedad : 'No especificado',
                'volumen' => isset($favorito->oferta->volumen_total) ? $favorito->oferta->volumen_total : 'No especificado',
                'disponibleDesde' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $favorito->oferta->disponible_desde)->format('d/m/Y'),
                'disponibleHasta' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $favorito->oferta->disponible_hasta)->format('d/m/Y'),
                'proveedor' => isset($favorito->oferta->usuario->nombre_comercial) ? $favorito->oferta->usuario->nombre_comercial : 'No especificado'
            ]);
        }
        $paginator = array();
        $paginator['Plength'] = count($productosBD);
        $paginator['Pindex'] = $page_index;
        // $productosBD = Solicitud::where('usuario_id', $id)->get();
        return response()->json(['productos' => $productos, 'paginator' => $paginator]);
    }

    public function deleteFavoriteProduct($id){

        if(!Auth::check()){
          return response()->json(['No esta autorizado'], 403);
        }

        $user_id =  Auth::user()->userMarket->id;

        $favorito = Favorito::whereHas('usuario', function($query) use($user_id){
                      $query->where('id', $user_id);
                    })
                    ->where('oferta_id',$id)
                    ->first();

        if( $favorito ){
          $favorito->delete();
        }

        /*return redirect()->action(
            'DashboardController@getFavoriteProducts', ['id' => $user_id, 'token' => Auth::login( Auth::user() ) ]
        );*/
        // return redirect()->route('api/getFavoriteProducts/'.$userId);getFavoriteProducts
    }

    public function getAllUsers() {
        if(!Auth::check()){
            return response()->json(['No esta autorizado'], 403);
        } else {
            if ( Auth::user()->userMarket->administrador != 1) {
                return response()->json(['No esta autorizado'], 403);
            }
        }

        $usersBD = User::orderBy('ID')->get();
        $users = [];

        foreach($usersBD as $user) {

            array_push($users, [
                'id' => $user->ID,
                'name' => $user->user_nicename,
                'status' => isset($user->userMarket) ? $user->userMarket->estatus  : '' ,
                'email' => $user->user_email,
                'registDate' => isset($user->userMarket) ? $user->userMarket->created_at?\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->userMarket->created_at)->format('d/m/Y'):'': ''
            ]);
        }

        return response()->json($users);
    }

    public function changeStatus(Request $request) {

        if(!Auth::check()){
            return response()->json(['No esta autorizado'], 403);
        } else {
            if ( Auth::user()->userMarket->administrador != 1) {
                return response()->json(['No esta autorizado'], 403);
            }
        }

        $user = UserMarket::find($request->id);
        $user->estatus = $user->estatus == 0 ? 1 : 0;
        $user->save();

        return response()->json(['success' => 'Success. Status changed'], 200);
    }

    public function changeStatusAdmin(Request $request) {
        if(!Auth::check()){
            return response()->json(['No esta autorizado'], 403);
        } else {
            if ( Auth::user()->userMarket->administrador != 1) {
                return response()->json(['No esta autorizado'], 403);
            }
        }

        $user = UserMarket::find($request->id);
        $user->administrador = $user->administrador == 0 ? 1 : 0;
        if ( $user->administrador == 1 ) {
            $this->metaUserUpdate(
                $user->wp_user_id,
                ['wp_capabilities'],
                'a:1:{s:13:"administrator";s:1:"1";}'
            );
        } else {
            $this->metaUserUpdate(
                $user->wp_user_id,
                ['wp_capabilities'],
                'a:1:{s:8:"customer";b:1;}'
            );
        }
        $user->save();

        return response()->json(['success' => 'Success. Status changed'], 200);
    }

    public function deleteUser(Request $request) {
        if(!Auth::check()){
            return response()->json(['No esta autorizado'], 403);
        } else {
            if ( Auth::user()->userMarket->administrador != 1) {
                return response()->json(['No esta autorizado'], 403);
            }
        }
        if( Auth::user()->ID != $request->id  ){
          User::find($request->id)->delete();
        }

        return response()->json(['success' => 'Success. User deleted'], 200);
    }

    public function searchUser(Request $request) {
        if(!Auth::check()){
            return response()->json(['No esta autorizado'], 403);
        } else {
            if ( Auth::user()->userMarket->administrador != 1) {
                return response()->json(['No esta autorizado'], 403);
            }
        }

        $query = $request->q;
        $users = [];

        $usersBD = User::where('user_login', 'like', '%'.$query.'%')
        ->orWhere('user_nicename', 'like', '%'.$query.'%')
        ->orWhere('user_email', 'like', '%'.$query.'%')
        ->orWhere('display_name', 'like', '%'.$query.'%')
        ->orWhereHas('userMarket', function($q) use ($query) {
            $q->where('name', 'like', '%'.$query.'%')
            ->orWhere('ap_paterno', 'like', '%'.$query.'%')
            ->orWhere('ap_materno', 'like', '%'.$query.'%');
        })->orderBy('ID')->get();


        foreach($usersBD as $user) {
            array_push($users, [
                'id' => $user->ID,
                'name' => $user->userMarket->name.' '.$user->userMarket->ap_paterno.' '.$user->userMarket->ap_materno,
                'status' => $user->userMarket->estatus,
                'email' => $user->user_email,
                'registDate' => $user->userMarket->created_at?\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->userMarket->created_at)->format('d/m/Y'):''
            ]);
        }

        return response()->json($users);
    }

    public function getCompras( Request $request ){


      if( Auth::check() ){
        $usuario = Auth::user();
        $usuario = $usuario->userMarket;
        $compras = array();

        $request->page = $request->page == 0 ? 0 : $request->page - 1;

        $solicitudes = $usuario->comprasSolicitud;
        $ofertas = $usuario->comprasOferta;


        //Rango de solicitudes que se mostraran de acuerdo al paginador;
        $a_solicitudes = $request->page * ( $request->size / 2);
        $b_solicitudes = $a_solicitudes + ( $request->size / 2 );
        $a_solicitudes = isset($solicitudes[$a_solicitudes]) ? $a_solicitudes : count($solicitudes);
        $b_solicitudes = isset($solicitudes[$b_solicitudes]) ? $b_solicitudes : count($solicitudes);

        //Rango de ofertas que se mostraran de acuerdo al paginador
        $a_ofertas = $request->page * ( $request->size / 2 );
        $b_ofertas = $a_ofertas + ( $request->size / 2 );
        $a_ofertas = isset( $ofertas[$a_ofertas] ) ? $a_ofertas : count($ofertas);
        $b_ofertas = isset( $ofertas[$b_ofertas] ) ? $b_ofertas : count($ofertas);


        //En caso que no haya suficientes solicitudes, se le aumentara el rango a las ofertas.
        if( ($request->size / 2) >= ($b_solicitudes - $a_solicitudes) ){
          $b_ofertas += ($request->size / 2) - ($b_solicitudes - $a_solicitudes);
        }

        //En caso que no haya suficientes ofertas, se le aumentara el rango a las solicitudes.
        if( ($request->size / 2) >= ($b_ofertas - $a_ofertas) ){
          $b_solicitudes += ($request->size / 2) - ($b_ofertas - $a_ofertas);
        }


        for(;$a_solicitudes < $b_solicitudes && isset($solicitudes[ $a_solicitudes ]); $a_solicitudes++){
            $solicitud = $solicitudes[ $a_solicitudes ];

            $total_venta = $solicitud->unidades;
            $completado = $solicitud->completado;
            $id = $solicitud->id;
            $solicitud = $solicitud->solicitud;
            $solicitud->imagenes;
            $solicitud->producto->categoria;
            $solicitud->unidad;
            $solicitud->usuario;
            $solicitud->estado;
            $solicitud->requerido_desde = $solicitud->getRequeridoDesde();
            $solicitud->requerido_hasta = $solicitud->getRequeridoHasta();
            $solicitud->tipo = 'solicitud';
            $solicitud->total_venta = $total_venta;
            $solicitud->completado = $completado;
            $solicitud->id = $id;
            array_push( $compras, $solicitud );
        }

        for( ; $a_ofertas < $b_ofertas && isset($ofertas[ $a_ofertas ]); $a_ofertas++ ){
            $oferta = $ofertas[ $a_ofertas ];

            if(isset($oferta)){
                $total_venta = $oferta->unidades;
                $completado = $oferta->completado;
                $id = $oferta->id;
                $oferta = $oferta->oferta;

                isset($oferta->imagenes) ? $oferta->imagenes : '';
                isset($oferta->producto->categoria) ? $oferta->producto->categoria : '';
                isset($oferta->unidad) ? $oferta->unidad : '';
                isset($oferta->usuario) ? $oferta->usuario : '';
                isset($oferta->estado) ? $oferta->estado : '';
                $oferta->disponible_desde = $oferta->getDisponibleDesde();
                $oferta->disponible_hasta = $oferta->getDisponibleHasta();
                $oferta->tipo = 'oferta';
                $oferta->total_venta = $total_venta;
                $oferta->completado = $completado;
                $oferta->id = $id;
                array_push( $compras, $oferta );
            }
        }

        $paginator = array();
        $paginator['length'] = count( $solicitudes ) + count( $ofertas );
        $paginator['pageIndex'] = $request->page;

        return response()->json(['items' => $compras, 'paginator' => $paginator ]);

      }else {
        return response()->json(['No esta autorizado para esta tarea'], 403);
      }
    }


    public function getPedidosAdmin(Request $rq){

      $tamano_pagina = $rq->size;
      $ofertas = CompraOferta::paginate($tamano_pagina);;
      $total =  CompraOferta::select( DB::raw('count(*) as total') )->get('*')[0]->total;

      $compras = array();
      foreach($ofertas as $oferta){

          $total_venta = $oferta->unidades;
          $completado = $oferta->completado;
          $id = $oferta->id;
          $oferta = $oferta->oferta;

          if( $oferta != null ){
            $oferta->imagenes;
            $oferta->producto->categoria;
            $oferta->unidad;
            $oferta->usuario;
            $oferta->estado;
            $oferta->disponible_desde = $oferta->getDisponibleDesde();
            $oferta->disponible_hasta = $oferta->getDisponibleHasta();
            $oferta->tipo = 'oferta';
            $oferta->total_venta = $total_venta;
            $oferta->completado = $completado;
            $oferta->id = $id;
            array_push( $compras, $oferta );
          }
      }

      $paginator = array();
      $paginator['length'] = $total;
      return response()->json(['items' => $compras, 'paginator' => $paginator], 200);
    }











    public function getOfertasAdmin(Request $rq){

      $compras = array();
      $tamano_pagina = $rq->size;
      $solicitudes = CompraSolicitud::paginate($tamano_pagina);
      $total = CompraSolicitud::select( DB::raw('count(*) as total') )->get('*')[0]->total;

      foreach($solicitudes as $solicitud){
          $total_venta = $solicitud->unidades;
          $completado = $solicitud->completado;
          $id = $solicitud->id;
          $solicitud = $solicitud->solicitud;
          if( $solicitud != null ){
            $solicitud->imagenes;
            $solicitud->producto->categoria;
            $solicitud->unidad;
            $solicitud->usuario;
            $solicitud->estado;
            $solicitud->requerido_desde = $solicitud->getRequeridoDesde();
            $solicitud->requerido_hasta = $solicitud->getRequeridoHasta();
            $solicitud->tipo = 'solicitud';
            $solicitud->total_venta = $total_venta;
            $solicitud->completado = $completado;
            $solicitud->id = $id;
            array_push( $compras, $solicitud );
          }
      }

      $paginator = array();
      $paginator['length'] = $total;

      return response()->json(['items' => $compras, 'paginator' => $paginator], 200);
    }










    public function metaUserUpdate($user_id, $meta_key, $meta_value){

        foreach( $meta_key as $key ){

            $wp = UserMeta::where('user_id', $user_id)
                ->where('meta_key', $key)
                ->first();

            if( $wp != null ){
                $wp->meta_value = $meta_value;
                $wp->save();
            }else{
                $wp = new UserMeta();
                $wp->user_id = $user_id;
                $wp->meta_key = $key;

                $wp->meta_value = $meta_value;
                $wp->save();
            }

        }
    }


}
