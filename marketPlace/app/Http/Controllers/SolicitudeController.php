<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Solicitud;
use App\Categoria;
use App\Pais;
use App\Estado;
use App\Oferta;
use App\ImagenesSolicitud;
use App\CertificacionSolicitud;
use App\ComentarioSolicitud;
use App\Certificacion;
use App\UserMarket;
use App\Producto;
use App\Unidad;
use App\Presentacion;
use Validator;
use DB;
use File;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class SolicitudeController extends Controller
{
    public function sendEmail(String $type, $id){

        $offert = Oferta::find($id);

        $mail = new PHPMailer;
        $mail->isSMTP();

        $mail->Host = "smtp.gmail.com";
        $mail->SMTPAuth = true;

        $mail->Username = "cdtst3@gmail.com";
        $mail->Password = "ljrhitovxyxunzhw";

        $mail->SMTPSecure = "tls";
        $mail->Port = 587;

        $mail->From = "name@gmail.com";
        $mail->FromName = "INFOOD";

        switch($type){

            case 'new':
                $mail->Subject = 'Nuevo producto solicitado';
                $mail->Body = File::get(storage_path('emails/newSolicitute.txt'));
                $direction_emails = DB::table('infood.wp_users')
                                            ->join('tbl_users','infood.wp_users.id','=', 'tbl_users.wp_user_id')
                                            ->where('tbl_users.administrador','=','1')
                                            ->select('infood.wp_users.user_email')->get();

                foreach($direction_emails as $dir){
                    $mail->addBCC($dir->user_email);
                    $mail->addAddress('contacto@infood.com.mx');
                }
            break;

            case 'approved':
                $mail->Subject = 'Estatus solicitud';
                $mail->Body = File::get(storage_path('emails/enableSolicitute.txt'));

                $mail->addAddress(DB::table('infood.wp_users')
                                            ->select('infood.wp_users.user_email')
                                            ->where('infood.wp_users.id','=',$id)
                                            ->first()->user_email);
                $mail->addAddress('contacto@infood.com.mx');
            break;
        }

        $mail->isHTML(true);
        $mail->send();
    }

    public function saveSolicitude(Request $request){
        $data=json_decode($request->solicitude,true);
        //dd($data);
        $validator = Validator::make($data, $this->rulesAddAndEdit(), $this->mesaggesAddAndEdit());

        if($validator->passes()) {
            $data = json_decode($request->solicitude);
            $desde = $data->requerido_desde->jsdate;
            $desde = str_replace('00:00.000Z', '00:00:00', $desde);

            $hasta = $data->requerido_hasta == null ? null : $data->requerido_hasta->jsdate;
            $hasta = str_replace('00:00.000Z', '00:00:00', $hasta);

            $desde = substr($desde, 0, 10) . ' ' . substr($desde, 14, strlen($desde) );
            $hasta = substr($hasta, 0, 10) . ' ' . substr($hasta, 14, strlen($hasta) );

            //dd($hasta);
            $solicitude = new Solicitud;

            $solicitude->aprobado = 0;
            $solicitude->ciudad = $data->ciudad;
            $solicitude->codigo_postal = $data->codigo_postal;
            $solicitude->requerido_desde = $desde; //'2018-07-11 00:00:00'
            $solicitude->requerido_hasta = $hasta;
            $solicitude->estado_id = $data->estado_id;
            $solicitude->otras_caracteristicas = $data->otras_caracteristicas;
            $solicitude->precio_unidad = $data->precio_unidad;
            $solicitude->resena = $data->resena;
            $solicitude->tamano = $data->tamano;
            $solicitude->unidad_id = $data->unidad_id;//bien
            $solicitude->presentacion_id = $data->presentacion_id;
            $solicitude->unidad_presentacion_id = $data->unidad_presentacion_id;
            $solicitude->variedad = $data->variedad;
            $solicitude->volumen_total = $data->volumen_total;
            $solicitude->usuario_id = Auth::user()->ID;
            $solicitude->producto_id = $data->producto_id;

            $solicitude->save();
            $idSale = $solicitude->id;

            foreach( $data->photo as $index=>$image ){
                if ( $image->type == 'default' ){
                //    ImagenesSolicitud::create([ 'solicitud_id'=>$idSale,'imagen'=>$image->imagen ]);
                }else if( $image->type == 'up' ) {
                    ImagenesSolicitud::create([ 'solicitud_id'=>$idSale,'imagen'=>$this->getDataImage( $image->imagen, 'producto',$index ) ]);
                }
            }

            foreach( $data->certificates as $certificates ){
                CertificacionSolicitud::create([ 'solicitud_id'=>$idSale, 'certificacion_id'=>$certificates->id]);
            }

            $this->sendEmail('new',null);

        }else return response()->json(['error' => $validator->errors()->all()]);;

    }

    public function editSolicitudeSetData(Request $request){
        $id = json_decode($request->id);
        $solicitud = Solicitud::find($id);
        $imagenes = DB::table('tbl_imagenes_solicitud')->where('solicitud_id',$id)->get();

        $administrador = UserMarket::where('wp_user_id',Auth::user()->ID)->first()->administrador;
        $certificates_sale = DB::table('tbl_certificaciones_solicitud')->where('solicitud_id',$id)->get();

        return json_encode(['ofert'=>$solicitud, 'images'=>$imagenes,'certificatesOffert'=>$certificates_sale, 'permisionOptions'=>$administrador ]);
    }

    public function editSolicitude(Request $request){
        $data=json_decode($request->solicitud,true);

        $validator = Validator::make($data, $this->rulesAddAndEdit(), $this->mesaggesAddAndEdit());

        if($validator->passes()) {
            $data = json_decode($request->solicitud);

            if( isset($data->requerido_desde->jsdate) ){
              $desde = $data->requerido_desde->jsdate;
              $desde = str_replace('00:00.000Z', '00:00:00', $desde);

              $hasta = $data->requerido_hasta == null ? null : $data->requerido_hasta->jsdate;
              $hasta = str_replace('00:00.000Z', '00:00:00', $hasta);

              $desde = substr($desde, 0, 10) . ' ' . substr($desde, 14, strlen($desde) );
              $hasta = substr($hasta, 0, 10) . ' ' . substr($hasta, 14, strlen($hasta) );
            }else{
              $desde = $data->requerido_desde;
              $hasta = $data->requerido_hasta;
            }

            $idSale = $data->id;

            $solicitud = Solicitud::find($idSale);

            $solicitud->ciudad = $data->ciudad;
            $solicitud->codigo_postal = $data->codigo_postal;
            $solicitud->requerido_desde = $desde; //'2018-07-11 00:00:00'
            $solicitud->requerido_hasta = $hasta;
            $solicitud->estado_id = $data->estado_id;
            $solicitud->otras_caracteristicas = $data->otras_caracteristicas;
            $solicitud->precio_unidad = $data->precio_unidad;
            $solicitud->resena = $data->resena;
            $solicitud->tamano = $data->tamano;
            $solicitud->unidad_id = $data->unidad_id;
            $solicitud->presentacion_id = $data->presentacion_id;
            $solicitud->unidad_presentacion_id = $data->unidad_presentacion_id;
            $solicitud->variedad = $data->variedad;
            $solicitud->volumen_total = $data->volumen_total;
            $solicitud->producto_id = $data->producto_id;

            $solicitud->save();

            $certificados  = CertificacionSolicitud::where('solicitud_id',"=",$idSale)->get();

            foreach($certificados as $cert){
                $cert->delete();
            }

            foreach( $data->photo as $index=>$image ){
                if ( $image->type == 'default' ){
                  //  ImagenesSolicitud::create([ 'solicitud_id'=>$idSale,'imagen'=>$image->imagen ]);
                }else {
                    ImagenesSolicitud::create([ 'solicitud_id'=>$idSale,
                                                'imagen'=>$this->getDataImage( $image->imagen, 'producto',$index ) ]);
                }
            }

            foreach( $data->certificates as $certificates ){
                CertificacionSolicitud::create([ 'solicitud_id'=>$idSale, 'certificacion_id'=>$certificates->id ]);
            }

        }else return response()->json(['error' => $validator->errors()->all()]);;
    }

    public function eraseImage(Request $request){
       $imagen =  ImagenesSolicitud::find($request->id);
       $image = explode( env('APP_URL').'storage/producto/solicitud/' , $imagen->imagen );
       count($image) > 1 ? Storage::disk('imagen_producto_solicitud')->delete($image[1]) : '' ;
       $imagen->delete();
    }

    public function retrievesUserSolicitudes(){
        $producto_clase = [];

        $id = Auth::user()->ID;

        $solicitutes=DB::table('tbl_solicitudes')
            ->where('usuario_id', $id)
            ->get();

        if($solicitutes != null){
            foreach($solicitutes as $solicitud){

                $image = ImagenesSolicitud::where('solicitud_id',$solicitud->id)
                    ->first()->imagen;
                $unidad = Unidad::find($solicitud->unidad_id);
                $producto = Producto::find($solicitud->producto_id);

                $producto_clase [] = [
                    "id" => $solicitud->id,
                    "imagen" => $image,
                    "precio" => $solicitud->precio_unidad,
                    "unidad" => $unidad != null ? $unidad->nombre : 'No especificado',
                    "nombre" => $producto->nombre,
                    "variedad" => $solicitud->variedad,
                    "volumen" => $solicitud->volumen_total,
                    "disponibleDesde" => date('d/m/Y', strtotime($solicitud->requerido_desde)),
                    "disponibleHasta" => date('d/m/Y', strtotime($solicitud->requerido_hasta)),
                    "aprobado" => $solicitud->aprobado
                ];
                //$product->proveedor = ;
            }
        }

        return json_encode($producto_clase);
    }

    public function copyDataSolicitute(Request $request){
        $id = json_decode($request->id);

        $solicitud_base = Solicitud::find($id);
        //dd($solicitud);
        $solicitud = $solicitud_base->replicate();

        $solicitud->aprobado = 0;

        $solicitud->save();

        $idSale = $solicitud->id;

        $imagenes = DB::table('tbl_imagenes_solicitud')->where('solicitud_id',$solicitud_base->id)->get();
        $certificados = CertificacionSolicitud::where('solicitud_id',"=",$solicitud_base->id)->get();

        foreach( $imagenes as $image ){
            ImagenesSolicitud::create([ 'solicitud_id'=>$idSale,'imagen'=>$image->imagen ]);
        }

        foreach( $certificados as $certificates ){
            CertificacionSolicitud::create([ 'solicitud_id'=>$idSale, 'certificacion_id'=>$certificates->certificacion_id]);
        }

        $this->sendEmail('new',null);
        return $this->retrievesUserSolicitudes();
    }

    public function deleteSolicitudes(Request $request){
        $id = json_decode($request->id);

        $solicitud = Solicitud::where('id','=',$id)->first();
        $images = ImagenesSolicitud::where('solicitud_id','=',$solicitud->id)->get();
        //dd($images);
        $certificates = CertificacionSolicitud::where('solicitud_id',"=",$solicitud->id)->get();

        foreach( $images as $img ){
            $image = explode( env('APP_URL').'storage/producto/solicitud/' , $img->imagen );
            count($image) > 1 ? Storage::disk('imagen_producto_solicitud')->delete($image[1]) : '' ;
            $img->delete();
        }

        foreach( $certificates as $cert ){
            $cert->delete();
        }

        $solicitud->delete();

        return $this->retrievesUserSolicitudes();
    }

    public function getDataSolicituteResume(Request $request){

        $app = app();
        $information = $app->make('stdClass');
        $certificates_sale = [];

        $id = json_decode($request->id);
        $userId = Auth::user()->ID;
        $solicitud = Solicitud::find($id);

        $nombreComercial = UserMarket::where('wp_user_id',$userId)->first()->nombre_comercial;

        $producto = Producto::find($solicitud->producto_id)->nombre;

        $origen_detalle = Estado::find($solicitud->estado_id);

        $presentacion_detalle = Presentacion::find($solicitud->presentacion_id);
        $presentacion = $presentacion_detalle != null ? $presentacion_detalle->nombre : 'No especificado';

        $unidad_detalle = Unidad::find($solicitud->unidad_id);
        $unidad = $unidad_detalle != null ? $unidad_detalle->nombre : 'No especificado' ;

        $tipoPrecio = $solicitud->precio === 'f' ? 'Fijo' : 'Variable';

        $imagenes = DB::table('tbl_imagenes_solicitud')->where('solicitud_id',$id)->get();
        $certificates = DB::table('tbl_certificaciones_solicitud')->where('solicitud_id',$id)->get();

        $comentarios = [];
        $comentarios_base = ComentarioSolicitud::where('solicitud_id',"=",$id)->get();

        foreach($comentarios_base as $cbase){
            $user =UserMarket::where('wp_user_id',$cbase->usuario_id)->first()->name;

            $comentarios[] = [
                                'user'=>$user,
                                'fecha'=>date('d/m/Y', strtotime($cbase->created_at)),
                                'comentario'=>$cbase->comentario
                            ];
        }

        $desde = date('d/m/Y',strtotime($solicitud->requerido_desde));
        $hasta = date('d/m/Y',strtotime($solicitud->requerido_hasta));

        foreach($certificates as $cert){
           $certificates_sale[] = [Certificacion::find($cert->certificacion_id)->imagen];
        }

        $information->producto = $producto.' '.$solicitud->variedad;
        $information->precio_unidad = $solicitud->precio_unidad;
        $information->caracteristicas = $solicitud->otras_caracteristicas;
        $information->productor = $nombreComercial != '' ? $nombreComercial : 'No especificado';
        $information->produccion_total = $solicitud->volumen_total != null ? $solicitud->volumen_total : 'No especificado' ;
        $information->presentacion = $presentacion;
        $information->tamano = $solicitud->tamano;
        $information->desde = $desde;
        $information->hasta = $hasta;
        $information->certificados = $certificates_sale;
        $information->resena = $solicitud->resena;
        $information->comentarios = $comentarios;
        $information->imagenes = $imagenes;
        $information->unidad = $unidad;
        $information->tipo_precio = $tipoPrecio;
        $information->origen = $origen_detalle != null ? $origen_detalle->nombre : 'No especificado';
        $information->ciudad = $solicitud->ciudad;
        $information->lat = $origen_detalle != null ? $origen_detalle->latitud : 19.405591;
        $information->lon = $origen_detalle != null ? $origen_detalle->longitud : -99.144656;;

        return json_encode($information);
    }

    public function addCommentSolicitude(Request $request){
        $comentario = json_decode($request->comentario);

        if($comentario->comentario !=''){
            $transaccion = ComentarioSolicitud::create([
                                        'solicitud_id'=>$comentario->id_oferta,
                                        'usuario_id'=>Auth::user()->ID,
                                        'comentario'=>$comentario->comentario
                                    ]);

            if($transaccion){
                $comentarios = [];
                $comentarios_base = ComentarioSolicitud::where('solicitud_id',"=",$comentario->id_oferta)->get();

                foreach($comentarios_base as $cbase){
                    $user = UserMarket::find($cbase->usuario_id)->nickname;
                    $comentarios[] = [
                                        'user'=>$user,
                                        'fecha'=>date('d/m/Y', strtotime($cbase->created_at)),
                                        'comentario'=>$cbase->comentario
                                    ];
                }
                return json_encode($comentarios);
            }
        }else{
            return 'false';
        }
    }

    public function getDataImage(String $base64Image,$tipo,$number){

        $id = Auth::user()->ID;
        $file_data = $base64Image;
        $file_name = 'image_'.time().'-'.$number.'-'.$id.'.jpg';
        
        @list($type, $file_data) = explode(';', $file_data);
        @list(, $file_data) = explode(',', $file_data);
        $file_data = str_replace(' ', '+', $file_data);

        if($file_data!=""){
            Storage::disk('imagen_producto_solicitud')->put($file_name,base64_decode($file_data));
            return env('APP_URL').'/storage/producto/solicitud/'.$file_name;
            //return 'http://127.0.0.1:8000/storage/producto/solicitud/'.$file_name;
        }
    }

    public function rulesAddAndEdit(){
        $rules = [
                    'producto_id'=>'required|regex:/^[1-9][0-9]*/',
                    'variedad'=>'required',
                    'volumen_total'=>'numeric|nullable',
                    'precio_unidad'=>'numeric|nullable',
                    'tamano'=>'numeric|nullable',
                    'pedido_minimo'=>'numeric|nullable',
                    'codigo_postal'=>'nullable',
                    'requerido_desde'=>'required',
                    'requerido_hasta'=>'required'
                ];
            return $rules;
    }

    public function mesaggesAddAndEdit(){
        $mesagges=[
                    'producto_id.required'=>'El campo Producto es obligatorio',
                    'variedad.required'=>'El campo Variedad es obligatorio',
                    'otras_caracteristicas.required'=>'El campo Otras Caracteristicas es obligatorio',
                    'volumen_total.numeric'=>'El campo Volumen Total es numerico',
                    'precio_unidad.numeric'=>'El campo Precio por Unidad es numerico',
                    'tamano.numeric'=>'EL campo Tama���o es numerico',
                    'pedido_minimo.numeric'=>'El campo Pedido minimo es numerico',
                    'requerido_desde.required'=>'El campo Disponibilidad de tu Producto Desde es obligatorio ',
                    'requerido_hasta.required'=>'El campo Disponibilidad de tu Producto Hasta es obligatorio ',
        ];

        return $mesagges;
    }

    public function retrievesallUserSolicitudes(){
        $producto_clase = [];

        $solicitutes=Solicitud::all();

        if($solicitutes != null){
            foreach($solicitutes as $solicitud){

                $image = DB::table('tbl_imagenes_solicitud')
                    ->where('solicitud_id',$solicitud->id)
                    ->limit(1)
                    ->get();
                $unidad = Unidad::find($solicitud->unidad_id);
                $producto = Producto::find($solicitud->producto_id);
                //dd($unidad);
                $img = '';
                foreach($image as $i){
                    $img = $i->imagen;
                }

                $producto_clase [] = [
                    "id" => $solicitud->id,
                    "imagen" => $img,
                    "precio" => $solicitud->precio_unidad,
                    "unidad" => $unidad != null ? $unidad->nombre : 'No especificado',
                    "nombre" => $producto->nombre,
                    "variedad" => $solicitud->variedad,
                    "volumen" => $solicitud->volumen_total,
                    "disponibleDesde" => date('d/m/Y', strtotime($solicitud->requerido_desde)),
                    "disponibleHasta" => date('d/m/Y', strtotime($solicitud->requerido_hasta)),
                    "aprobado" => $solicitud->aprobado
                ];
                //$product->proveedor = ;
            }
        }

        return json_encode($producto_clase);
    }

    public function deleteAdministrativeSolicitudes(Request $request){
        $id = json_decode($request->id);

        $solicitud = Solicitud::where('id','=',$id)->first();
        $images = ImagenesSolicitud::where('solicitud_id','=',$solicitud->id)->get();
        $certificates = CertificacionSolicitud::where('solicitud_id',"=",$solicitud->id)->get();

        foreach( $images as $img ){
            $image = explode( env('APP_URL').'storage/producto/solicitud/' , $img->imagen );
            count($image) > 1 ? Storage::disk('imagen_producto_solicitud')->delete($image[1]) : '' ;
            $img->delete();$img->delete();
        }

        foreach( $certificates as $cert ){
            $cert->delete();
        }

        $solicitud->delete();

        return $this->retrievesallUserSolicitudes();
    }

    public function approved(Request $request){
        $id = json_decode($request->id);

        $solicitud = Solicitud::find($id);

        $solicitud->aprobado == 0 ? $solicitud->aprobado = 1 : $solicitud->aprobado = 0;

        $solicitud->save();

        $solicitud->aprobado == 1 ? $this->sendEmail('approved',$solicitud->usuario_id) : '';
    }

}
