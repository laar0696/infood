<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserMeta;
use App\UserMarket;
use DB;
use Illuminate\Support\Facades\Storage;
use App\Pais;
use App\Estado;
use App\TipoUsuario;
use MikeMcLin\WpPassword\Facades\WpPassword;

use Illuminate\Support\Facades\Auth;

class DashboardForm extends Controller
{




    public function personalDataUpdate(Request $r){

        /*
            NUNCA USAR $data->enterprise_avatar ni $data->profile_avatar
            SE ENVIAN AL SERVIDOR, PERO SUS VALORES NO SE DEBEN IMPLEMENTAR
        */

        $data = json_decode($r->user);

        $data = isset( $data->user ) ? $data->user : $data;

        $autoEdit = Auth::user()->ID == $data->wp_user_id;

        if( !Auth::check() ){
            return response()->json(['No hay un usuario logueado', 500]);
        }

        if( $autoEdit ) {
            $user = User::find($data->wp_user_id)->userMarket;
            $this->editUserDataGeneral($user, json_decode($r->user) );
        } else {
            $user = User::find( $data->wp_user_id )->userMarket;
            $this->editUserDataGeneral($user,$data);
        }

    }






    public function editUserDataGeneral($user,$data){

        $imgPerfilUrl = $this->getDataImage($data->image_profile,'perfil',$user->id);
        $imgFacturacion = $this->getDataImage($data->image_enterprise,'empresa',$user->id);

        if( $imgPerfilUrl != null ) {
            $user->perfil_avatar =  $this->getDataImage($data->image_profile,'perfil',$user->id);
        }

        if( $imgFacturacion != null ) {
            $user->empresa_avatar = $this->getDataImage($data->image_enterprise,'empresa',$user->id);
        }

        $user->userWp->user_nicename = $data->nickname;
        $user->name = $data->first_name;
        $user->ap_paterno = $data->last_name;
        $user->ap_materno = $data->secont_last_name;
        $user->email_alt = $data->email_alt;
        $user->celular = $data->celphone;
        $user->rfc = $data->rfc;
        $user->telefono_empresa = $data->enterprise_phone;
        $user->nombre_comercial = $data->comertial_name;
        $user->semblanza = $data->semblance;
        $user->estado_id = $data->estado_id;
        $user->ciudad = $data->ciudad;
        if($data->estatus != null) {
            $user->estatus = $data->estatus;
        }
        $user->userWp->user_email = $data->billing_email;
        $user->tipo_usuario_id = $data->user_type;

        if ($data->administrador==true || $data->administrador == 1) {
            $this->metaUserUpdate(
                $user->wp_user_id,
                ['wp_capabilities'],
                'a:1:{s:13:"administrator";s:1:"1";}'
            );
        } else {
            $this->metaUserUpdate(
                $user->wp_user_id,
                ['wp_capabilities'],
                'a:1:{s:8:"customer";b:1;}'
            );
        }

        $user->userWp->save();
        $user->save();

        $u_id = $user->wp_user_id;


        $this->metaUserUpdate(
            $u_id,
            ['nickname'],
            $data->nickname
        );

        $this->metaUserUpdate(
            $u_id,
            ['email_alt'],
            $data->email_alt
        );

        $this->metaUserUpdate(
            $u_id,
            ['first_name', 'billing_first_name', 'shipping_first_name'],
            $data->first_name
        );

        $this->metaUserUpdate(
            $u_id,
            ['last_name', 'billing_last_name', 'shipping_last_name'],
            $data->last_name . ' ' . $data->secont_last_name
        );

        $this->metaUserUpdate(
            $u_id,
            ['billing_phone'],
            $data->phone

        );

        $this->metaUserUpdate(
            $u_id,
            ['billing_address_1', 'shipping_address_1'],
            $data->billing_address
        );

        $this->metaUserUpdate(
            $u_id,
            ['billing_address_2', 'billing_address_2'],
            ''
        );

        $this->metaUserUpdate(
            $u_id,
            ['billing_city', 'shipping_city'],
            ''
        );

        $this->metaUserUpdate(
            $u_id,
            ['billing_postcode', 'shipping_postcode'],
            $data->cp
        );

        $this->metaUserUpdate(
            $u_id,
            ['billing_company'],
            $data->billing_company
        );

        $wp = DB::table('infood.wp_usermeta')
            ->where('user_id', $u_id)
            ->get();

       return response()->json(['Usuario modificado', 200]);
    }







    public function getUserData($id) {
        if(!Auth::check()){
            return response()->json(['No esta autorizado'], 403);
        } else {
            if ( Auth::user()->userMarket->administrador != 1) {
                return response()->json(['No esta autorizado'], 403);
            }
        }

        $app = app();
        $user_meta_usable = $app->make('stdClass');

        $user = User::find($id);

        $tipo_usuario = $user->userMarket->tipo_usuario_id;

        $user_propio = Auth::user()->ID == $id;

        $u_id = $user->ID;

        $user_market = $user->userMarket;
        $user_meta = UserMeta::where('user_id', $u_id)->get();

        $user_meta_usable->email = $user_market->userWp->user_email;

        foreach($user_meta as $u){
            if($u->meta_key === 'nickname'){
                $user_meta_usable->nickname = $u->meta_value;
            }else if( $u->meta_key === 'billing_phone' ){
                $user_meta_usable->billing_phone = $u->meta_value;
            }else if(  $u->meta_key === 'billing_address_1'  ){
                $user_meta_usable->billing_address = $u->meta_value;
            }else if( $u->meta_key === 'billing_postcode' ){
                $user_meta_usable->cp = $u->meta_value;
            }else if( $u->meta_key === 'billing_company' ){
                $user_meta_usable->billing_company = $u->meta_value;
            };
        }

        $countries = Pais::all();
        $states = Estado::all();
        $countriesAux = [];
        $statesAux = [];

        foreach( $countries as $country ){
            foreach( $states as $state ) {
                if( $state->pais_id == $country->id ) {
                    $statesAux [] = [ 'id'=>$state->id, 'pais_id'=>$state->pais_id, 'name'=>$state->nombre];
                }
            }
            $countriesAux[] = [ 'id'=>$country->id, 'name'=>$country->nombre, 'states'=>$statesAux ];
            $statesAux = [];
        }

        return response()->json([
                                 'usermeta' => $user_meta_usable,
                                 'usermarket' => $user_market,
                                 'countries'=>$countriesAux,
                                 'users_type' => TipoUsuario::all(),
                                 'user_type' => $tipo_usuario,
                                 'propio' => $user_propio
                                ]);
    }






    public function personalDataGet(){
        $app = app();
        $user_meta_usable = $app->make('stdClass');

        $user = Auth::user();

        $tipo_usuario = Auth::user()->userMarket->tipo_usuario_id;

        $u_id = $user->ID;

        $user_market = $user->userMarket;
        $user_meta = UserMeta::where('user_id', $u_id)->get();
        $user_meta_usable->email = $user_market->userWp->user_email;


        foreach($user_meta as $u){
            if($u->meta_key === 'nickname'){
                $user_meta_usable->nickname = $u->meta_value;
            }else if( $u->meta_key === 'billing_phone' ){
                $user_meta_usable->billing_phone = $u->meta_value;
            }else if(  $u->meta_key === 'billing_address_1'  ){
                $user_meta_usable->billing_address = $u->meta_value;
            }else if( $u->meta_key === 'billing_postcode' ){
                $user_meta_usable->cp = $u->meta_value;
            }else if( $u->meta_key === 'billing_company' ){
                $user_meta_usable->billing_company = $u->meta_value;
            }

        }

        $countries = Pais::all();
        $states = Estado::all();
        $countriesAux = [];
        $statesAux = [];

        foreach( $countries as $country ){
            foreach( $states as $state ) {
                if( $state->pais_id == $country->id ) {
                    $statesAux [] = [ 'id'=>$state->id, 'pais_id'=>$state->pais_id, 'name'=>$state->nombre];
                }
            }
            $countriesAux[] = [ 'id'=>$country->id, 'name'=>$country->nombre, 'states'=>$statesAux ];
            $statesAux = [];
        }

        return response()->json([
                                 'usermeta' => $user_meta_usable,
                                 'usermarket' => $user_market,
                                 'countries'=>$countriesAux,
                                 'users_type' => TipoUsuario::all(),
                                 'user_type' => $tipo_usuario,
                                 'propio' => true
                                ]);
    }








    public function metaUserUpdate($user_id, $meta_key, $meta_value){

        foreach( $meta_key as $key ){

            $wp = UserMeta::where('user_id', $user_id)
                ->where('meta_key', $key)
                ->first();

            if( $wp != null ){
                $wp->meta_value = $meta_value;
                $wp->save();
            }else{
                $wp = new UserMeta();
                $wp->user_id = $user_id;
                $wp->meta_key = $key;

                $wp->meta_value = $meta_value;
                $wp->save();
            }

        }
    }







    public function getAllUserData($id){

        $info=DB::connection('mysql2')->
                  select('select meta_key, meta_value from wp_usermeta where user_id = 1');

    }








    public function billingSelectInfo(){
        $app = app();

        $country = DB::connection('mysql')->
                    select('select * from tbl_users');
    }











    public function getDataImage(String $base64Image, String $typeImage,String $user){

        $file_data = $base64Image;
        $file_name = 'image_'.$user.'.jpg';

        @list($type, $file_data) = explode(';', $file_data);
        @list(, $file_data) = explode(',', $file_data);
        $file_data = str_replace(' ', '+', $file_data);

        if($file_data!=""){
            if($typeImage == 'empresa'){
                Storage::disk('avatar_empresa')->put($file_name,base64_decode($file_data));
                return env('APP_URL').'/storage/avatar/empresa/'.'image_'.$user.'.jpg';
            }else{
                Storage::disk('avatar_perfil')->put($file_name,base64_decode($file_data));
                return env('APP_URL').'/storage/avatar/perfil/'.'image_'.$user.'.jpg';
            }

        }

    }







    public function updatePassword(Request $rq){
      $user = Auth::user();
      $user->user_pass = WpPassword::make( $rq->newPass );
      $user->save();
      return response()->json(['La contraseña fue actualizada.']);
    }




}
