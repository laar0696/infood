<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMarket extends Model
{

    protected $table = 'tbl_users';
    protected $primaryKey = 'id';



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'name',
        'perfil_avatar',
        'email',
        'email_alt',
        'celular',
        'empresa_avatar',
        'razon_social',
        'rfc',
        'telefono_empresa',
        'estado_id',
        'nombre_comercial',
        'semblanza',
        'tipo_usuario_id',
        'categoria_id',
        'estatus',
        'fecha_baja',
        'administrador',
        'ap_materno',
        'ap_paterno',
        'wp_user_id',
        'ciudad'
      ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'password'
    ];

    public function estado(){
      return $this->belongsTo('App\Estado', 'estado_id');
    }

    public function tipoUsuario(){
      return $this->belongsTo('App\TipoUsuario', 'tipo_usuario_id');
    }

    public function categoria(){
      return $this->belongsTo('App\Categoria', 'categoria_id');
    }

    public function userWp(){
      return $this->belongsTo('App\User', 'wp_user_id', 'ID');
    }

    public function calificacionesHechas() {
      return $this->hasMany('App\Calificacion', 'usuario_califica_id', 'id');
    }

    public function calificacionesRecibidas() {
      return $this->hasMany('App\Calificacion', 'usuario_calificado_id', 'id');
    }

    public function comprasOferta(){
      return $this->hasMany('App\CompraOferta', 'usuario_id');
    }

    public function comprasSolicitud(){
      return $this->hasMany('App\CompraSolicitud', 'usuario_id');
    }

    public function favoritos(){
      return $this->hasMany('App\Favorito','usuario_id');
    }

    public function solicitudes(){
      return $this->hasMany('App\Solicitud','usuario_id');
    }

    public function ofertas(){
      return $this->hasMany('App\Oferta', 'usuario_id');
    }
}
