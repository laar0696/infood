<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentarioCompraSolicitud extends Model
{
    protected $table = "tbl_comentarios_compra_solicitud";
    protected $primaryKey = "id";

    protected $fillable = [
      "comentario",
      "usuario_id",
      "compra_solicitud_id"
    ];

    public function compraSolicitud(){
      return $this->belongsTo('App\CompraSolicitud','compra_solicitud_id');
    }

    public function usuario(){
      return $this->belongsTo('App\UserMarket', 'usuario_id');
    }
}
