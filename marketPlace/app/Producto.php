<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = "tbl_productos";
    protected $primaryKey = "id";

    protected $fillable = [
        "nombre", "categoria_id"
    ];

    public function categoria(){
        return $this->belongsTo('App\Categoria', 'categoria_id');
    }
}
