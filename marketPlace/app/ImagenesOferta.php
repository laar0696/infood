<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagenesOferta extends Model
{
    protected $table = "tbl_imagenes_oferta";
    protected $guard = "api";
    protected $primaryKey = "id";

    protected $fillable = [
        "imagen",
        "venta_id"
    ];

    public function venta(){
        $this->belongsTo('App\Venta', 'venta_id');
    }
}
