<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompraSolicitud extends Model
{
    protected $table = 'tbl_compras_solicitud';
    protected $primaryKey = 'id';

    protected $fillable = [
      'unidades',
      'completado'
    ];

    public function usuario(){
      return $this->belongsTo('App\UserMarket','usuario_id');
    }

    public function solicitud(){
      return $this->belongsTo('App\Solicitud','solicitud_id');
    }

    public function comentarios(){
      return $this->hasMany('App\ComentarioCompraSolicitud', 'compra_solicitud_id');
    }
}
