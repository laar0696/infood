<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentarioOferta extends Model
{

    protected $table = "tbl_comentarios_oferta";
    protected $primaryKey = "id";

    protected $fillable = [
        "oferta_id","usuario_id","comentario"
    ];


}
