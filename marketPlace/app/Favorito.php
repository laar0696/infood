<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorito extends Model
{
    protected $table = "tbl_favoritos";
    protected $primayKey = "id";
    protected $guard = "api";

    protected $fillable = [
        "oferta_id",
        "usuario_id"
    ];

    public function oferta(){
        return $this->belongsTo('App\Oferta', 'oferta_id');
    }

    public function usuario(){
        return $this->belongsTo('App\UserMarket', 'usuario_id');
    }
}
