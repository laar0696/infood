<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificacion extends Model
{
    protected $table = "tbl_certificaciones";
    protected $primaryKey = "id";
    protected $guard = "api";

    protected $fillable = [
        "nombre",
        "imagen"
    ];

    public function ofertas(){
      return $this->belongsToMany('App\Oferta', 'tbl_certificaciones_oferta', 'certificacion_id', 'oferta_id');
    }

    public function solicitudes(){
      return $this->belongsToMany('App\Solicitud', 'tbl_certificaciones_solicitud', 'certificacion_id', 'solicitud_id');
    }

}
