<?php
/*
 * The template for displaying "No search results"
 * 
 * @package Healthy Farm
*/
?>
<section>
	<article>
		<div class="noSearch">
			<h2 class="subTitleSearch"><?php _e('No encontramos resultados para:'.get_search_query(), 'themerex'); ?></h2>
			<p>
				<b><?php _e('Buscar de nuevo', 'themerex'); ?></b>
				<?php echo do_shortcode('[trx_search top="20"]'); ?>						
			</p>
			<center><?php echo sprintf(__('Ir a <a href="%s">página principal</a>', 'themerex'), home_url(), get_bloginfo()); ?></center>		
			
		</div>
	</article>
</section>
